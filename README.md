# dorf
A toy operating system

Simple toy operating system that is unfinished and will likely forever be in
that state.

# Building

Install ext2-tools:

1. apt-get install e2tools

Can be built with a GCC Cross-compiler and Bochs or Qemu for the emulation.

For it to work properly with Grub, you have to take some extra steps:

1. apt-get install grub-pc-bin
2. Install [xorriso](https://www.gnu.org/software/xorriso/)

The ISO-file can then be created by typing (assumes xorriso is at:
/opt/bin/xorriso):

~~~
make
~~~

# Directories

1. [src](src/) - Source code
2. [data](data/) - Data which should be copied on the OS-disk
3. [doc](doc/) - Code to generate Doxygen documentation

# Source code directories

1. [arch](src/arch/) - Code specific to architecture (x86, x86_64)
2. [sys](src/sys/) - Kernel code.
3. [include](src/include/) - Header (.h) files.
4. [lib](src/lib/) - Kernel library (re-creation of common c-functions).
5. [drv](src/drv/) - Kernel drivers for devices.
6. [apps](src/apps/) - User-mode applications.
7. [tools](src/tools/) - Tools used in the build process.

# Screenshot

![User shell](doc/screenshot.png)

# Current boot process

Below is an overview of the boot process:

1. Setup UART connection for printing info.
2. Get kernel stack pointer, but don't move stack.
3. Check if bootloader passed the necessary parameters to us (modules, memory
   map, etc.)
4. Initialize physical memory manager.
5. Move multiboot modules to a more suitable location.
6. Check if CPU supports the features we require
7. Initialize APIC
8. Initialize LAPIC
9. Initialize GDT
10. Disable PIC
11. Initialize I/O APIC
12. Initialize IDT
13. Initialize ISR
14. Initialize UART with interrupts
15. Initialize syscalls
16. Initialize virtual memory
17. Initialize kernel heap
18. Move already initialized data from stack to global heap
19. Setup device manager
20. Mount RAM disk (root FS)
21. Register UART as a device in device tree
22. Initialize VGA screen
23. Initialize virtual keyboard driver
24. Initialize PS/2 driver
25. Initialize process manager
26. Start application processors (not working)
27. Enter usermode and execute application defined


## Known bugs

1. Files created through e2tools will NOT be created with root-permissions



#!/bin/bash

user=$(id -nu)
uid=$(id -u)
group=$(id -gn)
gid=$(id -g)

printf "root:0\n${user}:${uid}\n" > users
printf "root:0\n${group}:${gid}\n" > groups

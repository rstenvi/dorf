/**
* Read-only file system used for initial boot-up, does not support directories
* and filenames have a maximum length of 15 + 0x00 terminatoring character.
*/


#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <libgen.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#define FILENAME_MAXLEN 16

#define INITFS_MAGIC 0x494E4954

typedef struct _initfs_header	{
	uint32_t magic;
	uint32_t entries;
} __attribute__((packed)) initfs_header;

typedef struct _initfs_entry	{
	uint32_t offset, flength;
	char name[FILENAME_MAXLEN];
} __attribute__((packed)) initfs_entry;

int main(int argc, char* argv[])	{
	int i;
	uint32_t offset = sizeof(initfs_header) + (sizeof(initfs_entry)*(argc-1));
	struct stat st;
	initfs_header header;
	header.magic = 0x11223344;
	header.entries = argc-1;
	initfs_entry* entries = malloc(sizeof(initfs_entry)*(argc-1));
	for(i = 1; i < argc; i++)	{
		char* fname = basename(argv[i]);
		if(strlen(fname) >= FILENAME_MAXLEN)	{
			printf("'%s' has too long filename\n", argv[i]);
			return 1;
		}
		memset(entries[i-1].name, 0x00, FILENAME_MAXLEN);
		strncpy(entries[i-1].name, fname, FILENAME_MAXLEN-1);
		stat(argv[i], &st);
		entries[i-1].flength = st.st_size;
		entries[i-1].offset = offset;
//		printf("%s -> %i length %i\n", fname, offset, st.st_size);
		offset += st.st_size;
	}

	int ifd;
	int ofd = open("initfs.img", O_WRONLY | O_CREAT, 0644);
	if(ofd == -1)	perror("open");
	if(write(ofd, &header, sizeof(initfs_header)) <= 0)	perror("write");
	if(write(ofd, entries, sizeof(initfs_entry) * (argc-1)) <= 0)	perror("write");
	for(i = 1; i < argc; i++)	{
		int len = entries[i-1].flength;
		char* buf = malloc(len);
		ifd = open(argv[i], O_RDONLY);
		if(ifd == -1)	perror("open");
		int in = read(ifd, buf, len);
		if(in != len)	{
			printf("Read %i, expected %i\n", in, len);
			return 1;
		}
		if(write(ofd, buf, len) <= 0)	perror("write");
		free(buf);
	}
	free(entries);
}

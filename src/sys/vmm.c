
/**
* About recursive paging
* - The laste PDE contains the physical access of the page directory
* - Since each PDE covers 4MB of virtual memory, the last PDE covers 0xFFC00000
* and above
*/


#include "sys/kernel.h"
#include "sys/vmm.h"
#include "sys/pmm.h"
#include "sys/process.h"

#include "hal/hal.h"

extern cpu_info cpus[];

/**
* Virtual address to the directory which is currently in use.
* This is the last entry which is mapped to the page directory
*/
uint32_t* dir_virtual = (uint32_t*)0xFFFFF000;


uint32_t vmm_handle_page_fault(Registers* regs);

uint32_t* vmm_clone_kernel_dir(uint32_t* vmem)	{
	memcpy(vmem, dir_virtual, 256*4);
	vmem[1023] = dir_virtual[1023];
	return vmem;
}

uint32_t* vmm_clone_current_dir(uint32_t* vmem, uint32_t phys)	{
	int i, j;

	// Kernel directory is copied directly
	// Access is controlled using spinlocks
	for(i = 0; i < 256; i++)	{
		vmem[i] = dir_virtual[i];
	}
	for(i = 256; i < 1023; i++)	{
		if(dir_virtual[i] & X86_PAGEDIR_PRESENT)	{
			if( (dir_virtual[i] & X86_PAGE_WRITABLE) )	{
				dir_virtual[i] &= ~(X86_PAGE_WRITABLE);
				dir_virtual[i] |= X86_PAGE_CLONED;
			}
			pmm_addref( (void*)(dir_virtual[i] & 0xFFFFF000));
		}
		vmem[i] = dir_virtual[i];
	}

	// Must always map last directory entry to this directory
	vmem[1023] = phys | X86_PAGEDIR_PRESENT | X86_PAGEDIR_WRITABLE;
	return vmem;
}


void vmm_reclaim_userspace()	{
	int i, j;
	for(i = 256; i < 1023; i++)	{
		if(dir_virtual[i] & X86_PAGEDIR_PRESENT)	{

			// If the page directory is marked as cloned, page tables have not
			// been copied and we can just ignore them
			if(dir_virtual[i] & X86_PAGE_CLONED)	{
				kprintf(K_DEBUG, "Freeing %x i = %i\n", (void*)(dir_virtual[i] & 0xFFFFF000), i );
				pmm_free( (void*)(dir_virtual[i] & 0xFFFFF000) );
				dir_virtual[i] = 0;
				continue;
			}
			uint32_t* ptable = (uint32_t*)(0xFFC00000 + (i*KB4));
			for(j = 0; j < 1024; j++)	{
				if(ptable[j] & X86_PAGE_PRESENT && (ptable[j] & 0xFFFFF000) != 0x00)	{
					kprintf(K_DEBUG, "VMM Reclaim: %i, %i\n", i, j);
					int ref = pmm_free( (void*)(ptable[j] & 0xFFFFF000));
					if(ref == 0)
						ptable[j] = 0;
				}
			}
			pmm_free( (void*)(dir_virtual[i] & 0xFFFFF000));
			dir_virtual[i] = 0;
		}
	}
	kprintf(K_DEBUG, "VMM: Reclaimed dir dirtable @ %x\n", (dir_virtual[1023] & 0xFFFFF000));
}


static inline uint32_t* vmm_get_physical_page()	{
	uint32_t* ret = (uint32_t*)pmm_alloc_first();
	memset(ret, 0x00, KB4);
	return ret;
}

// diri = pagetable && pagei = page
#define ADDR2INDEX(addr,diri,pagei)\
	addr &= ~(0xFFF);\
	diri = addr / MB4;\
	pagei = (addr % MB4) / KB4





addr_t vmm_init()	{
	// Allocate space for directory and 1 page (1st 4MB)
	addr_t* pdir = vmm_get_physical_page();
	uint32_t* ptable = vmm_get_physical_page();
	kprintf(K_DEBUG, "VMM: Kernel dir @ 0x%x | ptable @ 0x%x\n", pdir, ptable);


	// TODO: Check flags here
	// Identity map 1st 4MB
	int i;
	for(i = 0; i < 1024; i++)	{
		ptable[i] = (i*KB4);
		ptable[i] |= X86_PAGE_PRESENT | X86_PAGE_WRITABLE;
	}

	// Map in the LAPIC address space
	ptable[LAPIC_PHYS_VIRT_ADDR/KB4] = 0xFEE00000 |
		X86_PAGE_PRESENT | X86_PAGE_WRITABLE | X86_PAGE_CACHE_DIS;
	lapic_new_address(LAPIC_PHYS_VIRT_ADDR);

	// Mark first 4 physical MB as taken
	pmm_mark_mem_taken(0, MB4);

	// TODO: Can reclaim LAPIC addr, as that is not identity mapped


	// TODO: Check flags here
	// Map in first 4MB
	pdir[0] = (uint32_t)ptable |
		X86_PAGEDIR_PRESENT | X86_PAGEDIR_WRITABLE;

	// Map index on itself
	// TODO: Could also have this as second 4MB block, makes more sense when I'm
	// in the lower half
	pdir[1023] = (uint32_t)pdir |
		X86_PAGEDIR_PRESENT | X86_PAGEDIR_WRITABLE;

	register_interrupt_handler(14, vmm_handle_page_fault);

	paging_enable(pdir);

	return (addr_t)pdir;
}


int vmm_map_page(uint32_t phys_addr, uint32_t virt_addr, uint32_t acl)	{
	kprintf(K_DEBUG, "VMM: Mapping 0x%x to 0x%x\n", virt_addr, phys_addr);

	// Get indexes and frame of virtual address
	uint32_t diri, pagei;
	ADDR2INDEX(virt_addr, diri, pagei);

	// If directory entry is present
	if(dir_virtual[diri] & X86_PAGEDIR_PRESENT)	{
		uint32_t* ptable = (uint32_t*)(0xFFC00000 + (diri*KB4));
		if( (ptable[pagei] & X86_PAGE_PRESENT))	{
			kprintf(K_WARNING, "Tried to use taken page 0x%x\n", virt_addr);
			return VMM_ERR_PAGE_IN_USE;
		}
		else	{
			ptable[pagei] = phys_addr | X86_PAGE_PRESENT | acl;
			flush_tlb_entry( virt_addr );
		}
	}
	else	{
		kprintf(K_DEBUG, "VMM: Dir NOT present %i\n", diri);
		// Directory entry is NOT present

		// Get page and map it in
		uint32_t* new_ptable = pmm_alloc_first();
		dir_virtual[diri] = (uint32_t)new_ptable | X86_PAGEDIR_PRESENT | X86_PAGEDIR_WRITABLE | X86_PAGEDIR_USER;
		flush_tlb_entry((uint32_t)dir_virtual);

		// Get virtual address and zero it
		// NB! Other functions assume it will be zeroed, so
		// don't optimize this.
		uint8_t* ptable = (uint8_t*)(0xFFC00000 + (diri*KB4));
		kprintf(K_DEBUG, "VMM: Zeroing 0x%x DIRI: 0x%x -> 0x%x\n", ptable, (diri*KB4), new_ptable);
		flush_tlb_entry( (uint32_t)ptable );
		memset(ptable, 0x00, KB4);
		return vmm_map_page(phys_addr, virt_addr, acl);
	}
	return VMM_SUCCESS;
}

// Caller is responsible to freeing the physical page
int vmm_unmap_page(uint32_t vaddr)	{
	// Get indexes and frame of virtual address
	uint32_t diri, pagei;
	ADDR2INDEX(vaddr, diri, pagei);
	kprintf(K_DEBUG, "VMM: UNMAP: 0x%p %i, %i\n", vaddr, diri, pagei);

	if(dir_virtual[diri] & X86_PAGEDIR_PRESENT)	{
		uint32_t* ptable = (uint32_t*)(0xFFC00000 + (diri*KB4));
		if(ptable[pagei] & X86_PAGE_PRESENT)	{
			ptable[pagei] = 0;
			flush_tlb_entry((uint32_t)ptable);
		}

		// Check if entire directory entry is empty
		int i;
		for(i = 0; i < 1024; i++)	{
			if(ptable[i] & X86_PAGE_PRESENT)	break;
		}
		if(i >= 1024)	{
			pmm_free( (void*)(dir_virtual[diri] & 0xFFFFF000));
			dir_virtual[diri] = 0;
			flush_tlb_entry((uint32_t)dir_virtual);
		}
	}
	else	{
		return VMM_ERR_NO_PAGEDIR_ENTRY;
	}
	flush_tlb_entry(vaddr);

	return VMM_SUCCESS;
}


addr_t vmm_get_mapped_page(addr_t vaddr, uint32_t flags)	{
	addr_t phys_addr = (addr_t)pmm_alloc_first();
	ASSERT(phys_addr != 0,"Unable to alloc physical page");

	if(vmm_map_page(phys_addr, vaddr, flags))
		PANIC("Unable to map physical and virtual page");
	flush_tlb_entry(vaddr);
	return phys_addr;
}

void vmm_switch_pdir(uint32_t* pdir)	{
	load_page_dir_addr( (uint32_t)pdir);
}

uint32_t vmm_virt2phys(uint32_t addr)	{
	uint32_t diri, pagei;
	ADDR2INDEX(addr, diri, pagei);
	if(dir_virtual[diri] & X86_PAGEDIR_PRESENT)	{
		uint32_t* ptable = (uint32_t*)(0xFFC00000 + (diri*KB4));
		if(ptable[pagei] & X86_PAGE_PRESENT)	{
			return (ptable[pagei] & 0xFFFFF00);
		}
	}
	// TODO: return 0 to indicate error when more stabilized
	//PANIC("Page not present\n");

	return 0;
}

void vmm_clone_entries(uint32_t* table, uint32_t* to, int start, int end)	{
	int i;
	for(i = start; i < end; i++)	{
		if(table[i] & X86_PAGE_PRESENT)	{
			if(table[i] & X86_PAGE_WRITABLE)	{
				table[i] &= ~(X86_PAGE_WRITABLE);
				table[i] |= X86_PAGE_CLONED;
			}
			pmm_addref( (void*)(table[i] & 0xFFFFF000));
		}
		to[i] = table[i];
	}
}

uint32_t vmm_handle_page_fault(Registers* regs)	{
	print_regs(regs, K_DEBUG);

	// Get the address that caused the exception
	uint32_t addr, addr2;
	read_cr2(addr);
	addr2=addr;
	kprintf(K_DEBUG, "Page fault @0x%x proc: %i\n", addr, current_pid());

	// Check if address belongs to the process address space
	if((regs->err_code & X86_PF_PROTECT) == 0)	{
		// Fault was caused by a non-present page
		PANIC("PF: Page NOT present");
	}

	else if((regs->err_code & (X86_PF_WRITE_CLONED)) == (X86_PF_WRITE_CLONED))	{
		kprintf(K_DEBUG, "\tPage protection vialoation\n");
		uint32_t diri, pagei;
		ADDR2INDEX(addr2, diri, pagei);
		bool fixed=false;

		if(dir_virtual[diri] & X86_PAGE_CLONED)	{
			if(pmm_getref((void*)(dir_virtual[diri] & 0xFFFFF000)) > 1)	{
				// Page directory has been cloned
				kprintf(K_DEBUG, "\tCopying page dir\n");

				uint32_t a = (uint32_t)vmm_get_mapped_page( VMM_TMP_BLOCK_START, (X86_PAGEDIR_PRESENT | X86_PAGEDIR_WRITABLE) );
				uint32_t* aa = (uint32_t*)VMM_TMP_BLOCK_START;
				uint32_t* ab = (uint32_t*)(0xFFC00000 + (diri*KB4));
				vmm_clone_entries( ab, aa, 0, 1024);

				// Reduce count on original
				pmm_free( (void*)(dir_virtual[diri] & 0xFFFFF000) );
				dir_virtual[diri] = (a & 0xFFFFF000) | (X86_PAGEDIR_PRESENT | X86_PAGEDIR_WRITABLE | X86_PAGEDIR_USER);

				vmm_unmap_page(VMM_TMP_BLOCK_START);
				flush_tlb_entry( (uint32_t)(dir_virtual + (KB4 * diri)) );
				flush_tlb_entry( (uint32_t)(VMM_TMP_BLOCK_START) );
				flush_tlb_entry( (uint32_t)(dir_virtual) );
			}
			else	{
				kprintf(K_DEBUG, "\tNo more references to physical page (page directory)\n");
				// Memory has already been copied, we can simply remove clone flag
				dir_virtual[diri] &= ~(X86_PAGE_CLONED);
				dir_virtual[diri] |= X86_PAGE_WRITABLE;
				flush_tlb_entry( (uint32_t)(dir_virtual + (KB4 * diri)) );
				flush_tlb_entry( (uint32_t)(dir_virtual) );

				// We only clone directory entries, so if we fix this, the page
				// table should not have been cloned.
				fixed=true;
			}
		}
		// If page dir was a problem, page table will also be a problem, so this
		// will execute as well, but this may also execute when page dir was not
		// a problem
		uint32_t* ptable = (uint32_t*)(0xFFC00000 + (diri*KB4));
		if(ptable[pagei] & X86_PAGE_CLONED)	{
			if(pmm_getref((void*)(ptable[pagei] & 0xFFFFF000)) > 1)	{
				kprintf(K_DEBUG, "\tCopying page table\n");
				uint32_t addr_blk = ((uint32_t)(addr) & 0xFFFFF000);

				uint32_t a = vmm_get_mapped_page( VMM_TMP_BLOCK_START, (X86_PAGE_PRESENT | X86_PAGE_WRITABLE) );
				uint32_t* aa = (uint32_t*)VMM_TMP_BLOCK_START;
				//uint32_t* ab = (uint32_t*)(ptable);
				uint32_t* ab = (uint32_t*)(addr_blk);
				memcpy(aa, ab, KB4);
				vmm_unmap_page(VMM_TMP_BLOCK_START);

				pmm_free( (void*)(ptable[pagei] & 0xFFFFF000) );
				ptable[pagei] = (a & 0xFFFFF000) | (X86_PAGE_PRESENT | X86_PAGE_WRITABLE | X86_PAGE_USER);
				flush_tlb_entry( (uint32_t)(VMM_TMP_BLOCK_START) );
				flush_tlb_entry( (uint32_t)(ptable) );
				flush_tlb_entry( (uint32_t)(addr_blk) );
				flush_tlb_entry( (uint32_t)(addr) );
				fixed=true;
			}
			else	{
				kprintf(K_DEBUG, "\tNo more refeferences to physical page (page table)\n");
				ptable[pagei] &= ~(X86_PAGE_CLONED);
				ptable[pagei] |= X86_PAGE_WRITABLE;
				fixed=true;
			}
		}
		if(!fixed)	{
			PANIC("PF: Unable to fix\n");
		}
	}
	else	{
		// If this has happened in user-mode, we need to seg-fault the process
		if((regs->err_code & X86_PAGE_USER) != 0)	{
			kprintf(K_DEBUG, "Unfixable page fault caused by user mode\n");
			segfault(regs);	// Segfault process
		}
		else	{
			PANIC("Unable to handle cause of PF\n");
		}
	}
	kprintf(K_DEBUG, "\tPage fault fixed\n");
	return 0;
}


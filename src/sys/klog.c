
#include "sys/kernel.h"
#include "sys/klog.h"
#include "hal/hal.h"

const char* klog_levels[K_SIZE] = {
	"[DEBUG] ",
	"[INFO] ",
	"[INFO] ",
	"[WARNING] ",
	"[ERROR] ",
	"[USER] ",
	"[FATAL] "
};

int vprintf_help(unsigned c, void **ptr)	{
	(void)ptr;
	KPUTC(c);	// Output device is defined at compile time (config.h)
	return 0;
}

void log_write(const char* str)	{
	const char* s = str;
	while(*s)	{
		KPUTC(*s);
		s++;
	}
}


int kprintf(enum KM_Level kl, const char *fmt, ...) {
	ASSERT(kl < K_SIZE, "Log level is not valid");
	if((int)kl >= DEBUG)    {
		log_write(klog_levels[kl]);
		va_list args;
		int rv;
		va_start(args, fmt);
		rv = do_printf(fmt, args, vprintf_help, NULL);
		va_end(args);
		return rv;
	}
	return 0;
}



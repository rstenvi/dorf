/**
* \pmm.c
* Physical memory manager.
* A bitmap is used to keep track of pages. Each block has a size of PMM_BLK_SZ
* bytes, since it is meant to support paging, that size should be 4096 (4 KB).
* Since all the allocation and deallocation works with blocks and the
* initialization doesn't care about the block size, any block size should work.
* \todo Clean up code and variables.
* \todo Make a clean API
* \todo Document all parts
*/
/**
* \addtogroup pmm
* @{
*/

#include "sys/kernel.h"
#include "sys/pmm.h"
#include "sys/vmm.h"
#include "sys/multiboot1.h"

#include "lib/stdio.h"

#define PMM_BLK_SZ 4096


#define align_downwards(a,b) a-=(a%(b))
#define align_upwards(a,b) if(a%b!=0) {a=((a/b)*b)+b;}

#define MAX_REF_BITS 8
#define MAX_REF_COUNT ((2 << MAX_REF_BITS) - 1)


/**
* The number of blocks in the system and also the number of MAX_REF_BITS
* allocated for pmm_refcount to mark available space.
*/
uint32_t pmm_bitmap_sz = 0;

/**
* Reference count for all physical pages
*/
uint8_t* pmm_refcount = NULL;



//--------------- Internal function definitions ---------------------------

/**
* Get highest physical address.
* \param[in,out] mmap The memory map we get from Grub
* \param[in] len The length of mmap which we also get from Grub.
* \return The highest physical address in the system.
*/
uint32_t pmm_get_max_space(multiboot_mmap* mmap, uint32_t len);






//------------------- Public API function implementations ------------------

uint32_t init_pmm(multiboot_mmap* mmap, uint32_t len)	{
	// The ref count must take up a whole byte
	ASSERT(MAX_REF_BITS == 8, "MAX_REF_BITS is invalid");

	// Make a pointer copy of nmap because pmm_get_max_space might change it
	multiboot_mmap* copy = mmap;
	uint32_t max = pmm_get_max_space(copy, len);
	copy = mmap;

	// Number of blocks we need, this is rounded down because we don't want to
	// use the løast block if it's not a full block size.
	pmm_bitmap_sz = (max / PMM_BLK_SZ);
	uint32_t pmm_bytes = pmm_bitmap_sz;

	/*
	uint32_t pmm_bytes = pmm_bitmap_sz/8;
	if(pmm_bitmap_sz%8 != 0)	{
		pmm_bytes++;
	}
	*/

	// Calculate how many blocks we need to allocate to store refcount
	/*
	uint32_t blocks_bitmap = pmm_bitmap_sz / PMM_BLK_SZ;
	if( (pmm_bitmap_sz % PMM_BLK_SZ) != 0)	{
		blocks_bitmap++;
	}
	*/

	// Calculate how many blocks we need to store ref count
	uint32_t blocks_refcount = pmm_bitmap_sz / PMM_BLK_SZ;
	if( (pmm_bitmap_sz % PMM_BLK_SZ) != 0)	blocks_refcount++;

	kprintf(K_DEBUG,
		"PMM Highest address @0x%x | Blocks %i | Blocks refcount %i\n",
		max, pmm_bitmap_sz, blocks_refcount
	);


	// Find contigous area in memory to store our reference count
	while((uint32_t)copy < (uint32_t)mmap + len)	{
		if(copy->type == 1 && (copy->length_l / PMM_BLK_SZ) >= blocks_refcount)	{
			// We need to make sure the base is aligned
			uint32_t base = copy->base_addr_l;
			align_upwards(base,PMM_BLK_SZ);

			// Must also align the end address
			uint32_t end = copy->base_addr_l + copy->length_l;
			align_downwards(base,PMM_BLK_SZ);

			if( (end - base) / PMM_BLK_SZ >= blocks_refcount)	{
				pmm_refcount = (uint8_t*)base;
				uint32_t i = 0;
				for(i = 0; i < pmm_bytes; i++)	{
					*(pmm_refcount+i) = 0x00;
				}
				// Must mark the memory region with bitmap as taken
				uint32_t start = (base/PMM_BLK_SZ);
				for(i = start; i < start + blocks_refcount; i++)	{
					pmm_refcount[i] = 0x01;
				}
				break;
			}
		}
		copy = (multiboot_mmap*)((uint32_t)copy + (copy->size + 4));
	}
	copy = mmap;

	// Mark all reserved memory as taken
	uint32_t expected = 0;
	while((uint32_t)copy < (uint32_t)mmap + len && (copy->base_addr_l < max-1))	{
		// First check if there is a gap in the specification, if there is, that
		// memory is taken.
		if(copy->base_addr_l > expected/* && copy->base_addr_l < max-1*/)	{
			align_downwards(expected,PMM_BLK_SZ);

			while(expected < copy->base_addr_l)	{
				pmm_refcount[expected/PMM_BLK_SZ] = 0x01;
				expected += PMM_BLK_SZ;
			}
		}

		// If this memory is taken and below our max
		if(copy->type != 1 && (copy->base_addr_l + copy->length_l) < max-1)	{
			// Mark this memory as taken
			uint32_t base = copy->base_addr_l;
			align_downwards(base,PMM_BLK_SZ);
			while(base < (copy->base_addr_l + copy->length_l))	{
				pmm_refcount[base/PMM_BLK_SZ] = 0x01;
				base += PMM_BLK_SZ;
			}
		}
		expected = copy->base_addr_l + copy->length_l;
		copy = (multiboot_mmap*)((uint32_t)copy + (copy->size + 4));
	}
	// 0 is error-value, so is always marked as used, might actually be used for
	// bitmap
	pmm_refcount[0] = 0x01;

	return max;
}

uint8_t pmm_addref(void* a)	{
	uint32_t blk = (uint32_t)a/PMM_BLK_SZ;
	pmm_refcount[blk] += 1;
	kprintf(K_DEBUG, "PMM ADD refCount %p is %i\n", a, pmm_refcount[blk]);
	return pmm_refcount[blk];
}

uint8_t pmm_getref(void* a)	{
	uint32_t blk = (uint32_t)a/PMM_BLK_SZ;
	return pmm_refcount[blk];
}

bool pmm_is_taken(uint32_t block)	{
	return pmm_refcount[block] != 0;
}


void pmm_mark_mem_taken(uint32_t start, uint32_t end)	{
	align_downwards(start, PMM_BLK_SZ);
	align_upwards(end, PMM_BLK_SZ);

	uint32_t i = start / PMM_BLK_SZ, j = end / PMM_BLK_SZ;
	for(; i < j; i++)	{
		pmm_refcount[i] += 1;
	}
}



// Is slow when many of the first are taken, there are several ways around
// that
void* pmm_alloc_first()	{
	uint32_t i = 1;
	for(i = 1; i< pmm_bitmap_sz; i++)	{
		if(pmm_refcount[i] == 0)	{
			pmm_refcount[i] += 1;
			kprintf(K_DEBUG, "PMM: ALLOC %i %x\n", i, (i*KB4));
			return (void*)(i*PMM_BLK_SZ);
		}
	}
	return NULL;
}

int pmm_free(void* block)	{
	uint32_t blk = (uint32_t)block/PMM_BLK_SZ;
	kprintf(K_DEBUG, "PMM free @ 0x%p | %i | REF: %i\n", block, blk, pmm_refcount[blk]);
	ASSERT(pmm_refcount[blk] > 0, "PMM free cannot free non-taken block");
	pmm_refcount[blk] -= 1;
	return pmm_refcount[blk];
}




void* pmm_alloc_first_n_blocks(uint32_t n)	{
	uint32_t found = 0, i;
	for(i = 1; i < pmm_bitmap_sz; i++)	{
		if(pmm_refcount[i] == 0x00)	{
			found++;
			if(found == n)	{
				for(; found > 0; found--)	{
					pmm_refcount[i-(found-1)] += 1;
				}
				return (void*)((i-(n-1))*PMM_BLK_SZ);
			}
		}
		else	found = 0;
	}
	return NULL;
}





//------------------- Internal function implementation ------------------------

uint32_t pmm_get_max_space(multiboot_mmap* mmap, uint32_t len)	{
	uint32_t max = 0;
	uint32_t mmap_addr = (uint32_t)mmap;
	while((uint32_t)mmap < mmap_addr + len)	{
		if(mmap->type == 1)	{
			max = ((uint32_t)mmap->base_addr_l + (uint32_t)mmap->length_l);
		}
		mmap = (multiboot_mmap*)((uint32_t)mmap + (mmap->size + 4));
	}
	return max;
}




/** @} */	// pmm

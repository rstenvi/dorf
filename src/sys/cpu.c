/**
* \cpu.c
* Code for starting the remaining CPUs
*/

#include "sys/kernel.h"
#include "sys/pmm.h"
#include "sys/vmm.h"
#include "hal/hal.h"
#include "drv/uart.h"

#include "lib/stdio.h"

#include "sizes.h"


extern cpu_info cpus[];
extern int num_cpus;

// In task.s
extern void task_enter_usermode();

void cpu_ap_enter();
void cpu_common_main(int bootap);

void cpu_start_aps(cpu_common* data)	{
	int i;
	uint32_t* code = (uint32_t*)0x7000;
	uint32_t curr_stack = KERNEL_STACK_TOP;
	for(i = 0; i < num_cpus; i++)	{

		// TODO: Just for the boot CPU
		cpus[i].kstack = (uint32_t*)(KERNEL_STACK_TOP - KB4);
		if(cpus[i].boot_cpu == true)	continue;
	}
	cpu_common_main(1);
}


__attribute__((optimize("align-functions=4096")))
void cpu_ap_enter()	{
	kprintf(K_DEBUG, "CPU: New AP\n");
	gdt_install();
	lapic_install(NULL);
	cpu_common_main(0);
}


void cpu_common_main(int bootap)	{
	idt_enable();
	xchg(&cpu->started, 1);
	kprintf(K_HIGH_INFO, "Reached main with CPU %i\n", cpus[lapic_cpuid()].id);

	if(bootap == 0)	{
		uint16_t* virt = (uint16_t*)GB1;
		virt[0] = 0x80cd;
		virt[1] = 0x80cd;
		virt[2] = 0xfeeb;
	}
	enable_int();
	task_enter_usermode();

	while(1);
}



void cpu_print_info(cpu_info* c)	{
	kprintf(
		K_DEBUG,
		"ID: %i | started: %i | CLI: %i\n",
		c->id, c->started, c->num_cli
	);
}

void cpu_print_all()	{
	int i;
	for(i = 0; i < num_cpus; i++)	{
		cpu_print_info(&cpus[i]);
	}
}


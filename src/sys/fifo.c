
#include "sys/kernel.h"
#include "sys/allocate.h"
#include "sys/fifo.h"


struct _Fifo	{
	uint16_t first, last;
	void** elem;
	uint16_t size;
};


void remove_middle(Fifo* f, int ind);


//------------------------- Public functions ------------------------- //

Fifo* fifo_create(uint16_t sz)	{
	Fifo* ret = (Fifo*)heap_malloc(sizeof(Fifo));
	ret->elem = (void*)heap_malloc(sizeof(void*) * sz);

	ret->size = sz;
	ret->first = ret->last = 0;

	kprintf(K_DEBUG, "FIFO init size: %i\n", ret->size);

	return ret;
}


int fifo_push(Fifo* f, void* ptr)	{
	if(f->last >= (f->size-1))	{
		if(f->last == f->first)	{
			f->last = f->first = 0;
		}
		else if(f->first == 0)	{
			PANIC("FIFO size is not big enough to hjold data\n");
		}
		else	{
			kprintf(K_DEBUG, "FIFO: moving first: %i last: %i\n", f->first, f->last);
			int i;
			for(i = f->first; i < f->last; i++)	f->elem[i - f->first] = f->elem[i];

			f->last = (f->last - f->first);
			f->first = 0;
		}
	}

	f->elem[f->last++] = ptr;
	return 0;
}

void* fifo_pop(Fifo* f)	{
	if(f->first < f->last)	{
		return f->elem[f->first++];
	}
	return NULL;
}

int fifo_elements(Fifo* f)	{
	return f->last - f->first;
}



void* fifo_find(Fifo* f, fifo_compare cmp, void* s)	{
	int i = 0;
	void* ret = NULL;
	for(i = f->first; i < f->last; i++)	{
		if(cmp(f->elem[i], s) == 0)	{
			ret = f->elem[i];
			remove_middle(f, i);
			break;
		}
	}
	return ret;
}


//--------------- Internal functions ----------------------- //

void remove_middle(Fifo* f, int ind)	{
	int i;
	for(i = ind; i < f->last-1; i++)	{
		f->elem[i] = f->elem[i+1];
	}
	f->last--;
}




#include "sys/syscall.h"
#include "sys/kernel.h"
#include "hal/isr.h"
#include "hal/hal.h"

uint32_t syscall_handler(Registers* regs);


void syscall_init()	{
	register_interrupt_handler(0x80, syscall_handler);
}


uint32_t syscall_handler(Registers* regs)	{
	switch(regs->eax)	{
		case SYSCALL_EXECVE:	return exec(regs);	break;
		case SYSCALL_EXIT:		return exit(regs);	break;
		case SYSCALL_WAIT:		return wait(regs);	break;
		case SYSCALL_FORK:		return fork(regs); break;
		case SYSCALL_GETPID:	return getpid(regs); break;
		case SYSCALL_YIELD:		return yield(regs); break;
		case SYSCALL_NICE:		return nice(regs); break;
		case SYSCALL_OPEN:		return open(regs); break;
		case SYSCALL_FSTAT:		return fstat(regs); break;
		case SYSCALL_CLOSE:		return close(regs);	break;
		case SYSCALL_LSEEK:		return lseek(regs);	break;
		case SYSCALL_READ:		return read(regs); break;
		case SYSCALL_WRITE:		return write(regs);	break;
		case SYSCALL_GETC:		return getc(regs);	break;
		case SYSCALL_PUTC:		return putc(regs);	break;
		case SYSCALL_POWEROFF:	return poweroff(regs);	break;
		case SYSCALL_MALLOC:	return malloc(regs);	break;
		case SYSCALL_REALLOC:	return realloc(regs);	break;
		case SYSCALL_FREE:		return free(regs);	break;
		case SYSCALL_HALT_CPU:	return halt_cpu(regs);	break;
		case SYSCALL_GETUID:	return getuid(regs);	break;
		case SYSCALL_GETGID:	return getgid(regs);	break;
		case SYSCALL_SETUID:	return setuid(regs);	break;
		case SYSCALL_SETGID:	return setgid(regs);	break;
		case SYSCALL_SETEUID:	return seteuid(regs);	break;
		case SYSCALL_SETEGID:	return setegid(regs);	break;
		default:
			kprintf(K_ERROR, "Invalid syscall %i\n", regs->eax);
			PANIC("");
			break;
	}

	return 0;
}




#include "sys/kernel.h"
#include "sys/process.h"
#include "sys/elf.h"
#include "sys/vmm.h"
#include "sys/dev.h"

typedef uint32_t Elf32_Addr;
typedef uint16_t Elf32_Half;
typedef uint32_t Elf32_Off;
typedef uint32_t Elf32_Sword;
typedef uint32_t Elf32_Word;

// Types
#define ET_NONE	 0
#define ET_REL		1
#define ET_EXEC	 2
#define ET_DYN		3
#define ET_CORE	 4
#define ET_LOPROC 0xff00
#define ET_HIPROC 0xffff

#define EM_NONE	0
#define EM_M32	 1
#define EM_SPARC 2
#define EM_386	 3
#define EM_68K	 4
#define EM_88K	 5
#define EM_860	 7
#define EM_MIPS	8


#define EV_NONE		0
#define EV_CURRENT 1

#define ELFCLASSNONE 0
#define ELFCLASS32	 1
#define ELFCLASS64	 2

#define ELFDATANONE 0
#define ELFDATA2LSB 1
#define ELFDATA2MSB 2


#define EI_MAGIC 0x464C457F	// \x7F ELF


#define PF_X 1
#define PF_W 2
#define PF_R 4
#define PF_MASKPROC 0xf0000000

typedef struct _Elf	{
	uint32_t e_ident;
	uint8_t arch, endianness, version, os_abi, abi_version, unused[7];
	Elf32_Half type, machine;
	Elf32_Word version2;
	addr_t entry, phoff, shoff;
	uint32_t flags;
	uint16_t ehsize, phentsize, phnum, shentsize, shnum, shstrndx;
} Elf;

typedef struct _Elf_progh	{
	Elf32_Word p_type;
	Elf32_Off p_offset;
	Elf32_Addr p_vaddr;
	Elf32_Addr p_paddr;
	Elf32_Word p_filesz;
	Elf32_Word p_memsz;
	Elf32_Word p_flag;
	Elf32_Word p_align;
} Elf_progh;

typedef struct _Elf_shdr	{
	Elf32_Word sh_name;
	Elf32_Word sh_type;
	Elf32_Word sh_flags;
	Elf32_Addr sh_addr;
	Elf32_Off sh_offset;
	Elf32_Word sh_size;
	Elf32_Word sh_link;
	Elf32_Word sh_info;
	Elf32_Word sh_addralign;
	Elf32_Word sh_entsize;
} Elf_shdr;




bool elf_valid(Elf* elf)	{
	kprintf(K_DEBUG, "Elf Signature 0x%x\n", elf->e_ident);
	if(elf->e_ident != EI_MAGIC)	{
		kprintf(K_FATAL, "Elf e ident is invalid %x\n", elf->e_ident);
		PANIC("Elf e ident is invalid\n");
	}
	return true;
}



int32_t progheader2mem(Elf_progh* p, uint32_t devid)	{
	uint32_t blks = p->p_filesz / 4096, i;
	if(p->p_filesz % 4096 != 0)	blks++;
	uint32_t flag = X86_PAGE_PRESENT | X86_PAGE_USER | X86_PAGE_WRITABLE;

	kprintf(K_DEBUG, "Progheader V 0x%x S 0x%x MS 0x%x\n",
		p->p_vaddr, p->p_filesz, p->p_memsz);

	// TODO: No execute not set up
	if(p->p_flag && PF_X)	{}
	/*if(p->p_flag && PF_W) */flag |= X86_PAGE_WRITABLE;

	for(i = 0; i < blks; i++)	{
		kprintf(K_DEBUG, "ELF: Page: 0x%x | PROT 0x%x\n", p->p_vaddr + (i*4096), flag);
		vmm_get_mapped_page( p->p_vaddr + (i*4096), flag );
	}
	kprintf(K_DEBUG, "ELF: FILESZ %i seeking to %i\n", p->p_filesz, p->p_offset);
	int32_t res = dev_lseek(devid, p->p_offset, DEVMGR_OPEN_SEEK_ABS);
	if(res < 0)		return -3;

	uint32_t read=0;
	res = dev_read(devid, (uint8_t*)(p->p_vaddr), p->p_filesz, &read);
	if(res != DEV_OK)		return -1;

	kprintf(K_DEBUG, "Inserted prog header\n");
	kprintf(K_DEBUG, "ELF: Page: 0x%x | contents: %x\n", p->p_vaddr, *((uint32_t*)p->p_vaddr));
	return 0;
}


// TODO: Must free memory on error
int32_t elf_load(uint32_t devid)	{
	kprintf(K_DEBUG, "Reading from ID %x\n", devid);
	uint32_t read=0;
	Elf elf;
	Elf_progh progh;

	int32_t res;
	res = dev_read(devid, (uint8_t*)(&elf), sizeof(Elf), &read);
	kprintf(K_DEBUG, "\tres = %i\n", res);
	if(res != DEV_OK || read < (int32_t)sizeof(Elf))		return -1;

	// Check that it is a valid ELF file
	if(elf_valid(&elf) == false)	return -2;
	kprintf(K_DEBUG, "ELF is valid, prog header at %i offset\n", elf.phoff);

	// Read in program header
	res = dev_lseek(devid, elf.phoff, DEVMGR_OPEN_SEEK_ABS);
	if(res < 0)		return -3;
	res = dev_read(devid, (uint8_t*)(&progh), sizeof(Elf_progh), &read);
	if(res != DEV_OK || read < (int32_t)sizeof(Elf_progh))		return -1;

	// Place the code and data into memory
	res = progheader2mem(&progh, devid);
	if(res < 0)	return -4;

	return elf.entry;
}


/**
* \file dev.c
*/

#include "sys/kernel.h"
#include "sys/allocate.h"
#include "hal/hal.h"
#include "dorf.h"

#define DEV_SIZE_BLOCK 16
#define DEV_SIZE_BLOCK_OPENED 32

extern cpu_info cpus[];

int32_t dev_error(void) {
	kprintf(K_ERROR, "Device manager is not supported device\n");
	return DEV_FAILURE_UNSUPPORTED;
}

int dev_fill_device_opened(
	DeviceOpened* dev,
	tdev_read read,
	tdev_write write,
	tdev_putc putc,
	tdev_getc getc,
	tdev_close close,
	void* devcsr)	{

	dev->read = read;
	dev->write = write;
	dev->putc = putc;
	dev->getc = getc;
	dev->close = close;
	dev->devcsr = devcsr;
	dev->offset = 0;

	return 0;
}


Device* dev_alloc_device(const char* path, tdev_open open, tdev_fstat fstat, void* devcsr)	{
	Device* dev = (Device*)heap_malloc(sizeof(Device));

	dev->path = (char*)heap_malloc(strlen(path)+1);
	strcpy(dev->path, path);

	dev->open = open;
	dev->fstat = fstat;

	dev->devcsr = devcsr;
	return dev;
}



int dev_init(DevMgr* dev)	{
	dev->devices = (Device**)heap_malloc(sizeof(Device*) * DEV_SIZE_BLOCK);
	memset(dev->devices, 0x00, (sizeof(Device*) * DEV_SIZE_BLOCK));
	
	dev->opened = (DeviceOpened**)heap_malloc(sizeof(DeviceOpened*) * DEV_SIZE_BLOCK_OPENED);
	memset(dev->opened, 0x00, (sizeof(DeviceOpened*) * DEV_SIZE_BLOCK_OPENED));

	dev->device_entries = DEV_SIZE_BLOCK;
	dev->opened_entries = DEV_SIZE_BLOCK_OPENED;
	return 0;
}

int dev_mount(DevMgr* dev, Device* device)	{
	int i;
	for(i = 0; i < dev->device_entries; i++)	{
		if(dev->devices[i] == NULL)	{
			dev->devices[i] = device;
			return 0;
		}
	}
	PANIC("No more space to allocate devices");
	return -1;
}

Device* dev_umount(DevMgr* dev, const char* path)	{
	int i;
	for(i = 0; i < dev->device_entries; i++)	{
		if(dev->devices[i] != NULL && strcmp(path, dev->devices[i]->path) == 0)	{
			Device* ret = dev->devices[i];
			dev->devices[i] = NULL;
			return ret;
		}
	}
	return NULL;
}

int _dev_append_open(DevMgr* dev, DeviceOpened* open)	{
	int i;
	for(i = 0; i < dev->opened_entries; i++)	{
		if(dev->opened[i] == NULL)	{
			dev->opened[i] = open;
			return i;
		}
	}
	return -1;
}

Device* dev_find_mount(DevMgr* dev, const char* path)	{
	int i;
	int plen = strlen(path);
	int mlen;
	int top_count = 0;
	Device* ret = NULL;
	for(i = 0; i < dev->device_entries; i++)	{
		if(dev->devices[i] != NULL)	{
			const char* tmp = dev->devices[i]->path;
			mlen = strlen(tmp);
			if(mlen <= plen && strncmp(path, tmp, mlen) == 0)	{
				// On exact match, we returm immediately
				if(mlen == plen)	{
					return dev->devices[i];
				}
				// New candidate for best match
				else if(mlen > top_count)	{
					top_count = mlen;
					ret = dev->devices[i];
				}
			}
		}
	}
	return ret;	// Might still be NULL if nothing has been found
}

DevRet dev_open(const char* path, uint32_t mode, int32_t* fd)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	Device* ddev = dev_find_mount(dev, path);
	if(ddev == NULL)	{
		return DEV_NO_MORE_SPACE;
	}

	DeviceOpened* open = (DeviceOpened*)heap_malloc(sizeof(DeviceOpened));
	int fd2 = _dev_append_open(dev, open);
	if(fd2 < 0)	{
		heap_free(open);
		return DEV_NO_MORE_SPACE;
	}
	open->id = fd2;

	// Get path without the leading mount part
	const char* rpath = path + strlen(ddev->path);
	DevRet ret = ddev->open(
		ddev->devcsr,
		rpath,
		mode,
		fd2,
		open
	);
	if(ret != DEV_OK)	{
		heap_free(open);
		dev->opened[fd2] = NULL;
		return ret;
	}

	*fd = fd2;
	return DEV_OK;
}

DevRet dev_fstat(const char* path, Filestat* fstat)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	Device* ddev = dev_find_mount(dev, path);
	if(ddev == NULL)	{
		return DEV_NO_MORE_SPACE;
	}

	// Get path without the leading mount part
	const char* rpath = path + strlen(ddev->path);
	DevRet ret = ddev->fstat(
		ddev->devcsr,
		rpath,
		fstat
	);
	return ret;
}

DevRet dev_read(uint32_t fd, uint8_t* buf, uint32_t maxsize, uint32_t* read)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	if(fd >= 0 && fd <= dev->opened_entries)	{
		DeviceOpened* open = dev->opened[fd];
		if(open == NULL)	return DEV_FAILURE_CALLER;

		DevRet ret = open->read(
			open->devcsr,
			buf,
			maxsize,
			open->offset,
			read
		);
		return ret;
	}
	return DEV_FAILURE_CALLER;
}

DevRet dev_getc(uint32_t fd, uint32_t* ch)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	if(fd >= 0 && fd <= dev->opened_entries)	{
		DeviceOpened* open = dev->opened[fd];
		if(open == NULL)	return DEV_FAILURE_CALLER;

		DevRet ret = open->getc(
			open->devcsr,
			ch
		);
		return ret;
	}
	return DEV_FAILURE_CALLER;
}

DevRet dev_putc(uint32_t fd, uint8_t ch)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	if(fd >= 0 && fd <= dev->opened_entries)	{
		DeviceOpened* open = dev->opened[fd];
		if(open == NULL)	return DEV_FAILURE_CALLER;

		DevRet ret = open->putc(
			open->devcsr,
			ch
		);
		return ret;
	}
	return DEV_FAILURE_CALLER;
}


DevRet dev_write(uint32_t fd, uint8_t* buf, uint32_t len, uint32_t* written)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	if(fd >= 0 && fd <= dev->opened_entries)	{
		DeviceOpened* open = dev->opened[fd];
		if(open == NULL)	return DEV_FAILURE_CALLER;

		DevRet ret = open->write(
			open->devcsr,
			buf,
			len,
			open->offset,
			written
		);
		return ret;
	}
	return DEV_FAILURE_CALLER;
}

DevRet dev_close(uint32_t fd)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	if(fd >= 0 && fd <= dev->opened_entries)	{
		DeviceOpened* open = dev->opened[fd];
		if(open == NULL)	return DEV_FAILURE_CALLER;

		DevRet ret = open->close(
			open->devcsr
		);
		if(ret != DEV_OK)	{
			return DEV_FAILURE_DOWNSTREAM;
		}
		heap_free(dev->opened[fd]);
		dev->opened[fd] = NULL;
		return ret;
	}
	return DEV_FAILURE_CALLER;
}

DevRet dev_lseek(uint32_t fd, int32_t off, uint32_t mode)	{
	DevMgr* dev = &(cpus[lapic_cpuid()].data->dev);
	if(fd >= 0 && fd <= dev->opened_entries)	{
		DeviceOpened* open = dev->opened[fd];
		if(open == NULL)	return DEV_FAILURE_CALLER;
		switch(mode)	{
			case DEVMGR_OPEN_SEEK_ABS:
				open->offset = off;
				break;
			case DEVMGR_OPEN_SEEK_CUR:
				open->offset += off;
				break;
			case DEVMGR_OPEN_SEEK_END:
				PANIC("Not supported yet");
				break;
			default:
				break;
		}
		return DEV_OK;
	}
	return DEV_FAILURE_CALLER;
}


/**
* \file kmain.c
* Entry point in the kernel.
*/

#include "sys/kernel.h"
#include "sys/vmm.h"

#include "hal/hal.h"
#include "hal/apic.h"

#include "sys/multiboot1.h"
#include "sys/pit.h"
#include "sys/pmm.h"
#include "sys/vmm.h"
#include "sys/process.h"
#include "sys/allocate.h"
#include "sys/dev.h"
#include "sys/syscall.h"
#include "sys/blk_mgr.h"

#include "drv/vga.h"
#include "drv/ps2.h"
#include "drv/uart.h"
#include "drv/kbd.h"
#include "drv/null.h"
#include "drv/devinterface.h"

#include "lib/stdio.h"

#include "sys/exception.h"


cpu_info cpus[MAX_CPUS];
int num_cpus = 0;

void test_main(multiboot_info *mboot_ptr, uint32_t mem_start, uint32_t mem_end, uint32_t stack);


void kmain(
		multiboot_info *mboot_ptr,
		uint32_t mem_start,
		uint32_t mem_end,
		uint32_t stack
		)
{


	if(mem_end > MAX_KERNEL_MEM)	PANIC("Kernel is too large\n");
	if(VM_VALID == false)			PANIC("Virtual memory is NOT valid\n");


	rsdp_descriptor rsdp;
	acpi_info ainfo;
	IO_apic io_apic;
	KHeap kheap;
	int resp;	// Responses from functions
	char cpuid[13];


// If the kernel output port is serial, we must initialize it
// if not, we can wait until we have set up the device manager.
#if OUTPUT==DBGOUT_SERIAL
	if( (resp = uart_init()) != 0)	{
		kprintf(K_HIGH_INFO, "UART NOT initialized: %i\n", resp);
		PANIC("");
	}
	kprintf(K_HIGH_INFO, "MAIN: UART init\n");
#endif


	int esp=0;
	get_esp(esp);
	kprintf(K_HIGH_INFO, "Stack is at 0x%x\n", esp);


	// Check if we multiboot passes us enough information about our kernel
	// This also works as simple sanity check on the structure.
	check_necessary_flags(mboot_ptr->flags);


	// 64-bit is even less supported
	#if defined(x86_64)
		printf("Running 64-bit kernel\n");
		while(1);
	#endif


#ifdef TEST_KERNEL
	if(pmm_run_all_tests_before() == false)
		PANIC("pmm_run_all_tests_before() failed");
#endif

	// Initialize the physical memory manager
	init_pmm((multiboot_mmap*)mboot_ptr->mmap_addr, mboot_ptr->mmap_length);
	kprintf(K_HIGH_INFO, "MAIN: Physical Memory Manager init\n");

#ifdef TEST_KERNEL
	if(pmm_run_all_tests_after() == false)
		PANIC("pmm_run_all_tests_after() failed");
#endif


	// Mark kernel-code as taken
	pmm_mark_mem_taken(mem_start, mem_end);

	if(!move_module(
			mboot_ptr, "sc2.bin", (uint8_t*)KEYBOARD_MAP,
			KEYBOARD_MAP_MAX_SZ
		)
	)	{
		kprintf(K_FATAL, "Unable to move sc2.bin to 0x%x\n", KEYBOARD_MAP);
		PANIC("Unable to move sc2.bin");
	}

	// Move the boot AP code to 0x7000
	if(!move_module(mboot_ptr, "bootap.bin", (uint8_t*)0x7000, 512))	{
		kprintf(K_FATAL, "Unable to move bootap.bin code to 0x7000\n");
		PANIC("Unable to move bootapp.bin");
	}

	// Find the usermode code and mark it as taken
	// We can't move it to the correct location before virtual memory
	addr_t user_start, user_end;
	if(!find_module(mboot_ptr, "usermode.bin", &user_start, &user_end))	{
		PANIC("Unable to find usermode.bin");
	}
	pmm_mark_mem_taken(user_start, user_end);
	kprintf(K_DEBUG, "usermode.bin is from 0x%x -> 0x%x\n",
		user_start, user_end);


	addr_t ext_start, ext_end;
	if(!find_module(mboot_ptr, "ext2.fs", &ext_start, &ext_end))	{
		PANIC("Unable to find ext2.fs");
	}
	pmm_mark_mem_taken(ext_start, ext_end);
	kprintf(K_DEBUG, "ext2.fs is from 0x%x -> 0x%x\n", ext_start, ext_end);

	kprintf(K_HIGH_INFO, "Found and identified all boot modules\n");

	if(cpu_supported() == 0)
		PANIC("CPU not supported");

	get_vendor_id(cpuid);
	kprintf(K_HIGH_INFO, "CPU id: %s\n", cpuid);
#ifdef TEST_KERNEL
	if(strncmp(cpuid, "GenuineIntel", 12) != 0)
		PANIC("CPU ID is not a match");
#endif

	if(apic_init(&rsdp) == false)	{
		kprintf(K_FATAL, "Unable to find APIC\n");
		PANIC("APIC NOT found\n");
	}

	if(apic_find_info(&ainfo, &rsdp) != 0)	{
		kprintf(K_FATAL, "Unable to find info on system using APIC\n");
		PANIC("Unable to store information about system\n");
	}

	resp = apic_find_cpus(&rsdp, &io_apic);
	if(resp == 0)	{
		kprintf(K_FATAL, "Found 0 CPUs\n");
		PANIC("");
	}
	kprintf(K_HIGH_INFO, "APIC found %i CPUs\n", resp);

	if(lapic_install(&io_apic) == false)	{
		PANIC("Unable to install local APIC\n");
	}

	uint32_t ver = lapic_read_version();
	kprintf(K_LOW_INFO, "LAPIC version: %i, max LVT %i\n",
		ver&0xFF, (ver & 0x00FF0000) >> 16);


	// Install the GDT
	gdt_install();
	kprintf(K_HIGH_INFO, "MAIN: GDT initialized\n");

	// Disable the PIC
	pic_init();
	kprintf(K_HIGH_INFO, "MAIN: PIC initialized\n");

	ioapic_install(&io_apic);
	kprintf(K_HIGH_INFO, "MAIN: IOAPIC initialized\n");

	idt_install();
	kprintf(K_HIGH_INFO, "MAIN: IDT initialized\n");

	isr_install();
	kprintf(K_HIGH_INFO, "MAIN: ISR initialized\n");

	uart_install_interrupt();
	kprintf(K_HIGH_INFO, "MAIN: UART interrupts installed\n");


	syscall_init();
	kprintf(K_HIGH_INFO, "MAIN: Syscalls installed\n");

	addr_t pdir = vmm_init();
	kprintf(K_HIGH_INFO, "MAIN: Paging init\n");

	// Set up usermode code at 1 GB
	vmm_get_mapped_page(GB1, X86_PAGE_USER | X86_PAGE_WRITABLE);
	memcpy((void*)GB1, (void*)user_start, (user_end - user_start));

	allocate_init(&kheap, HEAP_START, 32, (HEAP_END - HEAP_START), X86_PAGE_WRITABLE);
	kprintf(K_HIGH_INFO, "MAIN: Kernel Heap init\n");

	cpu_common* data = (cpu_common*)heap_malloc_internal(&kheap, sizeof(cpu_common));
	memcpy(data->cpuid, cpuid, 13);
	memcpy(&(data->rsdp), &rsdp, sizeof(rsdp_descriptor));
	memcpy(&(data->ainfo), &ainfo, sizeof(acpi_info));
	memcpy(&(data->io_apic), &io_apic, sizeof(IO_apic));
	memcpy(&(data->kheap), &kheap, sizeof(KHeap));

	// Will allocate VM from BLKMGR_START to BLKMGR_START + (KB4*KB4)
	data->blkmgr = blk_mgr_init(BLKMGR_START, KB4);
	cpus[lapic_cpuid()].data = data;

	kprintf(K_HIGH_INFO, "MAIN: Defined CPU structure\n");

	if(dev_init(&(data->dev)) != 0)	PANIC("dev\n");
	kprintf(K_HIGH_INFO, "MAIN: Device manager init\n");
	kprintf(K_HIGH_INFO, "MAIN: Device manager init\n");

	DeviceOpened* ramdiskdev = ramdisk_init(ext_start, ext_end);
	if(ramdiskdev == NULL)	PANIC("Unable to create ramdisk");

	if(_dev_append_open(&(data->dev), ramdiskdev) < 0)	PANIC("Unable to add ramdisk");
	kprintf(K_HIGH_INFO, "MAIN: ramdisk init\n");
	kprintf(K_HIGH_INFO, "MAIN: ramdisk init\n");

	kprintf(K_HIGH_INFO, "MAIN: starting ext2\n");
	Device* extdev = ext2_init(ramdiskdev->id, "/");
	if(extdev == NULL)	PANIC("Unable to init ext2 disk");
	if(dev_mount(&(data->dev), extdev) != 0)	PANIC("Unable to mount root FS");
	kprintf(K_HIGH_INFO, "MAIN: ext2 init\n");


	Device* vgadev = vga_init("/dev/stdout", 0, 0, DEFAULT_FG, DEFAULT_BG);
	if(vgadev == NULL)	PANIC("Unable to init VGA");
	if(dev_mount(&(data->dev), vgadev) != 0)	PANIC("Unable to mount VGA device");
	kprintf(K_HIGH_INFO, "MAIN: VGA (/dev/stdout) init\n");

	Device* kbddev = kbd_init("/dev/stdin", KEYBOARD_MAP);
	if(kbddev == NULL)	PANIC("Unable to init keyboard");
	if(dev_mount(&(data->dev), kbddev) != 0)	PANIC("Unable to mount keyboard device");
	kprintf(K_HIGH_INFO, "MAIN: Keyboard (/dev/stdin) init\n");

	Device* devdev = devinterface_init("/dev");
	if(devdev == NULL)	PANIC("Unable to init dev device");
	if(dev_mount(&(data->dev), devdev) != 0)	PANIC("Unable to mount dev device");

	resp = ps2_init();
	if(resp < 0)
		kprintf(K_LOW_INFO, "No PS/2 controller found: %i\n", resp);
	else	{
		if(resp > 0)
			kprintf(K_LOW_INFO, "1 PS/2 device found\n");
		else
			kprintf(K_LOW_INFO, "2 PS/2 devices found\n");
	}
	ps2kbd_init();

	// TODO: The devices created are not mounted
	// Should refactor the UART code
	uart_create_device();

	cpus[lapic_cpuid()].data->procs = process_init(pdir);
	kprintf(K_HIGH_INFO, "MAIN: Configured kernel process: \n");

	kprintf(K_HIGH_INFO, "Starting APs\n");
	cpu_start_aps(data);

	// REMARK: Should never return here
	while(1);
}


#ifdef TEST_KERNEL
void test_main(multiboot_info *mboot_ptr, uint32_t mem_start, uint32_t mem_end,
	uint32_t stack)	{

	int n;
	if( (n = uart_init()) != 0)	{
		kprintf(K_HIGH_INFO, "UART NOT initialized: %i\n", n);
	}

	check_necessary_flags(mboot_ptr->flags);


	if(pmm_run_all_tests_before() == false)
		PANIC("pmm_run_all_tests_before() failed");
	init_pmm((multiboot_mmap*)mboot_ptr->mmap_addr, mboot_ptr->mmap_length);
	if(pmm_run_all_tests_after() == false)
		PANIC("pmm_run_all_tests_after() failed");


	kprintf(K_HIGH_INFO, "PASSED all tests\n");


	while(1);
}
#endif

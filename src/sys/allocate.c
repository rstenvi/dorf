
#include "sys/allocate.h"
#include "sys/vmm.h"
#include "hal/hal.h"

#define BITMAP_BLOCK 4096


extern cpu_info cpus[MAX_CPUS];

void allocate_init(KHeap* alloc, uint32_t start, uint32_t blksz, uint32_t maxsz, uint32_t vmflag)	{
	alloc->vmmstart = start;
	alloc->blksz = blksz;
	alloc->maxsz = maxsz;
	alloc->vmflag = vmflag;

	kprintf(K_DEBUG, "Getting mapped page at 0x%x\n", start);
	void* phys = vmm_get_mapped_page(start, vmflag);
	ASSERT(phys != NULL, "Unable to map page");

	alloc->bitmap = start;
	memset((void*)start, 0x00, BITMAP_BLOCK);

	uint32_t reserved = BITMAP_BLOCK / blksz;
	int i;
	for(i = 0; i < reserved; i++)	{
		alloc->bitmap[i/8] |= (1 << (i%8));
	}

	alloc->next = NULL;

//	init_spinlock(&(kheap->lock), LOCK_HEAP);
}

int _allocate_find_free(uint8_t* bitmap, uint32_t bitmapsz, uint32_t blocks)	{
	int i, count = 0;
	for(i = 0; i < bitmapsz; i++)	{
		if((bitmap[i/8] & (1 << (i%8))) == 0)	{
			count++;
			if(count >= blocks)	{
				return i - count + 1;
			}
		}
		else	{
			count = 0;
		}
	}
	return -1;
}

int allocate_map_pages(uint32_t start, uint32_t end, uint32_t vmflag)	{
	uint32_t rstart = (start / 4096) * 4096, i;
	for(i = rstart; i <= end; i += 4096)	{
		uint32_t phys = vmm_virt2phys(i);
		if(phys == 0)	{
			phys = vmm_get_mapped_page(i, vmflag);
			ASSERT(phys != 0, "Unable to map page");
		}
	}
	return 0;
}

void* heap_malloc_internal(KHeap* alloc, uint32_t size)	{
	size += sizeof(uint16_t);	// Need 2 extra bytes to store blocks reserved

	uint32_t blocks = size / alloc->blksz;
	if(size % alloc->blksz != 0)	{
		blocks++;
	}
	kprintf(K_DEBUG, "Need %i blocks\n", blocks);
	int free = _allocate_find_free(alloc->bitmap, BITMAP_BLOCK, blocks);
	kprintf(K_DEBUG, "free = %i\n", free);
	if(free >= 0)	{
		uint32_t offset = alloc->blksz * free;
		void* addr = (void*)(alloc->vmmstart + offset);

		// Map in pages if they are not already present
		allocate_map_pages((uint32_t)addr, (uint32_t)addr + size * alloc->blksz, alloc->vmflag);

		// Mark size at the beginning
		uint16_t* rstart = (uint32_t*)addr;
		*rstart = blocks;
		addr = addr + sizeof(uint16_t);

		// Mark memory as taken
		int i;
		kprintf(K_DEBUG, "Marking %i (%i) bits as taken\n", blocks, free);
		for(i = free; i < (free + blocks); i++)	{
			kprintf(K_DEBUG, "i = %i\n", i);
			alloc->bitmap[i/8] |= (1 << (i%8));
		}
		return addr;
	}
	PANIC("Run out of contiguous space");
	return NULL;
}

void* heap_malloc(uint32_t size)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	KHeap* alloc = &(c->data->kheap);
	kprintf(K_DEBUG, "Kernel MALLOC %i\n", size);
	//spinlock_acquire(&kheap->lock);
	void* ret = heap_malloc_internal(alloc, size);
	kprintf(K_DEBUG, "Kernel MALLOC %p\n", ret);
	//spinlock_release(&kheap->lock);
	return ret;
}

int heap_free_internal(KHeap* alloc, void* addr)	{
	uint16_t* numblks = (addr - sizeof(uint16_t));
	uint32_t start = (uint32_t)(addr) - sizeof(uint16_t);
	uint32_t offset = (start - alloc->vmmstart);
	uint32_t start_blk = offset / alloc->blksz;
	uint16_t i;
	for(i = start_blk; i < start_blk + *numblks; i++)	{
		alloc->bitmap[i/8] &= ~(1 << (i%8));
	}
	return 0;
}

void heap_free(void* addr)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	KHeap* alloc = &(c->data->kheap);
	kprintf(K_DEBUG, "Kernel free %p\n", addr);

	kprintf(K_DEBUG, "FREE: %p\n", addr);
	//spinlock_acquire(&kheap->lock);
	heap_free_internal(alloc, addr);
	//spinlock_release(&kheap->lock);
}

void* heap_realloc_internal(KHeap* alloc, void* addr, uint32_t newsz)	{
	kprintf(K_DEBUG, "HEAP realloc %p (%i)\n", addr, newsz);
	if(addr == NULL)	{
		return heap_malloc_internal(alloc, newsz);
	}
	uint16_t* numblks = (addr - sizeof(uint16_t));
	uint32_t oldsz = *(numblks) * alloc->blksz;

	// If old size is still large enough
	if(oldsz >= newsz)	{
		return addr;
	}

	// Get new space and copy over data
	void* newaddr = heap_malloc_internal(alloc, newsz);
	if(newaddr == NULL)	return NULL;
	memcpy(newaddr, addr, oldsz);

	// Free old space
	heap_free_internal(alloc, addr);


	return newaddr;
}

void* heap_realloc(void* addr, uint32_t newsz)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	KHeap* alloc = &(c->data->kheap);

	void* ret = heap_realloc_internal(alloc, addr, newsz);

	return ret;
}


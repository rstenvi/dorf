/**
 * \file blk_mgr.c
 * Manage blocks of physical and virtual memory
 */

//#define bitmap_is_set_macro(a,b) (a & (1 << ((b) % 8)))

#include "sys/blk_mgr.h"
#include "sys/vmm.h"
#include "sys/pmm.h"
#include "sys/kernel.h"


/**
 * Maintains a block list of available and taken virtual memory.
 */
typedef struct _Blk_mgr	{
	uint32_t start_blk, end_blk, blk_sz;
	uint8_t bitmap[BLKMGR_BM_SZ];
} __attribute__((packed)) Blk_mgr;



// Allocates 4084 blk_sz blocks of virtual memory
// The blocks can be allocated one block at the time
// One block (the first one) is reserved for the bitmap
Blk_mgr* blk_mgr_init(addr_t start_mem, uint32_t blk_sz)	{
	if(blk_sz > 4096)			PANIC("Size unsupported\n");
	if((4096 % blk_sz) != 0)	PANIC("Size unsupported\n");
	if((start_mem % 4096) != 0)	PANIC("Start memory invalid\n");

	uint32_t start_block = (start_mem / 4096);
	uint32_t blocks = (BLKMGR_BM_BITS * blk_sz) / 4096;

	void* phys = pmm_alloc_first();
	kprintf(K_DEBUG, "Mapping 0x%x with 0x%x\n", phys, start_mem);
	if(vmm_map_page((uint32_t)phys, start_mem, X86_PAGE_WRITABLE) != VMM_SUCCESS) {
		PANIC("Unable to map page");
	}

	// Need to zero out memory
	memset((void*)start_mem, 0x00, 4096);

	// Create block and mark first block as taken
	Blk_mgr* ret = (Blk_mgr*)start_mem;
	ret->start_blk = start_block;
	ret->end_blk = (start_block + blocks);
	ret->blk_sz = blk_sz;

	// Need to mark all taken blocks as taken
	uint32_t blk_per_blk = ret->blk_sz / 4096, i;
	for(i = 0; i < blk_per_blk; i++)	{
		ret->bitmap[i/8] |= (1 << (i % 8));
	}
	return ret;
}

// TODO: Should release all in bitmap
void blk_mgr_destroy(Blk_mgr* mgr)	{
	PANIC("BLK not implemented");
}

void* blk_mgr_alloc(Blk_mgr* mgr, uint32_t block)	{
	void* phys = pmm_alloc_first();
	if(vmm_map_page((uint32_t)phys, block*4096, X86_PAGE_WRITABLE) != VMM_SUCCESS) {
		PANIC("Unable to map page");
	}
	uint32_t ind = block - mgr->start_blk;
	mgr->bitmap[ind/8] |= (1 << (ind % 8));
	return (void*)(block * 4096);
}

// Take over the physical page, virtual memory will be re-used, so physical page
// MUST be moved to a different virtual page by the caller.
void blk_mgr_take(Blk_mgr* mgr, void* addr)	{
	if(mgr->blk_sz != 4096)	PANIC("Operation not supported on size\n");

	uint32_t blk = (uint32_t)addr/mgr->blk_sz;
	blk -= mgr->start_blk;
	uint32_t ind = blk / 8;
	uint32_t off = blk % 8;
	mgr->bitmap[ind] &= ~(1 << off);
	vmm_unmap_page((uint32_t)addr);
}

void blk_mgr_free(Blk_mgr* mgr, addr_t addr)	{
	// TODO: Check if whole page can be removed
	PANIC("BLK not implemented");
	// Will also free in PMM
	vmm_unmap_page(addr);

	// Mark as free in bitmap
	uint32_t blk = (addr/4096) - mgr->start_blk;
	uint32_t ind = blk / 8;
	uint32_t off = blk % 8;
	mgr->bitmap[ind] &= ~(1 << off);
}


void* blk_mgr_alloc_first(Blk_mgr* mgr)	{
	// TODO: Does not check all bits i not divisible by 8
	uint32_t len = (mgr->end_blk - mgr->start_blk) / 8, i, j;

	uint32_t blk_per_blk = (4096 / mgr->blk_sz);

	kprintf(K_DEBUG, "BLKMGR: Len %i, num blocks: %i blk sz: %i\n", len, blk_per_blk, mgr->blk_sz);

	for(i = 0; i < len; i++)	{
		if(mgr->bitmap[i] != 0xFF)	{
			uint8_t val = mgr->bitmap[i];
			for(j = 0; j < 8; j++)	{
				if((val & (1 << j)) == 0)	{
					// We first need to check if we have already allocated that
					// block. Since we are looking from the beginning and taking
					// the first available, we can be sure that if we are in the
					// middle of a 4KB block, we will aready have allocated it.
					uint32_t blk_ind = (i * 8) + j;
					if( (blk_ind % blk_per_blk) != 0)	{
						return (void*)(mgr->start_blk + (blk_ind * mgr->blk_sz));
					}
					uint32_t blk_off = blk_ind / blk_per_blk;

					kprintf(K_DEBUG,
						"Allocating address 0x%x i=%i&j=%i | ind %i | blk %i | per %i\n",
						(mgr->start_blk + blk_off) * 4096, i, j, blk_off,
						blk_per_blk, blk_ind
					);
					return blk_mgr_alloc(mgr, (mgr->start_blk + blk_off));
				}
			}
		}
	}
	PANIC("Unable to allocate bitmap");
	return NULL;
}

// Allocate consecutive
// TODO: Allow more fine-grained control
void* blk_mgr_alloc_first_consec(Blk_mgr* mgr, uint32_t blks)	{
	if(blks != 8)	PANIC("Length not supported\n");
	uint32_t len = (mgr->end_blk - mgr->start_blk) / 8, i, j;
	for(i = 0; i < len; i++)	{
		if(mgr->bitmap[i] == 0x00)	{
			for(j = 0; j < 8; j++)	{
				PANIC("Not implemented yet\n");
			}
		}
	}
	return NULL;
}



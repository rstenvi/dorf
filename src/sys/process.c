/*
* \file process.c
*/

#include "sys/process.h"
#include "sys/allocate.h"
#include "sys/pmm.h"
#include "sys/dllist.h"
#include "sys/dev.h"
#include "sys/elf.h"
#include "sys/fifo.h"

#include "hal/hal.h"

#include "lib/string.h"
#include "lib/stdio.h"
#include "dorf.h"

extern cpu_info cpus[];


typedef struct _SecurityToken 	{
	/** How many references to this token. */
	uint32_t references;

	uint32_t ruid;	/**< real UID */
	uint32_t euid;	/**< effective UID */
	uint32_t rgid;
	uint32_t egid;
} SecurityToken;

struct _pcb	{
	uint16_t pid;
	uint32_t state;
	uint32_t* dirtable;
	tcb* threads;
	uint16_t num_threads;
	procpri_t priority;

	SecurityToken* token;
	DevOpened* devs[PROC_OPEN_MAX];
	KHeap* pheap;
};


typedef struct _tcb	{
	pcb* parent;
	uint32_t* pstack;
	uint32_t stack_limit;
	Registers* regs;
	uint32_t state;

	// If process is blocked
	union	{
		devid_t blocked_devid;
		procid_t blocked_procid;
	};
} tcb;

typedef struct _DevOpened	{
	uint32_t procid, devid, references;
} DevOpened;


typedef struct _Procblocked	{
	tcb* thread;
	Blockedby blockedby;
	union	{
		devid_t devid;
		procid_t procid;
	};
} Procblocked;


/**
* Holds all information about all the processes on the system.
*/
typedef struct _Processes	{

	/**
	* A bitmap of 4096 bytes with available PIDs.
	* Can therefore be 2^12 * 2^3 = 2^15 PIDs
	*/
	uint8_t* free_pids;

	Fifo* list_ready[PROC_PRIORITIES];
	Fifo* list_blocked;

	/**
	* Array of security token pointers. Each pcb will also contain a reference
	* to one of these entries.
	*/
	SecurityToken* tokens[PROC_MAX_TOKENS];
} Processes;



uint32_t schedule(tcb* t, cpu_info* c);
void choose_schedule(cpu_info* c, int lowest);
uint32_t do_schedule(tcb* t, cpu_info* c);

int add_readylist(Processes* p, tcb* t);
tcb* proc_find_blocklist(Processes* p, devid_t devid);
pcb* alloc_empty_proc(Processes* procs);


// ------------- External kernel interface ----------------------- //

Processes* process_init(addr_t pdir)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	Processes* ret = (Processes*)heap_malloc(sizeof(Processes));

	ret->free_pids = (uint8_t*)blk_mgr_alloc_first(c->data->blkmgr);
	memset(ret->free_pids, 0x00, KB4);


	// Initialize security tokens
	// Start with all pointers set to zero, that is how we know the spot is
	// available
	memset(ret->tokens, 0x00, (sizeof(SecurityToken*) * PROC_MAX_TOKENS));
	SecurityToken* tok = (SecurityToken*)heap_malloc(sizeof(SecurityToken));
	tok->references = 1;
	tok->ruid = 0;
	tok->euid = 0;
	tok->rgid = 0;
	tok->egid = 0;
	ret->tokens[0] = tok;	// Copy into structure


	// Allocate empty process with no state information, it can therefore
	// not be switched to. It will be overwritten with the first "exec"
	// when we enter usermode.
	pcb* p = alloc_empty_proc(ret);
	p->state = p->threads->state = PROC_RUNNING;

	p->token = tok;	// The first process gets the token we just defined


	p->dirtable = (uint32_t*)pdir;

	p->threads->pstack = (uint32_t*)0xFFB00F00;

	ret->list_blocked = fifo_create(PROC_BLOCKEDLIST_MAX);
	int i;
	for(i = 0; i < PROC_PRIORITIES; i++)	{
		ret->list_ready[i] = fifo_create(PROC_READYLIST_MAX);
	}


	c->current = p->threads;
	c->data->procs = ret;	// Place in CPU variable
	return ret;
}

int current_pid()	{
	cpu_info* c = &cpus[lapic_cpuid()];
	return c->current->parent->pid;
}

void proc_store_registers(Registers* regs)	{
	// Copy over registers for interrupt
	cpu_info* c = &cpus[lapic_cpuid()];
	tcb* t = c->current;
	memcpy(t->regs, regs, sizeof(Registers));
}


//------------------ Internal functions ------------------------ //

int blockedlist_cmp_proc(void* elem, void* f)	{
	procid_t find = *((uint32_t*)f);
	tcb* t = (tcb*)elem;
	if(t->state == BLOCKED_BY_PROC)	{
		if(t->blocked_procid == find)	return 0;
	}
	return -1;
}

int blockedlist_cmp_device(void* elem, void* f)	{
	devid_t find = *((devid_t*)f);
	tcb* t = (tcb*)elem;
	if(t->state == BLOCKED_BY_DEVICE)	{
		if(t->blocked_devid == find)	return 0;
	}
	return -1;
}

// Unblock processes that is blocked by a certain proc id
int unblock_thread_proc(procid_t pid)	{
	Processes* p = cpus[lapic_cpuid()].data->procs;
	int count = 0;

	void* res = NULL;
	while(1)	{
		res = fifo_find(p->list_blocked, &blockedlist_cmp_proc, (void*)(&pid));
		if(res != NULL)	{
			tcb* t = (tcb*)res;
			kprintf(K_DEBUG, "PROC: Unblocked pid %i\n", t->parent->pid);
			add_readylist(p, t);
			count++;
		}
		else	{
			break;
		}
	}
	return count;
}

tcb* unblock_thread_device(devid_t did)	{
	Processes* p = cpus[lapic_cpuid()].data->procs;

	void* res = fifo_find(p->list_blocked, &blockedlist_cmp_device, (void*)(&did));
	if(res == NULL)	return NULL;

	tcb* t = (tcb*)res;
	return t;
}

int block_thread_proc(tcb* blocked, procid_t pid)	{
	Processes* p = cpus[lapic_cpuid()].data->procs;

	blocked->state = BLOCKED_BY_PROC;
	blocked->blocked_procid = pid;
	fifo_push(p->list_blocked, (void*)blocked);

	return 0;
}

int block_thread_device(tcb* blocked, devid_t devid)	{
	Processes* p = cpus[lapic_cpuid()].data->procs;

	blocked->state = BLOCKED_BY_DEVICE;
	blocked->blocked_devid = devid;

	fifo_push(p->list_blocked, (void*)blocked);
	return 0;
}

int close_fd(pcb* p, uint32_t fd)	{
	int res = 0;
	if(p->devs[fd]->references == 1)	{
		res = dev_close(p->devs[fd]->devid);
		if(res == DEV_OK)	{
			heap_free(p->devs[fd]);
			p->devs[fd] = NULL;
		}
	}
	else if(p->devs[fd]->references > 1)	{
		p->devs[fd]->references -= 1;
		p->devs[fd] = NULL;
		res = 0;
	}
	else	{
		PANIC("Invalid reference count for open file descriptors");
	}
	return res;
}

int reclaim_open_fds(pcb* p)	{
	int i = 0;
	for(i = 0; i < PROC_OPEN_MAX; i++)	{
		if(p->devs[i] != NULL)	close_fd(p, i);
	}
	return 0;
}

int find_open_id(cpu_info* c)	{
	pcb* p = c->current->parent;
	int i;
	for(i = 0; i < PROC_OPEN_MAX; i++)	{
		if(p->devs[i] == NULL)	return i;
	}
	return -1;
}

int clone_open_table(pcb* to, pcb* from)	{
	int i;
	for(i = 0; i < PROC_OPEN_MAX; i++)	{
		if(from->devs[i] != NULL)	{
			from->devs[i]->references += 1;
		}
		to->devs[i] = from->devs[i];
	}
	return 0;
}


int proc_getc_intr(uint32_t devid, uint32_t ch)	{
	tcb* ret = unblock_thread_device(devid);
	if(ret == NULL)	{
		return 0;
	}

	cpu_info* c = &cpus[lapic_cpuid()];
	ret->regs->eax = ch;

	// Add current to readylist and make the switch
	add_readylist(c->data->procs, c->current);
	vmm_switch_pdir(ret->parent->dirtable);
	do_schedule(ret, c);

	return 0;	// Will never be reached
}

pcb* alloc_empty_proc(Processes* procs)	{
	pcb* p = (pcb*)heap_malloc(sizeof(pcb)); memset(p, 0x00, sizeof(pcb));
	tcb* t = (tcb*)heap_malloc(sizeof(tcb)); memset(t, 0x00, sizeof(tcb));

	uint32_t pid = 0;
	if(find_free_bitmap(procs->free_pids, &pid, 2048, true) == false)	{
		PANIC("No available PIDs\n");
	}
	kprintf(K_DEBUG, "PROC: Allocated PID: %i\n", pid);
	p->pid = pid;
	p->state = t->state = PROC_READY;
	p->priority = PROC_PRI_HIGH;

	t->parent = p;

	t->regs = (Registers*)heap_malloc(sizeof(Registers));

	p->threads = t;
	p->num_threads = 1;
	p->pheap = NULL;

	return p;
}

int add_readylist(Processes* p, tcb* t)	{
	t->state = PROC_READY;
	fifo_push(p->list_ready[t->parent->priority], t);
	return 0;
}


// Find a new thread to execute
void choose_schedule(cpu_info* c, int lowest)	{
	Processes* p = c->data->procs;
	tcb* t = c->current;

	int i;
	tcb* exec = NULL;

	// Try and run all high-priority processes first
	// TODO: With current implementation (2 priorities where lowest is reserved
	// for no priority) this is not a problem. If more priorites are to be
	// implemented we risk starving lowest priority threads.
	for(i = PROC_PRIORITIES-1; i >= 0; i--)	{
		exec = fifo_pop(p->list_ready[i]);
		if(exec != NULL)	break;
	}

	// There should always be one process ready to execute
	if(exec == NULL)	{
		PANIC("No process to execute\n");
	}

	do_schedule(exec, c);
}

uint32_t do_schedule(tcb* t, cpu_info* c)	{
	c->current = t;
	t->parent->state = t->state = PROC_RUNNING;
	schedule(t, c);
	return 0;
}


uint32_t schedule(tcb* t, cpu_info* c)	{
	uint32_t stack = (uint32_t)(c->kstack) + 4092 - sizeof(Registers);

	memcpy((uint8_t*)stack, (uint8_t*)t->regs, sizeof(Registers));
	vmm_switch_pdir(t->parent->dirtable);
	asm (
		"mov %0, %%edx\n\t"
		"jmp interrupt_return"
		: 
		: "r" (stack)
		:
	);
	kprintf(K_DEBUG, "NEVER happens\n");
	return 0;
}

tcb* thread_current()	{
	cpu_info* c = &cpus[lapic_cpuid()];
	return c->current;
}



// TODO: Both for BP and AP
pcb* create_kernel_proc(int cpu_id)	{
/*
	cpu_info* c = &cpus[lapic_cpuid()];

	pcb* p = alloc_empty_proc(c->data->procs);
	p->state = p->threads[0]->state = PROC_RUNNING;
//	p->threads[0]->kstack = (uint32_t*)(KERNEL_STACK_TOP - KB4 + 4);

	// Allocate a page directory table for this kernel process
	p->dirtable = vmm_get_mapped_page(
		(VMM_CPUS_START + (cpu_id*KB4)),
		X86_PAGE_WRITABLE
	);

	// Clone current dir
	vmm_clone_current_dir( (VMM_CPUS_START + (cpu_id*KB4)), p->dirtable);

	p->threads[0]->pstack = (uint32_t*)0xFFB00F00;
*/
	return NULL;	// p
}





int _syscall_address_check(void* addr, const char* vname, const char* fname)	{
	if(addr == NULL)	{
		kprintf(K_HIGH_INFO, "Process %i passed NULL-pointer to kernel mode %s(%s)\n", current_pid(), fname, vname);
		return 1;
	}
	if( (uint32_t)addr < USERMODE_START)	{
		kprintf(K_HIGH_INFO, "Process %i passed pointer in unowned address space to kernel mode %s(%s) %p\n", current_pid(), fname, vname);
		return 1;
	}
	return 0;
}

int proc_argv_get_count(char* argv, int arglen)	{
	int count = 0;
	char* s = argv;
	int i;
	for(i = 0; i < arglen; i++)	{
		if(*s == 0x00)	{
			count++;
		}
		s++;
	}
	return count;
}

int proc_argv_get_length(char* argv)	{
	int len = 0;
	char* s = argv;
	while( 1 )	{
		if(*s == 0x00)	{
			if(*(s+1) == 0x00)	break;
		}
		s++;
		len++;
	}
	return len;
}


int dchar_elements(const char** elems)	{
	int i;
	for(i = 0; elems[i] != NULL; i++);
	return i;
}


uint8_t* fill_stack(uint8_t* stack, const char** elems, int* count)	{
	int i;
	*count = dchar_elements(elems);
	Fifo* fifo = fifo_create(*(count)+2);
	for(i = *count - 1; i >= 0; i--)	{
		stack -= strlen(elems[i]) + 1;
		strcpy((char*)stack, elems[i]);
		fifo_push(fifo, (void*)stack);
	}
	stack = (uint8_t*)((uint32_t)stack & 0xFFFFFFFC);	// Align
	stack -= 4;

	uint8_t* addr;
	while( (addr = (uint8_t*)fifo_pop(fifo)) != NULL)	{
		stack -= 4;
		*((uint32_t*)stack) = (uint32_t)(addr);
	}

	return stack; 	// Think this is the location of argv
}



uint8_t* proc_create_stack2(uint8_t* stack, const char** argv, const char** envp)	{
	int argc, envpc;
	stack -= 4;	// Get some space
	stack = (uint8_t*)((uint32_t)stack & 0xFFFFFFFC);	// Align on 4 bytes
	stack = fill_stack(stack, envp, &envpc);
	uint32_t envploc = (uint32_t)stack;

	stack -= 4;
	stack = fill_stack(stack, argv, &argc);
	uint32_t argvloc = (uint32_t)stack;
	stack -= 4; *((uint32_t*)stack) = (uint32_t)(envploc);	// envp
	stack -= 4; *((uint32_t*)stack) = (uint32_t)(argvloc);	// argv
	stack -= 4; *((uint32_t*)stack) = argc;
	stack -= 4;

	return stack;
}


void dump_2dchar(const char** mems)	{
	int i;
	for(i = 0; mems[i] != NULL; i++)	{
		kprintf(K_DEBUG, "arg: %s\n", mems[i]);
	}
}

char** copy_2dchar(const char** mems)	{
	int len = dchar_elements(mems);
	char** ret = (char**)heap_malloc(sizeof(char**) * (len + 1));
	int i;

	for(i = 0; i < len; i++)	{
		ret[i] = (char*)heap_malloc(strlen(mems[i]) + 1);
		strcpy(ret[i], mems[i]);
	}
	ret[len] = NULL;
	return ret;
}

void free_2dchar(char** mems)	{
	int i;
	for(i = 0; mems[i] != NULL; i++)	{
		heap_free(mems[i]);
	}
	heap_free(mems);
}

int _check_perm_fstat(uint32_t mode, uint8_t perm)	{
	if((mode && DORF_OREAD) != 0 && (perm & FSTAT_PERM_READ) == 0)		return 1;
	if((mode && DORF_OWRITE) != 0 && (perm & FSTAT_PERM_WRITE) == 0)	return 1;
	if((mode && DORF_EEXEC) != 0 && (perm & FSTAT_PERM_EXEC) == 0)		return 1;
	return 0;
}

int _check_perm_fstat_all(uint32_t mode, Filestat* fstat, SecurityToken* token)	{
	int permcheck = 1;	// Defaults to failure
	if(fstat->owner == token->euid)	{
		permcheck = _check_perm_fstat(mode, fstat->operm);
	}
	else if(fstat->group == token->egid)	{
		// TODO: Multiple groups are not supported
		permcheck = _check_perm_fstat(mode, fstat->gperm);
	}
	else	{
		permcheck = _check_perm_fstat(mode, fstat->wperm);
	}

	return permcheck;
}



//---------------- Syscal interface -------------------------------- //

int halt_cpu(Registers* regs)	{
	enable_int();
	while(1)	{
		asm("hlt");
	}
}

int nice(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	procpri_t n = regs->ebx;
	procpri_t o = c->current->parent->priority;
	if(n < o)	{
		c->current->parent->priority = n;
		regs->eax = SYSCALL_OK;
	}
	else	{
		kprintf(K_HIGH_INFO, "Process %i attempted to ask for higher priority\n", current_pid());
		regs->eax = SYSCALL_ACCESS_DENIED;
	}
	return regs->eax;
}

int wait(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	procid_t pid = regs->ebx;

	if(pid == c->current->parent->pid)	{
		kprintf(K_WARNING, "PROC: Cannot wait on itself\n");
		regs->eax = SYSCALL_WRONG_PARAMETERS;
	}
	else if(bitmap_is_set((uint32_t*)(c->data->procs->free_pids), pid) == false)	{
		kprintf(K_WARNING, "PROC: Cannot wait on a process that is not in use\n");
		regs->eax = SYSCALL_WRONG_PARAMETERS;
	}
	else	{
		c->current->parent->state = PROC_BLOCKED;

		block_thread_proc(c->current, pid);

		regs->eax = SYSCALL_OK;
		choose_schedule(c, 0);
	}
	return regs->eax;
}


int fstat(Registers* regs)	{
	// Get parameters
	const char* path = (char*)regs->ebx;
	Filestat* fstat = (Filestat*)regs->ecx;

	if(_syscall_address_check((void*)path, "fstat", "path") != 0 || _syscall_address_check((void*)fstat, "fstat", "fstat") != 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}

	DevRet res = dev_fstat(path, fstat);
	if(res != DEV_OK)	{
		regs->eax = SYSCALL_NO_SUCH_FILE;
	}
	else	{
		regs->eax = SYSCALL_OK;
	}
	return regs->eax;
}



int open(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];

	int open;
	if( (open = find_open_id(c)) < 0)	{
		regs->eax = SYSCALL_NO_AVAILABLE_FD;
		return regs->eax;
	}

	char* path = (char*)regs->ebx;
	uint32_t mode = regs->ecx;

	if(_syscall_address_check((void*)path, "path", "open") != 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}

	// Normalize and check mode
	if( ((mode & DORF_OEXCLUSIVE) != 0 && (mode & DORF_OINCLUSIVE) != 0))	{
		regs->eax = SYSCALL_INCOMPAT_PARAMS;
		return regs->eax;
	}

	// Get file meta-data
	Filestat fstat;
	DevRet res = dev_fstat(path, &fstat);
	if(res != DEV_OK)	{
		// TODO: The file may not exist, if so, we must check DORF_OCREATE and
		// create the file if user has access create-access in dir.
		regs->eax = SYSCALL_NO_SUCH_FILE;
		return regs->eax;
	}


	// Check if user has access to open the file in the given mode
	// Checks are in the following priority:
	// 1. Use owner permissions if user is owner
	// 2. Use group permissions if user is in group
	// 3. Use world-permissions if user is not in group or in owner
	// Only first match is checked, it is therefore possible that world has access to
	// the file, but the owner has no access.
	int permcheck = _check_perm_fstat_all(mode, &fstat, c->current->parent->token);

	// If user has permission, we open the file
	if(permcheck == 0)	{
		kprintf(K_DEBUG, "PROC: Open @ 0x%p (%s) len %i\n", path, path, strlen(path));
		int32_t fd = 0;
		res = dev_open(path, mode, &fd);
		kprintf(K_DEBUG, "PROC: Open new devid: %i (0x%x) | res: %i\n", fd, fd, res);

		// Check if we were able to open a file descriptor
		if(res != DEV_OK)	{
			regs->eax = SYSCALL_UNKNOWN_ERROR;
			return regs->eax;
		}
		kprintf(K_DEBUG, "PROC: open path %s at %i\n", path, open);

		pcb* p = c->current->parent;
		p->devs[open] = (DevOpened*)heap_malloc(sizeof(DevOpened));
		p->devs[open]->procid = open;
		p->devs[open]->devid = fd;
		p->devs[open]->references = 1;
		regs->eax = open;	// Return file descriptor
	}
	else	{
		regs->eax = SYSCALL_ACCESS_DENIED;
		return regs->eax;
	}

	return regs->eax;
}


int close(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	kprintf(K_DEBUG, "PROC: Old registers @ 0x%x | new registers @ 0x%x\n", c->current->regs, regs);
	pcb* p = c->current->parent;
	uint32_t procid = regs->ebx;

	// Cannot close standard input or output
	if(procid > 1)	{
		regs->eax = close_fd(p, procid);
	}
	else	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
	}
	return regs->eax;
}

int lseek(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t procid = regs->ebx;
	int32_t off = regs->ecx;
	uint32_t mode = regs->edx;

	DevRet res = dev_lseek(p->devs[procid]->devid, off, mode);
	regs->eax = res;
	return res;
}

int read(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t procid = regs->ebx;
	uint8_t* buf = (uint8_t*)regs->ecx;
	uint32_t len = regs->edx;
	
	if(_syscall_address_check((void*)buf, "buf", "read") != 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}
	if(len <= 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}

	uint32_t read;
	DevRet res = dev_read(p->devs[procid]->devid, buf, len, &read);
	if(res == DEV_OK)	{
		regs->eax = read;
	}
	else if(res == DEV_OK_WAITING)	{
		PANIC("Not implemented yet for read\n");
	}
	return regs->eax;
}

int getc(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t procid = regs->ebx;

	uint32_t read;
	int32_t res = dev_getc(p->devs[procid]->devid, &read);
	if(res == DEV_OK)	{
		regs->eax = read;
	}
	else if(res == DEV_OK_WAITING)	{
		tcb* t = c->current;
		kprintf(K_DEBUG, "PROC: waiting for input, blocking process | ret to @ 0x%x | 0x%x\n",
			regs->eip, t->regs);
		block_thread_device(t, p->devs[procid]->devid);
		choose_schedule(c, 0);
	}
	return regs->eax;
}


int putc(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t procid = regs->ebx;
	uint8_t ch = (uint8_t)regs->ecx;

	DevRet res = dev_putc(p->devs[procid]->devid, ch);
	regs->eax = res;
	return regs->eax;
}


int write(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t procid = regs->ebx;
	uint8_t* buf = (uint8_t*)regs->ecx;
	uint32_t len = regs->edx;
	
	if(_syscall_address_check((void*)buf, "buf", "write") != 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}
	if(len <= 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}

	kprintf(K_DEBUG, "Write fileid: %i, buffer @0x%p, length: %i\n", procid, buf, len);

	uint32_t written;
	DevRet res = dev_write(p->devs[procid]->devid, buf, len, &written);
	regs->eax = res;
	return regs->eax;
}



int exec(Registers* regs)	{
	kprintf(K_DEBUG, "PROC: exec\n");
	cpu_info* c = &cpus[lapic_cpuid()];

	const char* exec_path = (const char*)regs->ebx;
	
	if(_syscall_address_check((void*)exec_path, "exec_path", "exec") != 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}
	
	Filestat fstat;
	DevRet res = dev_fstat(exec_path, &fstat);
	if(res != DEV_OK)	{
		kprintf(K_WARNING, "Tried to execute non-existent program %s\n", exec_path);
		regs->ebx = 0xff; exit(regs);
	}

	int mode = DORF_EEXEC;	// Set permission to execute
	int permcheck = _check_perm_fstat_all(mode, &fstat, c->current->parent->token);

	if(permcheck != 0)	{
		kprintf(K_WARNING, "Attempted to execute program which user did not have access to %s\n", exec_path);
		regs->ebx = 0xff; exit(regs);
	}
		

	int exec_len = strlen(exec_path);
	const char** exec_argv = (const char**)regs->ecx;
	const char** exec_envp = (const char**)regs->edx;

	dump_2dchar(exec_argv);
	dump_2dchar(exec_envp);

	char* path = (char*)heap_malloc(exec_len + 1);
	strcpy(path, exec_path);

	const char** argv = (const char**)copy_2dchar(exec_argv);
	const char** envp = (const char**)copy_2dchar(exec_envp);

	vmm_reclaim_userspace();

	int32_t devid_new = 0;
	// TODO: Set flag for open
	res = dev_open(path, 0, &devid_new);
	kprintf(K_DEBUG, "Opened %x\n", devid_new);
	if(res < 0)	exit(regs);


	int32_t ret = elf_load(devid_new);
	if(ret < 0)	{
		kprintf(K_DEBUG, "Load ELF %i\n", ret);
		PANIC("Unable to load ELF\n");
	}
	kprintf(K_DEBUG, "Loaded ELF, entry 0x%x\n", (uint32_t)ret);

	dev_close(devid_new);
	uint32_t phys = vmm_get_mapped_page(0xFFB00000 , X86_PAGE_USER | X86_PAGE_WRITABLE);
	phys = vmm_get_mapped_page(0xFFB00000 - KB4, X86_PAGE_USER | X86_PAGE_WRITABLE);
	regs->esp = (uint32_t)proc_create_stack2((uint8_t*)(0xFFB00000+KB4-4), argv, envp);
	c->current->pstack = (uint32_t*)(regs->esp);
	kprintf(K_DEBUG, "Stack is @ 0x%x\n", regs->esp);

	regs->eip = (uint32_t)ret+0x00;
	kprintf(K_DEBUG, "EXEC: Returning to 0x%x, esp 0x%x, EBP: 0x%x\n", regs->eip, regs->esp, regs->ebp);
	heap_free(path);
	free_2dchar((char**)argv);
	free_2dchar((char**)envp);
	load_page_dir_addr((uint32_t)(c->current->parent->dirtable));

	return regs->eax;
}


int getpid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	regs->eax = c->current->parent->pid;
	return regs->eax;
}

int getuid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	regs->eax = c->current->parent->token->ruid;
	return regs->eax;
}

int getgid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	regs->eax = c->current->parent->token->rgid;
	return regs->eax;
}

int geteuid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	regs->eax = c->current->parent->token->euid;
	return regs->eax;
}

int getegid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	regs->eax = c->current->parent->token->egid;
	return regs->eax;
}



int setegid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	SecurityToken* tok = c->current->parent->token;

	// Only root can set effective ID, here we only check the real ID
	if(tok->ruid == 0 || tok->rgid == 0)	{
		tok->egid = regs->ebx;
	}
	regs->eax = SYSCALL_OK;
	return regs->eax;
}

int seteuid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	SecurityToken* tok = c->current->parent->token;

	// Only root can set effective ID, here we only check the real ID
	if(tok->ruid == 0)	{
		tok->euid = regs->ebx;
	}
	regs->eax = SYSCALL_OK;
	return regs->eax;
}

int setgid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	SecurityToken* tok = c->current->parent->token;

	// Only root can set effective ID, here we only check the real ID
	if(tok->ruid == 0 || tok->rgid == 0)	{
		tok->rgid = regs->ebx;
	}
	regs->eax = SYSCALL_OK;
	return regs->eax;
}

int setuid(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	SecurityToken* tok = c->current->parent->token;

	// Only root can set effective ID, here we only check the real ID
	if(tok->ruid == 0)	{
		tok->ruid = regs->ebx;
	}
	regs->eax = SYSCALL_OK;
	return regs->eax;
}

int yield(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	regs->eax = 0;

	// Should be added to back of readylist so it's not returned to again
	add_readylist(c->data->procs, c->current);
	choose_schedule(c, 0);
	return regs->eax;
}

int segfault(Registers* regs)	{
	kprintf(K_WARNING, "PROC: Segfaulted process: %i\n", current_pid());
	exit(regs);
}

int exit(Registers* reg)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	Processes* procs = c->data->procs;
	tcb* t = c->current;
	pcb* p = t->parent;

	p->token->references -= 1;	// Decrement reference count
	if(p->token == 0)	{
		kprintf(K_DEBUG, "Reference count for token is zero, removing token from system\n");
		int i;
		// Attempt to find the correct address and free its memory
		for(i = 0; i < PROC_MAX_TOKENS; i++)	{
			if(procs->tokens[i] == p->token)	{
				heap_free(p->token);
				procs->tokens[i] = NULL;
				break;
			}
		}
	}

	kprintf(K_DEBUG, "PROC: exit pid = %i, dirtable @ 0x%x\n", p->pid, p->dirtable);
	// Reclaim open file descriptors
	reclaim_open_fds(p);

	int ret = unblock_thread_proc(p->pid);
	kprintf(K_DEBUG, "PROC: exit unblocked %i procs\n", ret);

	// Free PID
	bitmap_unmark((uint32_t*)(procs->free_pids), p->pid);

	heap_free(p->threads->regs);
	heap_free(p->threads);

	kprintf(K_DEBUG, "reclaim userspace\n");
	vmm_reclaim_userspace();

	// TODO list
	// - Remove all threads from readylist
	// - Free stack for all threads (kernel and process)
	// - Free directory table
	// Questions
	// - What if thread is running on other CPU?
	//   - We can maybe wait to delete info from process until it's reclaimed

	int pid = p->pid;
	heap_free(p);

	// TODO: Somewhere in reclaim userspace I trash the virtual memory pointing to dirtable
	// dirtable is @ 0x879000
	// virtual address points to 0x86b000
	choose_schedule(c, 0);

	return 0;
}

int kill(Registers* regs)	{
	PANIC("Not implemented yet\n");
	return 0;
}

int poweroff(Registers* regs)	{
	for (const char *s = "Shutdown"; *s; ++s) {
		outb(0x8900, *s);
	}

	return 0;	// Will never be reached
}

int malloc(Registers* regs)	{
	kprintf(K_DEBUG, "SYSCALL: malloc\n");
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t sz = regs->ebx;

	// Validate size argument
	if(sz == 0 || sz > 4000)	{
		regs->eax = 0;
		return regs->eax;
	}
	if(p->pheap == NULL)	{
		// TODO: will fail if called on forked process where parent also has a heap
		p->pheap = (KHeap*)heap_malloc(sizeof(KHeap));
		ASSERT(p->pheap != NULL, "Failed to init proc heap");
		allocate_init(p->pheap, USERMODE_HEAP_START, 16, (USERMODE_HEAP_END - USERMODE_HEAP_START), X86_PAGE_WRITABLE | X86_PAGE_USER);
	}

	void* addr = heap_malloc_internal(p->pheap, sz);
	regs->eax = (uint32_t)addr;

	return regs->eax;
}

int realloc(Registers* regs)	{
	kprintf(K_DEBUG, "SYSCALL: realloc\n");
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	void* addr = (void*)regs->ebx;
	uint32_t newsz = regs->ecx;
	
	// NULL-ptr is allowed in realloc
	if(addr != NULL && _syscall_address_check((void*)addr, "addr", "realloc") != 0)	{
		regs->eax = 0;
		return regs->eax;
	}

	// Validate size argument
	if(newsz == 0 || newsz > 4000)	{
		regs->eax = 0;
		return 0;
	}

	if(p->pheap == NULL)	{
		// TODO: will fail if called on forked process where parent also has a heap
		p->pheap = (KHeap*)heap_malloc(sizeof(KHeap));
		ASSERT(p->pheap != NULL, "Failed to init proc heap");
		allocate_init(p->pheap, USERMODE_HEAP_START, 16, (USERMODE_HEAP_END - USERMODE_HEAP_START), X86_PAGE_WRITABLE | X86_PAGE_USER);
	}

	void* newaddr = heap_realloc_internal(p->pheap, addr, newsz);
	regs->eax = (uint32_t)newaddr;

	return regs->eax;
}

int free(Registers* regs)	{
	cpu_info* c = &cpus[lapic_cpuid()];
	pcb* p = c->current->parent;
	uint32_t addr = regs->ebx;
	
	if(_syscall_address_check((void*)addr, "addr", "free") != 0)	{
		regs->eax = SYSCALL_WRONG_PARAMETERS;
		return regs->eax;
	}
	
	kprintf(K_DEBUG, "SYSCALL: free @0x%x\n", addr);

	// TODO: Does not check if user has access to address, so user-mode program
	// can free heap addresses allocated to the kernel
	int res = heap_free_internal(p->pheap, (void*)addr);
	regs->eax = res;
	return regs->eax;
}

int fork(Registers* regs)	{
	kprintf(K_DEBUG, "SYSCALL: FORK\n");
	cpu_info* c = &cpus[lapic_cpuid()];

	pcb* p = alloc_empty_proc(c->data->procs);

	// Copy token and increment reference count
	p->token = c->current->parent->token;
	p->token->references += 1;

	p->priority = c->current->parent->priority;

	p->dirtable = (uint32_t*)vmm_get_mapped_page(
		PROC_VMM_START + (0 * KB4),
		X86_PAGE_WRITABLE
	);
	vmm_clone_current_dir( (uint32_t*)(PROC_VMM_START + (0 * KB4)), (uint32_t)p->dirtable);
	vmm_unmap_page(PROC_VMM_START + (0 * KB4));

	// Clone the table of opened file descriptors
	clone_open_table(p, c->current->parent);

	// Copy over the registers and set return values
	memcpy(p->threads->regs, regs, sizeof(Registers));

	kprintf(K_DEBUG, "PROC: fork registers placed @ 0x%x\n", p->threads->regs);


	regs->eax = p->pid;
	p->threads->regs->eax = 0;
	p->threads->pstack = c->current->pstack;

	kprintf(K_DEBUG, "REGS 0x%x | 0x%x | SS 0x%x | ESP 0x%x\n",
		p->threads->regs, regs, p->threads->regs->ss,
		p->threads->regs->esp
	);
	add_readylist(c->data->procs, p->threads);

	// Reload page directory so changes take effect
	load_page_dir_addr((uint32_t)(c->current->parent->dirtable));
	kprintf(K_DEBUG, "Fork ended\n");
	return 0;
}



#include "apps/dorf_user.h"
#include "dorf.h"
#include "lib/stdio.h"

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

void print_dir(const char* path);

#define MAXLEN 256

int main(int argc, char** argv, char** envp)	{
	set_environment(envp);	// TODO: Workaround
	char* path = NULL;
	Filestat stat;
	int res;

	// If no arguments, we list PWD
	if(argc < 2)	{
		path = getenv("PWD");
	}
	else	{
		path = (const char*)argv[1];
	}

	if(path == NULL)	{
		printf("Unable to find path to list\n");
		exit(1);
	}

	res = fstat(path, &stat);
	if(res != 0)	{
		printf("Unable to find path: '%s'\n", path);
		exit(1);
	}

	switch(stat.filetype)	{
		case FILE_REGULAR:
			printf("%s\n", path);
			break;
		case FILE_DIRECTORY:
			print_dir(path);
			break;
		default:
			printf("'%s' is an unknown filetype: %i\n", path, stat.filetype);
			break;
	}
	exit(0);
}


void print_dir(const char* path)	{
	char buf[MAXLEN];
	int fd = open(path, 0);
	if(fd < 0)	{
		printf("Unable to open path: '%s'\n", path);
		exit(1);
	}

//	printf("ls: %s %s\n", argv[0], argv[1]);
	
	int buflen = read(fd, buf, MAXLEN);
	int len = 0;
	int line_printed = 0, i;
	while(len < buflen)	{
		Direntry* d = (Direntry*)&buf[len];
		char* p = &buf[len+4];
		
		for(i = 0; i < d->length; i++)	{
			putc(DORF_STDOUT, p[i]);
			line_printed++;
		}
	
		if(line_printed > 60)	{
			putc(DORF_STDOUT, '\n');
			line_printed = 0;
		}
		else	{
			putc(DORF_STDOUT, ' ');
		}
		len += d->length + 4;
	}
	printf("\n");

	close(fd);
}

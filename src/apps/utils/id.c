#include "apps/dorf_user.h"
#include "dorf.h"
#include "lib/stdio.h"

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

#define MAXLEN 256




/**MANUAL
# Name
id - Print information about current user / group

# Synopsis

id [OPTIONS]...

# Description

Parsing of arguments has not been implemented yet.

# Arguments

-u Only print information about user.
-g Only print information about group.
-n Print real name instad of user id (only valid if -u or -g is specified)

*/
int main(int argc, char** argv[], char** envp)	{
	set_environment(envp);	// TODO: Workaround

	int uid = getuid();
	printf("%i\n", uid);

	return 0;
}


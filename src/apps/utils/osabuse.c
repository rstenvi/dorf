/**
 * \file osabuse.c
 * A user-mode program which attempts to abuse any functionality in kernel mode.
 *
 * This program is essentially a testing ground for find bugs in the
 * syscall-interfaces. The goal is that this program should never cause the
 * kernel to crash, the kernel should instead kill the process.
 *
 * This program will also look for various other security issues, like reading
 * or changing kernel memory.
 */
#include "apps/dorf_user.h"
#include "dorf.h"
#include "lib/stdio.h"

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

int abuse_open();
int abuse_malloc();
int abuse_memory();
int print_error(int res, const char* msg);
int abuse_putc();
int abuse_write();
int abuse_close();

int main(int argc, char** argv[])	{
	int res;

	printf("+ Testing close()\n");
	res = abuse_close();
	if(res != 0)	return print_error(-1, "Failed on abuse_close()");

	printf("+ Testing write()\n");
	res = abuse_write();
	if(res != 0)	return print_error(-1, "Failed on abuse_write()");

	printf("+ Testing putc()\n");
	res = abuse_putc();
	if(res != 0)	return print_error(-1, "Failed on abuse_putc()");

	printf("+ Testing memory\n");
	res = abuse_memory();
	if(res != 0)	return print_error(-1, "Failed on abuse_memory()");

	printf("+ Testing open()\n");
	res = abuse_open();
	if(res != 0)	return print_error(-1, "Failed on abuse_open()");

	printf("+ Testing malloc()\n");
	res = abuse_malloc();
	if(res != 0)	return print_error(-1, "Failed on abuse_malloc()");


	printf("[SUCCESS]: Reached end of main\n");
	exit(0);
}

int print_error(int res, const char* msg)	{
	printf("[ERROR]: %s\n", msg);

	// TODO: We don't have a wrapper to main yet, so we must explicitly exit
	// when want to end program-execution. Should fix that when we create a gcc
	// target for this OS.
	exit(res);
}

int abuse_open()	{
	int fd;
	fd = open(NULL, 0);
	if(fd >= 0)	return print_error(-1, "Sent NULL to opened and received file descriptor as response");

	fd = open("invalid", 0);
	if(fd >= 0)	return print_error(-1, "Sent invalid file to be opened and received file descriptor as response");

	// Check if we can open a file
	fd = open("/bin/osabuse", 0);
	if(fd < 0)	return print_error(-1, "Sent valid file to be opened and received error");

	return 0;
}

int abuse_malloc()	{
	int ret, ret2;
	ret = malloc(0);
	if(ret != 0)	return print_error(-1, "Requested 0B from malloc and non-NULL value was returned");

	ret = malloc(-2);
	if(ret != 0)	return print_error(-1, "Requested -2B from malloc and non-NULL value was returned");

	ret = malloc(10);
	if(ret == 0)	return print_error(-1, "Requested 10B from malloc and NULL value was returned");

	ret2 = free(ret + 1);
	if(ret2 >= 0)	return print_error(-1, "Tried to free (addr+1) and negative value was NOT returned");

	ret2 = free(ret);
	if(ret2 != 0)	return print_error(-1, "Tried to free (addr) and non-NULL value was returned");

	ret2 = free(-1);
	if(ret2 >= 0)	return print_error(-1, "Tried to free (0xffffffff) and negative value was NOT returned");
	return 0;
}


int access_invalid_mem(char* addr)	{
	int f = fork();
	if(f == 0)	{
		printf("\t+ Attempting to access 0x%p\n", addr);
		char b = addr[0];	// Should seg-fault the program and force exit
		return print_error(-1, "Invalid memory access did not result in page fault");
		exit(0);
	}
	else	{
		int w = wait(f);
		return 0;
	}

}

int abuse_memory()	{
	access_invalid_mem((char*)0x0);			// NULL-ptr
	access_invalid_mem((char*)0xF000);		// Some address that may or may not exist
	access_invalid_mem((char*)0x100000);	// Kernel-code
	access_invalid_mem((char*)0x100010);	// Kernel-code

	// TODO: Should also test write to user-mode code, when that has been set
	// up with correct permissions
	//access_invalid_mem((char*)0x40100002);	// User-mode code
}

int abuse_putc()	{
	int res;
	res = putc(DORF_STDIN, 'A');	// Write to inputa
	if(res >= 0)	return print_error(-1, "putc to DORF_STDIN should cause error");

	return 0;
}


int abuse_write()	{
	int res, fd;
	fd = open("/dev/null", 0);
	if(fd < 0)	return print_error(-1, "Unable to open /dev/null");

	// Test normal write
	res = write(fd, "data", 4);
	if(res != 4)	return print_error(-1, "Expected written bytes to be 4");

	close(fd);
}

int abuse_close()	{
	int res;
	res = close(DORF_STDOUT);	// Not allowed
	return 0;
}


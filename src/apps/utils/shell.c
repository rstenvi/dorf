

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

#if defined(__linux__)
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <ctype.h>
	#include <dirent.h>
	#include <sys/wait.h>
	#include <sys/types.h>
	#include <unistd.h>
#else
	#include "dorf.h"
	#include "apps/dorf_user.h"
	#include "apps/stdin.h"
	#include "lib/string.h"
	#include "lib/ctype.h"
	#include "lib/stdio.h"
	#include "apps/stdlib.h"
	#include "apps/picol.h"
	#include "lib/ctype.h"
#endif

#define CMD_MAXLEN 128
#define READ_MAXLEN 512

int user_shell(char** oargv, char** envp, struct picolInterp* interp);
int count_argv(const char** argv);
char** cmdline2args(const char* cmdline, char** program, int* argc);
int free_cmdargs(char* program, char** argv, int argc);
void dump_args(char* program, char** argv, int argc);
int cmdline_replace_envvars(char* cmdline, char* out, int maxlen);
char* executable_in_path(const char* fname);
int spawn(char* program, char** argv, char** envp);
int check_builtin(const char* program, char** argv, int argc);
void trim_whitespace(char* str);
#if !defined(__linux__)
int cat_internal(const char* fname);
int picol_poweroff(struct picolInterp *i, int argc, char **argv, void *pd);
int picol_pwd(struct picolInterp* i, int argc, char** argv, void* pd);
int picol_cat(struct picolInterp* i, int argc, char** argv, void* pd);
int picol_no_command(struct picolInterp *i, int argc, char **argv, void *pd);
#endif

int main(int argc, char** argv, char** envp)	{
	// TODO: Workaround before we have a C standard library
#if !defined(__linux__)
	set_environment(envp);
#endif

	int i;

#if !defined(__linux__)
	cat_internal("/etc/motd");
	struct picolInterp interp;
	picolInitInterp(&interp, picol_no_command);
	picolRegisterCoreCommands(&interp);
	picolRegisterCommand(&interp, "echo", picolCommandPuts,NULL);
	picolRegisterCommand(&interp, "poweroff", picol_poweroff,NULL);
	picolRegisterCommand(&interp, "cat", picol_cat,NULL);
	picolRegisterCommand(&interp, "pwd", picol_pwd,NULL);
#endif

	user_shell(argv, envp, &interp);

	// TODO: Should free up memory from environment-variables
	return 0;
}

void trim_whitespace(char* str)	{
	int last = strlen(str) - 1;
	while(last >= 0 && str[last] == ' ')	str[last] = 0x00;
}

int spawn(char* program, char** argv, char** envp)	{
	int wstatus;
	int f = fork();
	if(f == 0)	{
		execve(program, argv, envp);
	}
	else	{
#if defined(__linux__)
		int w = wait(&wstatus);
#else
		int w = wait(f);
#endif
	}
	return 0;
}
char special_altgr(char ch)	{
	switch(ch)	{
//		case '1':	return '¡';
		case '2':	return '@';
//		case '3':	return '£';
		case '4':	return '$';
//		case '5':	return '½';
//		case '6':	return '¥';
		case '7':	return '{';
		case '8':	return '[';
		case '9':	return ']';
		case '0':	return '}';
	}
	return ch;
}
char special2upper(char ch)	{
	switch(ch)	{
		case '1':	return '!';
		case '2':	return '"';
		case '3':	return '#';
//		case '4':	return '¤';
		case '5':	return '%';
		case '6':	return '&';
		case '7':	return '/';
		case '8':	return '(';
		case '9':	return ')';
		case '0':	return '=';
		case ',':	return ';';
		case '.':	return ':';
		case '-':	return '_';
		case '<':	return '>';
		case '\'':	return '*';
//		case '|':	return '§';
	}
	// Default is regular value
	return ch;
}

#if !defined(__linux__)
char keyin2letter(uint32_t keyin)	{
	char input = 0x00;
	if( (input = kbd2char( (keyin & 0xFF))) != 0x00)	{
		uint8_t flag = ((keyin & 0xFF00) >> 8);
		if( (flag & KBD_FLAGS_MASK_SHIFT) || (flag & KBD_FLAGS_MASK_CAPSLOCK) &&
			~( (flag & KBD_FLAGS_MASK_SHIFT) && (flag & KBD_FLAGS_MASK_CAPSLOCK) ) )	{
				input = toupper(input);
				if( (flag & KBD_FLAGS_MASK_SHIFT) )	input = special2upper(input);
		}
		else if( (flag & KBD_FLAGS_MASK_ALTGR) )	input = special_altgr(input);
		else	{
			input = tolower(input);
		}


		if( (flag & KBD_FLAGS_MASK_PRESSED) == 0)	{
			if(input == 0x08)	{
				return 0x08;
			}
			else	{
				return input;
			}
		}
		else	{
			input = 0x00;
		}
	}
	return input;

}
#endif

int user_shell(char** oargv, char** envp, struct picolInterp* interp)	{
	uint32_t keyin;
	char ch;
	char cmd[CMD_MAXLEN];
	char cmdo[CMD_MAXLEN];
	int cmdlen, res;

	char* program = NULL;
	char** argv = NULL;
	int argc = 0;
	while(1)	{
		memset(cmd, 0x00, CMD_MAXLEN);
		memset(cmdo, 0x00, CMD_MAXLEN);
		printf("dorf# ");
		ch = 0x00;
		cmdlen = 0;
#if defined(__linux__)
		while((ch = getc(stdin)) != '\n')	{
#else
		while((ch = keyin2letter(getc(DORF_STDIN))) != '\n')	{
#endif

			if(ch == 0x00)	{

			}
			// Backspace
			else if(ch == 0x08)	{
				if(cmdlen > 0)	{
					cmd[--cmdlen] = 0x00;
#if !defined(__linux__)
					putc(DORF_STDOUT, ch);
#endif
				}
			}
			else if(cmdlen < (CMD_MAXLEN - 1))	{
				cmd[cmdlen++] = ch;
#if !defined(__linux__)
				putc(DORF_STDOUT, ch);
#endif
			}
			else	{
				printf("Exceeded maximum allowed input: %i\n", CMD_MAXLEN);
				break;
			}
		}
#if !defined(__linux__)
		putc(DORF_STDOUT, '\n');
#endif
		// Ignore if max length has been exceeded
		if(cmdlen >= CMD_MAXLEN)	continue;

		cmd[cmdlen] = 0x00;

		trim_whitespace(cmd);	// Remove unecessary space

		// No data given as input
		if(strlen(cmd) == 0)	continue;

		int pcode = picolEval(interp, cmd);
		if(pcode != PICOL_OK)	{
			printf("[%i] %s\n", pcode, interp->result);
		}
/*
		cmdline_replace_envvars(cmd, cmdo, CMD_MAXLEN);

		argv = cmdline2args(cmdo, &program, &argc);
		//dump_args(program, argv, argc);

		if(check_builtin(program, argv, argc) != 0)	{
			//printf("Not among builtin\n");
			char* fpath = NULL;
			if((fpath = executable_in_path(program)) != NULL)	{
				//printf("Executable found in PATH: '%s'\n", fpath);

				spawn(fpath, argv, envp);
				free(fpath);	// Memory allocated by called function
			}
			else	{
				printf("Command not found among built-in or in PATH\n");
			}
		}
		free_cmdargs(program, argv, argc);
		*/
	}
}

#if !defined(__linux__)
int picol_poweroff(struct picolInterp *i, int argc, char **argv, void *pd)	{
	poweroff();
}

int picol_cat(struct picolInterp* i, int argc, char** argv, void* pd)	{
	if(argc > 1)	{	return cat_internal(argv[1]);	}
	return -1;
}

int picol_pwd(struct picolInterp* i, int argc, char** argv, void* pd)	{
	const char* pwd = getenv("PWD");
	if(pwd != NULL)	{
		printf("%s\n", pwd);
	}
	else	{
		printf("Unable to find PWD in among environmental variables\n");
	}
	return 0;
}

int picol_no_command(struct picolInterp *i, int argc, char **argv, void *pd)	{
	char* fpath = NULL;
	if((fpath = executable_in_path(argv[0])) != NULL)	{
		argv[argc] = NULL;	// That is how the OS nows iẗ́'s the end
		spawn(fpath, argv, get_environment());
		free(fpath);	// Memory allocated by called function
		return PICOL_OK;
	}
	return PICOL_ERR;
}

int cat_internal(const char* fname)	{
	int fd = open(fname, 0);
	if(fd > 0)	{
		char buf[READ_MAXLEN];
		int nread;
		do	{
			nread = read(fd, buf, READ_MAXLEN);
			if(nread > 0)	{
				write(DORF_STDOUT, buf, nread);
			}
			else	{
				printf("Unable to read from file, returned: %i\n", nread);
			}
		} while(nread == READ_MAXLEN);
		close(fd);
		return 0;
	}
	else	{
		printf("Unable to open '%s' | returned %i\n", fname, fd);
		return -1;
	}
}

int cat(char** argv, int argc)	{
	if(argc > 1)	{
		return cat_internal(argv[1]);
	}
	return -1;
}
#endif

int pwd(char** argv, int argc)	{
	const char* pwd = getenv("PWD");
	if(pwd != NULL)	{
		printf("%s\n", pwd);
	}
	else	{
		printf("Unable to find PWD in among environmental variables\n");
	}
	return 0;
}

int echo(char** argv, int argc)	{
	int i;
	for(i = 1; i < argc; i++)	{
		printf("%s ", argv[i]);
	}
	printf("\n");
	return 0;
}

int check_builtin(const char* program, char** argv, int argc)	{
	char* rest = NULL;
	if(strcmp(program, "echo") == 0)	{
		return echo(argv, argc);
	}
	else if(strcmp(program, "poweroff") == 0)	{
#if defined(__linux__)
		printf("poweroff() does nothing on Linux\n");
#else
		poweroff();		// Defined as a syscall
#endif
		return 0;		// Should never return
	}
#if !defined(__linux__)
	else if(strcmp(program, "cat") == 0)	{
		return cat(argv, argc);
	}
	else if(strcmp(program, "pwd") == 0)	{
		return pwd(argv, argc);
	}
#endif
	return -1;
}

int free_cmdargs(char* program, char** argv, int argc)	{
	if(program != NULL)	{
		free(program);
		program = NULL;
	}
	int i;
	// Start at 1 because program is argv[0]
	for(i = 1; i < argc; i++)	{
		if(argv[i] != NULL)	{
			free(argv[i]);
		}
	}
	if(argv != NULL)	{
		free(argv);
		argv = NULL;
	}
	return 0;
}

void dump_args(char* program, char** argv, int argc)	{
	printf("\nprogram: %s | argc = %i\n", program, argc);
	int i;
	for(i = 0; argv[i] != NULL; i++)	{
		printf("argv[%i]: '%s'\n", i, argv[i]);
	}
}

#if defined(__linux__)
int executable_in_dir(const char* dname, const char* fname)	{
	DIR *d;
	struct dirent *dir;
	d = opendir(dname);
	if (d) {
		while ((dir = readdir(d)) != NULL) {
			if(strcmp(fname, dir->d_name) == 0)	return 0;
		}
		closedir(d);
	}
	return 1;
}

#else
int executable_in_dir(const char* dname, const char* fname)	{
	char buf[READ_MAXLEN];
	int fd = open(dname, 0);
	if(fd < 0)	{
		printf("Unable to open: '%s' - returned %i\n", dname, fd);
		return -1;
	}
	int flen = strlen(fname);
	int len = 0;
	// TODO: Only read READ_MAXLEN bytes so will not work on large dirs
	int buflen = read(fd, buf, READ_MAXLEN);
	while(buflen > 0 && len < buflen)	{
		Direntry* d = (Direntry*)&buf[len];
		char* p = &buf[len+4];
		if(flen == d->length && memcmp(p, fname, d->length) == 0)	{
			close(fd);
			return 0;
		}
		len += d->length + 4;
	}
	close(fd);
	return 1;
}
#endif

char* executable_in_path(const char* fname)	{
	const char* path = getenv("PATH");
	if(path == NULL)	return NULL;

	int res;
	// We need to change string, so we make a full copy
	char* opath = (char*)malloc(strlen(path) + 1);
	strcpy(opath, path);
	char* wpath = opath;
	char* delim;
	bool br = false;

	do	{
		delim = strchr(wpath, ':');
		if(delim == NULL)	{
			br = true;
		}
		else	{
			*delim = 0x00;	// Replace ':' with 0x00
		}
		res = executable_in_dir(wpath, fname);
		if(res == 0)	{
			char* pname = (char*)malloc(strlen(wpath) + 1 + strlen(fname) + 1);
			strcpy(pname, wpath);
			strcat(pname, "/");
			strcat(pname, fname);
			free(opath);
			return pname;
		}
		wpath = delim = delim + 1;	// Get to next entry
	} while(br == false);

	free(opath);
	return NULL;
}

int find_user_variable(char* cmdline, int* start, int* end, char* name, int maxlen)	{
	bool invar = false;		// Currently not storing variable
	bool bracket = false;	// { has not been hit
	int i, len = strlen(cmdline), namelen = 0;
	*end = 0;
	for(i = 0; i < len; i++)	{
		char c = cmdline[i];
		bool copy = false;
		if(invar)	{
			if(bracket)	{
				if(c == '}')	{
					bracket = false;
					*end = i + 1;
				}
				else	{
					copy = true;
				}
			}
			else	{
				if(namelen == 0 && c == '{')	{
					bracket = true;
				}
				else	{
					copy = true;
				}
			}
			if(copy)	{
				if((bracket || isupper(c) || isdigit(c) || c == '_') )	{
					if(i < maxlen)	{
						name[namelen++] = c;
					}
					else	return -1;
				}
				else	{
					*end = i;
					invar = false;
					break;
				}
			}
		}
		else	{
			if(c == '$')	{
				invar = true;
				*start = i;
			}
		}
	}
	// If not found, we return error
	if(namelen == 0)	return -2;

	if(*end == 0)	*end = i;	// If variable goes until end of string
	// Has been found, return 0
	name[namelen] = 0x00;
	return 0;
}

int cmdline_replace_envvars(char* cmdline, char* out, int maxlen)	{
	#define NAME_MAXLEN 64
	char name[64];
	int res, i;
	int start, end;
	char* currcmd = cmdline;
	int currout = 0;
	memset(out, 0x00, maxlen);
	while( (res = find_user_variable(currcmd, &start, &end, name, NAME_MAXLEN)) == 0)	{
		// First copy what is outside variables
		for(i = 0; i < start && currout < maxlen; i++)	{
			out[currout++] = currcmd[i];
		}
		char* var = getenv(name);
		if(var != NULL)	{
			strncat(out, var, (maxlen - strlen(out)));
		}
		currcmd += end;
	}
	strncat(out, currcmd, (maxlen - strlen(out)));
	return 0;
}

// Note: line does not have to be valid if count < 0
char** create_argv(char* line, char* program, int count)	{
	char** ret = NULL;
	if(count != 0)	{
		if(count < 0)	{
			ret = (char**)malloc(sizeof(char*) * (2));
		}
		else	{
			ret = (char**)malloc(sizeof(char*) * (count + 2));	// 2 extra 1 for program and 1 for NULL-ptr
		}
		if(ret == NULL)	return NULL;
		ret[0] = program;

		if(count < 0)	{
			ret[1] = NULL;
			return ret;
		}
	}
	bool dquote = false;

	int i = 1, len = strlen(line);
	char* curr = line;
	char* last = line;
	do	{
		char c = *curr;
		if(dquote == false && c == '"')	{
			dquote = true;
		}
		else if(dquote == true && c != '"')	{

		}
		// BUG: If end-quote is missing, everything after start quote is ignored
		else if(isspace(c) || c == 0x00 || (dquote == true && c == '"'))	{
			if(ret != NULL)	{
				// Ignore quotes if they are there
				if(dquote)	{ last++; }

				int slen = curr - last;
				ret[i] = (char*)malloc(sizeof(char) * (slen + 1));
				memcpy(ret[i], last, slen);
				ret[i][slen] = 0x00;
				i++;

				last = curr + 1;
			}
			else	{
				count++;
			}
			if(dquote)	{
				dquote = false;
				curr++;
				last++;
			}
		}
		curr++;
	} while(*(curr - 1) != 0x00);	// Parse to and including 0x00

	if(ret == NULL)	{
		if(count == 0)	{	// No arguments
			return create_argv(line, program, -1);
		}
		else	{
			return create_argv(line, program, count);
		}
	}
	else	{
		ret[i] = NULL;
	}
	return ret;
}

int count_argv(const char** argv)	{
	int i;
	for(i = 0; argv[i] != NULL; i++)	{ }
	return i;
}

char** cmdline2args(const char* cmdline, char** program, int* argc)	{
	char* argstart = strchr(cmdline, ' ');
	int plen;
	char** argv;
	int count = 0;
	if(argstart == NULL)	{
		plen = strlen(cmdline);
		count = -1;		// Specified to not parse arguments
	}
	else	{
		plen = argstart - cmdline;
	}
	*program = (char*)malloc(sizeof(char) * (plen + 1));
	memcpy(*program, cmdline, plen);
	*(*(program) + plen) = 0x00;
	argv = create_argv(argstart + 1, *program, count);
	*argc = count_argv((const char**)argv);
	return argv;
}



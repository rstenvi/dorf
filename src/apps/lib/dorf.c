
#include <stddef.h>
#include <stdint.h>
#include "apps/dorf_user.h"
#include "dorf.h"
#include "lib/string.h"


char *strdup(const char* src) {
	char *dst = (char*)malloc(strlen (src) + 1);
	if (dst == NULL) return NULL;
	strcpy(dst, src);
	return dst;
}


/**
* Duplicate file descriptor and connect it to newfd (if not -1).
*
* This is meant to be used when piping output from one program to input of
* another, should be used like this:
* 
* int fd = fddup(DORF_STDOUT, -1);
* fork(...); execve(...);	// Program we want to capture the output of
* fddup(fd, DORF_STDIN);
* fork(...); execve(...);	// Program we want to send input to
*
* \params[in] oldfd Old file descriptor which should be copied, MUST be a valid
* opened file descriptor.
* \params[in] newfd New file descriptor number, MUST be a valid opened file
* descriptor OR -1 to open a new file descriptor.
* \returns The new file descriptor number or negative value on failure.
*/
int fddup(int oldfd, int newfd)	{
	return -1;	// Not implemented yet
}

int fstat(const char* path, Filestat* fstat)	{
	int ret = 0;
	__asm__ __volatile__ ("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_FSTAT), "b" (path), "c" (fstat)
	);
	return ret;
}

/**
 * Open the file pointed to by path.
 * \param[in] path The file which should be opened.
 * \param[in] mode Starting with the LSB, each bit
 * is described below.
 * 1. Open for reading
 * 2. Open for writing
 * 3. Create if not exist
 * 4. Exclusive access (normally only exclusive if opened with write access)
 * 5. Non-exclusive (Used to open shared file for writing)
 * 6. Virtual file - used for IPC
 * \returns Returns the file descriptor which can be used to perform other
 * operations on the file. On error, a negative value is returned:
 * * -1 - The maximum amount of open file descriptors have been reached for the
 *  process.
 * * -2 - Device driver reported an error.
 */
int open(const char* path, int mode)	{
	int ret = 0;
	__asm__ __volatile__ ("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_OPEN), "b" (path), "c" (mode)
	);
	return ret;
}

/**
 * Write to the file descriptor given in fd.
 * \param[in] fd The file descriptor which should be written to, must have been previously opened.
 * \param[in] Data to write.
 * \param[in] len Number of bytes to write.
 * \returns \todo Document return values
 */
int write(int fd, char* data, int len)	{
	int ret = 0;
	__asm__ __volatile__ ("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_WRITE), "b" (fd), "c" (data), "d" (len)
	);
	return ret;
}


/**
* Read contents of a file into specified buffer.
* \param[in] fd A file descriptor that has previously been opened.
* \param[out] buf Output buffer.
* \param[in] len Maximum amount of bytes to read.
* \returns Returns the number of bytes read
*/
int read(int fd, char* buf, int len)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_READ), "b" (fd), "c" (buf), "d" (len)
	);
	return ret;
}

/**
 * Close a file descriptor.
 * \param[in] fd File descriptor to close.
 */
int close(int fd)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_CLOSE), "b" (fd)
	);
	return ret;
}

int getc(int fd)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_GETC), "b" (fd)
	);
	return ret;
}

int fork()	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_FORK)
	);
	return ret;
}

int getuid()	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_GETUID)
	);
	return ret;
}

int getgid()	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_GETGID)
	);
	return ret;
}

int setuid(int id)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_SETUID), "b" (id)
	);
	return ret;
}

int setgid(int id)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_SETGID), "b" (id)
	);
	return ret;
}

int seteuid(int id)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_SETEUID), "b" (id)
	);
	return ret;
}

int setegid(int id)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_SETEGID), "b" (id)
	);
	return ret;
}

int poweroff()	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_POWEROFF)
	);
	return ret;
}

int yield()	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_YIELD)
	);
	return ret;
}

int exit(int status)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_EXIT), "b" (status)
	);
	return ret;
}

int wait(int pid)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_WAIT), "b" (pid)
	);
	return ret;
}

int putc(int fd, unsigned char c)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_PUTC), "b" (fd), "c" (c)
	);
	return ret;
}

int execve(char* path, char** argv, char** envp)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_EXECVE), "b" (path), "c" (argv), "d" (envp)
	);
	return ret;
}

/**
 * Allocate sz bytes of memory.
 * \param[in] sz Number of bytes to allocate.
 * \returns An address pointing to sz bytes of memory than can be used. On
 * error, the value returned is NULL.
 */
int malloc(int sz)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_MALLOC), "b" (sz)
	);
	return ret;
}

int realloc(void* addr, int newsz)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_REALLOC), "b" ((unsigned int)addr), "c" (newsz)
	);
	return ret;
}

int free(void* addr)	{
	int ret = 0;
	__asm__ __volatile__("int $0x80"
		: "=a" (ret)
		: "0" (SYSCALL_FREE), "b" ((unsigned int)addr)
	);
	return ret;
}


#include "apps/stdin.h"
#include "apps/dorf_user.h"
#include "lib/stdio.h"

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

int vprintf_putc(unsigned c, void** ptr);
int printf(const char* fmt, ...);


int printf(const char* fmt, ...)	{
	va_list args;
	int rv;
	va_start(args, fmt);
	rv = do_printf(fmt, args, vprintf_putc, NULL);
	va_end(args);
	return rv;
}


int vprintf_putc(unsigned c, void** ptr)	{
	(void)ptr;
	putc(DORF_STDOUT, c);
}


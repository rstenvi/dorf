
#include "kbd_constants.h"

char kbd2char(unsigned char c)	{
	char r = 0x00;
	switch(c)	{
		case KBD_EVENT_1:	r = '1'; break;
		case KBD_EVENT_2:	r = '2'; break;
		case KBD_EVENT_3:	r = '3'; break;
		case KBD_EVENT_4:	r = '4'; break;
		case KBD_EVENT_5:	r = '5'; break;
		case KBD_EVENT_6:	r = '6'; break;
		case KBD_EVENT_7:	r = '7'; break;
		case KBD_EVENT_8:	r = '8'; break;
		case KBD_EVENT_9:	r = '9'; break;
		case KBD_EVENT_0:	r = '0'; break;
		case KBD_EVENT_A:	r = 'A'; break;
		case KBD_EVENT_B:	r = 'B'; break;
		case KBD_EVENT_C:	r = 'C'; break;
		case KBD_EVENT_D:	r = 'D'; break;
		case KBD_EVENT_E:	r = 'E'; break;
		case KBD_EVENT_F:	r = 'F'; break;
		case KBD_EVENT_G:	r = 'G'; break;
		case KBD_EVENT_H:	r = 'H'; break;
		case KBD_EVENT_I:	r = 'I'; break;
		case KBD_EVENT_J:	r = 'J'; break;
		case KBD_EVENT_K:	r = 'K'; break;
		case KBD_EVENT_L:	r = 'L'; break;
		case KBD_EVENT_M:	r = 'M'; break;
		case KBD_EVENT_N:	r = 'N'; break;
		case KBD_EVENT_O:	r = 'O'; break;
		case KBD_EVENT_P:	r = 'P'; break;
		case KBD_EVENT_Q:	r = 'Q'; break;
		case KBD_EVENT_R:	r = 'R'; break;
		case KBD_EVENT_S:	r = 'S'; break;
		case KBD_EVENT_T:	r = 'T'; break;
		case KBD_EVENT_U:	r = 'U'; break;
		case KBD_EVENT_V:	r = 'V'; break;
		case KBD_EVENT_W:	r = 'W'; break;
		case KBD_EVENT_X:	r = 'X'; break;
		case KBD_EVENT_Y:	r = 'Y'; break;
		case KBD_EVENT_Z:	r = 'Z'; break;
		case KBD_EVENT_SPACE:	r = ' '; break;
		case KBD_EVENT_COMMA:	r = ','; break;
		case KBD_EVENT_DOT:		r = '.'; break;
		case KBD_EVENT_SLASH:	r = '/'; break;
		case KBD_EVENT_SCOLON:	r = ';'; break;
		case KBD_EVENT_SQUOTE:	r = '<'; break;	// TODO: Wrong key
		case KBD_EVENT_ENTER:	r = '\n'; break;
		case KBD_EVENT_BACKSPACE:	r = 0x08; break;
		case KBD_EVENT_EQUAL:		r = '+';	break;
		case KBD_EVENT_DASH:		r = '-';	break;
		default:			/*printf("%x\n", c);*/r = 0x00; break;
	}
	return r;
}


#include <stddef.h>
#include <stdint.h>

#include "apps/dorf_user.h"
#include "apps/stdlib.h"
#include "lib/string.h"

char** environment = (char**)NULL;

// TODO: Remove when we got a stdlib
void set_environment(char** envp)	{
	environment = envp;
}

char* getenv(const char* name)	{
	int i = 0;
	int len = strlen(name);
	for(i = 0; environment[i] != NULL; i++)	{
		if(strncmp(name, environment[i], len) == 0)	{
			return &(environment[i][len+1]);
		}
	}
	return NULL;
}

char** get_environment()	{
	return environment;
}


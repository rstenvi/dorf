
#include "apps/dorf_user.h"
#include "apps/stdin.h"
#include "lib/ctype.h"

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

char* msg = "This is a test msg\n";

#define CMDLINE_MAX_EXEC 32
#define CMDLINE_MAX_ARGV 32


typedef struct _Cmdline	{
	char execp[32];
	char argv[32];
	char envp[32];
	int argc;
} Cmdline;

typedef int (*run_builtin)(char*);
typedef int (*shortcut_builtin)();

int num_builtins = 0;

#define BUILTIN_CMD_MAXLEN 32
#define BUILTIN_CMD_MAXNUM 2


typedef struct _Builtins	{
	char cname[BUILTIN_CMD_MAXLEN];
	run_builtin exec;
} Builtins;

Builtins builtins[BUILTIN_CMD_MAXNUM];

#define NUM_SHORTCUTS 26

//shortcut_builtin shortcuts[NUM_SHORTCUTS];	// A-Z (when ctrl is pressed)



int user_shell();

int builtin_poweroff(char* s);
int builtin_push(char* cmd, run_builtin f);



// TODO: Should be a OS-specified structure (replace the one in ext2 also)
/*
typedef struct _Direntry	{
	uint32_t length;
	char* name;
} __attribute__((packed)) Direntry;
*/

int main(int argc, char** argv, char* envp)	{
	int i;
	char* s = (char*)malloc(19);
	strcpy(s, "da1123a");
	printf("malloc: %s\n", s);
	printf("argv = %p\n", argv);
	printf("argc = %i ", argc);
	for(i = 0; i < argc; i++)	{
		printf(" %s ", argv[i]);
	}
	printf("\n");
	printf("envp = %p | %s\n", envp, envp);

	builtin_push("poweroff", builtin_poweroff);

	user_shell();

	while(1);
	return 0;
}

void execls()	{
	printf("execls\n");
	char* path = "/ls";
	char* a = "/test\x00\x00";
	char* b = "a=b\x00";
	int ff = fork();
	if(ff == 0)	{
		//asm("xchg %bx, %bx");
		execve(path, a, b);
	}
	else	{
		yield();
	}

}

char* normalize_cmdline(char* buf)	{
	// Trim whitespace: https://stackoverflow.com/questions/122616/how-do-i-trim-leading-trailing-whitespace-in-a-standard-way

	return buf;
}

int string_find(const char* buf, char find)	{
	int i = 0;
	while(buf[i] != 0x00 && buf[i] != find)	i++;
	if(buf[i] == 0x00)	return -1;
	else				return i;
}


int parse_cmdline(char* buf, Cmdline* cmdline)	{
	int res = 0;
	res = strncmp("poweroff", buf, strlen("poweroff"));
	if(res == 0)	poweroff();

	strcpy(cmdline->execp, buf);
	cmdline->argc = strlen(cmdline->execp);

	strcpy(cmdline->execp, buf);

	int find = string_find(cmdline->execp, ' ');
	if(find > 0)	{
		cmdline->execp[find] = cmdline->argv[find] = 0x00;
		int i = find+1;
		int j = i;
		while(buf[i] != 0x00)	{
			cmdline->argv[i-j] = buf[i];
			if(buf[i] == ' ')	cmdline->argv[i-j] = 0x00;
			i++;
		}
		// Add two zero bytes at the end
		cmdline->argv[i-j] = 0x00;
		cmdline->argv[i-j+1] = 0x00;
	}
	else	{
		return -1;
	}

	cmdline->envp[0] = 'a';
	cmdline->envp[1] = 0x00;
	cmdline->envp[2] = 0x00;
	cmdline->envp[3] = 0x00;
	cmdline->envp[4] = 0x00;
	cmdline->envp[5] = 0x00;

//	cmdline->argv = argv;
	return 0;
}

char special2upper(char ch)	{
	switch(ch)	{
		case '0':	return '=';
		case '1':	return '!';
		case '2':	return '"';
		case '3':	return '#';
//		case '4':	return '¤';
		case '5':	return '%';
		case '6':	return '&';
		case '7':	return '/';
		case '8':	return '(';
		case '9':	return ')';
		case ',':	return ';';
		case '.':	return ':';
		case '-':	return '_';
		case '\'':	return '*';
//		case '|':	return '§';
	}
	// Default is regular value
	return ch;
}


int user_shell()	{
	Cmdline cmdline;
	char input = 0x00;
	const char* shell = "dorf# ";
	uint32_t keyin = 0x00, shlen = strlen(shell);
	char inbuffer[64];
	int inlen=0;

	while(1)	{
		memset(inbuffer, 0x00, 64);
		memset(cmdline.execp, 0x00, CMDLINE_MAX_EXEC);
		memset(cmdline.argv, 0x00, CMDLINE_MAX_ARGV);
		input = 0x00;
		inlen = 0;
		write(DORF_STDOUT, shell, shlen);
		while(input != '\n')	{
			keyin = getc(DORF_STDIN);
			if( (input = kbd2char( (keyin & 0xFF))) != 0x00)	{
				uint8_t flag = ((keyin & 0xFF00) >> 8);
				if( (flag & KBD_FLAGS_MASK_SHIFT) || (flag & KBD_FLAGS_MASK_CAPSLOCK) &&
					~( (flag & KBD_FLAGS_MASK_SHIFT) && (flag & KBD_FLAGS_MASK_CAPSLOCK) ) )	{
						input = toupper(input);
						if( (flag & KBD_FLAGS_MASK_SHIFT) )	input = special2upper(input);
				}
				else	{
					input = tolower(input);
				}


				if( (flag & KBD_FLAGS_MASK_PRESSED) == 0)	{
					if(input == 0x08 && inlen > 0)	{
						inbuffer[inlen--] = 0x00;
					}
					else	{
						inbuffer[inlen++] = input;
					}
					putc(DORF_STDOUT, input);
				}
				else	{
					input = 0x00;
				}
			}
		}
		inbuffer[inlen-1] = 0x00;	// Ignore newline
		if(parse_cmdline(inbuffer, &cmdline) == 0)	{
			int f = fork();
			if(f == 0)	{
				printf("Started child argv: %s | argc: %i\n", cmdline.argv, cmdline.argc);
				execve(cmdline.execp, cmdline.argv, cmdline.envp);
			}
			else	{
				int w = wait(f);
			}
		}
		// TODO: Execute command
	}
}


int builtin_poweroff(char* s)	{
	poweroff();
}


int builtin_push(char* cmd, run_builtin f)	{
	if(num_builtins < BUILTIN_CMD_MAXNUM)	{
		builtins[num_builtins].exec = f;
		if(strlen(cmd) < BUILTIN_CMD_MAXLEN)	{
			strcpy(builtins[num_builtins].cname, cmd);
			num_builtins++;
			return 0;
		}
		else	{
			return -1;
		}
	}
	else	{
		printf("Reached max number of builtins\n");
		return -2;
	}
}

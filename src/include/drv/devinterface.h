
#ifndef __DEVINTERFACE_H
#define __DEVINTERFACE_H

#include "../sys/kernel.h"
#include "../sys/dev.h"

Device* devinterface_init(const char* path);

#endif

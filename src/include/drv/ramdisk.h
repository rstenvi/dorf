#ifndef __RAMDISK_H
#define __RAMDISK_H


DeviceOpened* ramdisk_init(addr_t start_addr, addr_t end_addr);
DevRet ramdisk_write(void* devcsr, uint8_t* from, uint32_t len, uint32_t offset, uint32_t* wrote);
DevRet ramdisk_read(void* devcsr, uint8_t* to, uint32_t len, uint32_t offset, uint32_t* read);

#endif


#ifndef __KBD_H
#define __KBD_H

#include "../sys/dev.h"

#define KBD_FLAGS_MASK_PRESSED 	 (1 << 0)
#define KBD_FLAGS_MASK_SHIFT     (1 << 1)
#define KBD_FLAGS_MASK_CTRL      (1 << 2)
#define KBD_FLAGS_MASK_ALT       (1 << 3)
#define KBD_FLAGS_MASK_ALTGR       (1 << 3)
#define KBD_FLAGS_MASK_CAPSLOCK    (1 << 4)
#define KBD_FLAGS_MASK_SCROLLOCK   (1 << 5)
#define KBD_FLAGS_MASK_NUMLOCK     (1 << 6)

// 2 bits for keytype
#define KBD_FLAGS_MASK_KEYTYPE  (3 << 1)
	#define KBD_FLAGS_MASK_KEYPAD (0 << 1)
	#define KBD_FLAGS_MASK_APIC   (1 << 1)
	#define KBD_FLAGS_MASK_MM     (2 << 1)
	#define KBD_FLAGS_MASK_PRINT  (3 << 1)

#define KBD_FLAG_MASK_PEND_MORE (1 << 7)

#define KBD_MAX_PEND_SCANCODES 8

void kbd_add_scancode(uint8_t sc);
Device* kbd_init(const char* path, uint32_t keymap);


#endif

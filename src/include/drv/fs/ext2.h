

#ifndef __EXT2_H
#define __EXT2_H


#define EXT2_SIG 0xEF53

struct _Ext;
struct _Ext_inode;

/**
 * Find the inode from a path, the path, note the specific path format.
 * \param[in] ext 
 * \param[in,out] path A path where the separator is 0x00 and ends with 0x0000
 * \param[in] inum Inode to start at, if start at root, inum should be 2
 * \param[in,out] inode A buffer to read inode info into, will also hold last
 * inode read
 * \param[in,out] buf Temporary buffer to read into, should be large enough to
 * hold directories that are read in.
 * \param[in] maxbuf The maximum number of characters that can be read into buf
 */
uint32_t path2inode(struct _Ext* ext, const char* path, uint32_t inum, struct _Ext_inode* inode, char* buf, uint32_t maxbuf);


DevRet ext2_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* opened);
DevRet ext2_close(void* devcsr);
DevRet ext2_read(void* devcsr, uint8_t* buf, uint32_t len, uint32_t offset, uint32_t* read_ret);
DevRet ext2_write(void* devcsr, uint8_t* buf, uint32_t len, uint32_t offset, uint32_t* wrote);

typedef enum _ExtFiletype	{
	EXT2_FIFO,
	EXT2_CHARACTER,
	EXT2_DIRECTORY,
	EXT2_BLOCK,
	EXT2_REGULAR,
	EXT2_SYMBOLIC,
	EXT2_SOCKET,
	EXT2_UNKNOWN
} ExtFiletype;




Device* ext2_init(uint32_t devid, const char* rpath);


#endif



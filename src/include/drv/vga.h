/**
* \addtogroup VGA_drv
* @{
* \file vga.h
* Header file and public API for the VGA driver.
*/

#ifndef __VGA_H
#define __VGA_H

#include "../sys/kernel.h"
#include "../sys/lock.h"

/**
* Different colors for foreground and background color.
*/
typedef enum	{
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Pink = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10, 
	LightCyan = 11, 
	LightRed = 12, 
	LightPink = 13, 
	Yellow = 14, 
	White = 15
} vga_color;

typedef struct _VgaScreen VgaScreen;


/**
* Initializes the screen. This includes setting all the variables in the structure
* VgaScreen and painting the entire screen with the color of bg.
* \param[in] fg Foreground color for future text.
* \param[in] bg Color of screen after this and background color with future
* writes.
*/
Device* vga_init(const char* path, uint32_t startx, uint32_t starty, vga_color fg, vga_color bg);

/** @} */	// VGA_drv
#endif

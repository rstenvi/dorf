
#ifndef __STDIN_H
#define __STDIN_H

char kbd2char(unsigned char);


#define KBD_FLAGS_MASK_PRESSED 	 (1 << 0)
#define KBD_FLAGS_MASK_SHIFT     (1 << 1)
#define KBD_FLAGS_MASK_CTRL      (1 << 2)
#define KBD_FLAGS_MASK_ALT       (1 << 3)
#define KBD_FLAGS_MASK_ALTGR       (1 << 3)
#define KBD_FLAGS_MASK_CAPSLOCK    (1 << 4)
#define KBD_FLAGS_MASK_SCROLLOCK   (1 << 5)
#define KBD_FLAGS_MASK_NUMLOCK     (1 << 6)


#endif

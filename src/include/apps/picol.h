
#ifndef __PICOL_H
#define __PICOL_H


enum {PICOL_OK, PICOL_UNKNOWN_CMD, PICOL_CMD_REGISTERED, PICOL_NO_SUCH_VAR, PICOL_WRONG_NUM_ARGS, PICOL_ERR, PICOL_RETURN, PICOL_BREAK, PICOL_CONTINUE};

// Callback function when registering a new command
typedef int (*picolCmdFunc)(struct picolInterp *i, int argc, char **argv, void *privdata);


struct picolVar {
    char *name, *val;
    struct picolVar *next;
};

struct picolCmd {
    char *name;
    picolCmdFunc func;
    void *privdata;
    struct picolCmd *next;
};

struct picolCallFrame {
    struct picolVar *vars;
    struct picolCallFrame *parent; /* parent is NULL at top level */
};

struct picolInterp {
    int level; /* Level of nesting */
    struct picolCallFrame *callframe;
    struct picolCmd *commands;
    picolCmdFunc noCommand;
    char *result;
};


void picolInitInterp(struct picolInterp*, picolCmdFunc);
void picolRegisterCoreCommands(struct picolInterp*);

int picolRegisterCommand(struct picolInterp*, char*, picolCmdFunc, void*);

int picolEval(struct picolInterp*, char*);



// Functions which can be registered at aliases
int picolCommandPuts(struct picolInterp *i, int argc, char **argv, void *pd);

#endif


#ifndef __DORF_USER_H
#define __DORF_USER_H

#include "sys/syscall.h"
#include "../dorf.h"


#define DORF_STDIN  0x00
#define DORF_STDOUT 0x01




// TODO: Because of the way malloc is set up, this only works properly in user-mode
// Should be in string.h
char *strdup(const char*);



//-------------- Files --------------------//


int open(const char*, int);
int write(int, char*, int);
int read(int,char*,int);
int putc(int, unsigned char);
int getc(int fd);
int fstat(const char*, Filestat*);
int lseek(int,int,int);
int close(int);
int fddup(int, int);


//-------------- Memory --------------------//

int malloc(int);
int free(void*);
int realloc(void*, int);


//-------------- Process --------------------//

int wait(int pid);
int nice();
int exec(char*);
int getpid();
int yield();
int exit(int);
int kill(int);
int fork();
int execve(char*, char**, char**);

//-------------- User ---------------------//

int getuid();
int getgid();

int setuid(int);
int setgid(int);
int seteuid(int);
int setegid(int);


//-------------- Misc --------------------//

int poweroff();
int sys_environment();


#endif

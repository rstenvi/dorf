

#ifndef __BLK_MGR_H
#define __BLK_MGR_H

#include "kernel.h"


#define BLKMGR_BM_SZ 4084
#define BLKMGR_BM_BITS (BLKMGR_BM_SZ * 8)


typedef struct _Blk_mgr Blk_mgr;

Blk_mgr* blk_mgr_init(addr_t start_mem, uint32_t blk_sz);
void blk_mgr_free(Blk_mgr* mgr, addr_t addr);
void* blk_mgr_alloc_first(Blk_mgr* mgr);
void blk_mgr_destroy(Blk_mgr* mgr);

#endif


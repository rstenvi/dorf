/**
* \file dev.h
*/

#ifndef __DEV_H
#define __DEV_H

#include "kernel.h"
#include "../dorf.h"


#define DEVMGR_OPEN_SEEK_ABS 0
#define DEVMGR_OPEN_SEEK_CUR 1
#define DEVMGR_OPEN_SEEK_END 2

typedef uint32_t devid_t;


typedef enum _DevRet	{
	DEV_OK = 0,
	DEV_OK_WAITING,
	DEV_NO_MORE_SPACE,
	DEV_FAILURE_DOWNSTREAM,
	DEV_FAILURE_UNSUPPORTED,
	DEV_NO_SUCH_FILE,
	DEV_FAILURE_CALLER
} DevRet;

struct _DeviceOpened;

typedef DevRet (*tdev_fstat)(void*,const char*, Filestat*);
typedef DevRet (*tdev_open)(void*,const char*, uint32_t, uint32_t, struct _DeviceOpened*);
typedef DevRet (*tdev_read)(void*, uint8_t*, uint32_t, uint32_t, uint32_t*);
typedef DevRet (*tdev_write)(void*, uint8_t*, uint32_t, uint32_t, uint32_t*);
typedef DevRet (*tdev_putc)(void*, uint8_t);
typedef DevRet (*tdev_getc)(void*,uint32_t*);
//typedef int32_t (*dev_seek)(void*, int32_t, uint32_t);
typedef DevRet (*tdev_close)(void*);

typedef struct _Device	{
	char* path;	/**< Full path to mount point. */
	tdev_open open;
	tdev_fstat fstat;
	void* devcsr;	/**< Device can store a pointer to some arbitrary data. */
} Device;

typedef struct _DeviceOpened	{
	uint32_t id;
	tdev_read read;
	tdev_write write;
	tdev_putc putc;
	tdev_getc getc;
	tdev_close close;
	uint32_t offset;
	void* devcsr;
} DeviceOpened;


typedef struct _DevMgr	{
	Device** devices;
	uint16_t device_entries;	/**< Current size of the array. */
	DeviceOpened** opened;
	uint16_t opened_entries;	/**< Current size of open array. */
} DevMgr;

int32_t dev_error(void);

int dev_fill_device_opened(
	DeviceOpened* dev,
	tdev_read read,
	tdev_write write,
	tdev_putc putc,
	tdev_getc getc,
	tdev_close close,
	void* devcsr);



Device* dev_alloc_device(const char* path, tdev_open open, tdev_fstat fstat, void* devcsr);
int dev_init(DevMgr* dev);

int dev_mount(DevMgr* dev, Device* device);
Device* dev_umount(DevMgr* dev, const char* path);



DevRet dev_open(const char* path, uint32_t mode, int32_t* fd);
DevRet dev_fstat(const char* path, Filestat* fstat);
DevRet dev_read(uint32_t fd, uint8_t* buf, uint32_t maxsize, uint32_t* read);
DevRet dev_getc(uint32_t fd, uint32_t* ch);
DevRet dev_putc(uint32_t fd, uint8_t ch);
DevRet dev_write(uint32_t fd, uint8_t* buf, uint32_t len, uint32_t* written);
DevRet dev_close(uint32_t fd);
DevRet dev_lseek(uint32_t fd, int32_t off, uint32_t mode);


#endif

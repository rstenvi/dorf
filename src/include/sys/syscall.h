
#ifndef __SYSCALL_H
#define __SYSCALL_H


// System calls required by C-library (Newlib)
#define SYSCALL_FORK    0x06
#define SYSCALL_EXECVE  0x01
#define SYSCALL_GETPID  0x08
#define SYSCALL_ENVIRON 0x05
#define SYSCALL_WAIT    0x13
#define SYSCALL_KILL    0x0a
#define SYSCALL_TIMES   0x11
#define SYSCALL_EXIT    0x02

#define SYSCALL_STAT    0x10
#define SYSCALL_OPEN    0x03
#define SYSCALL_FSTAT   0x07
#define SYSCALL_LSEEK   0x0c
#define SYSCALL_READ    0x0d
#define SYSCALL_WRITE   0x0e
#define SYSCALL_CLOSE   0x04

#define SYSCALL_ISATTY  0x09
#define SYSCALL_LINK    0x0b
#define SYSCALL_SBRK    0x0f
#define SYSCALL_UNLINK  0x12


#define SYSCALL_YIELD 0x20
#define SYSCALL_NICE  0x21

#define SYSCALL_GETUID 0x30
#define SYSCALL_GETGID 0x31
#define SYSCALL_SETUID 0x32
#define SYSCALL_SETGID 0x33
#define SYSCALL_SETEUID 0x34
#define SYSCALL_SETEGID 0x35

#define SYSCALL_GETC     0xe0
#define SYSCALL_PUTC     0xe1
#define SYSCALL_POWEROFF 0xe2
#define SYSCALL_MALLOC   0xe3
#define SYSCALL_FREE     0xe4
#define SYSCALL_REALLOC  0xe5


#define SYSCALL_HALT_CPU	0xfe

void syscall_init();


#endif

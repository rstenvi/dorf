/**
* \file stdio.h
*/

#ifndef __KLOG_H
#define __KLOG_H

#include <stdarg.h>
#include "../lib/stdio.h"


/**
* Printf message levels. Which ones are printed is controlled by the compiler, a
* debug level of 0 will print everything, a level of 2 is meant to be normal
* printing level.
*/
enum KM_Level	{
	/** Debug messages that should be printed when compiled in debug mode. */
	K_DEBUG = 0,

	/** Normal info about what is going on. */
	K_LOW_INFO,

	/** Only the most important information. */
	K_HIGH_INFO,

	/** Warning messages. */
	K_WARNING,

	/** Error messages, if it causes some failure. */
	K_ERROR,

	/** Info that the user should read or respond to. */
	K_USER,

	/** Unrecoverable error. */
	K_FATAL,

	K_SIZE
};


int kprintf(enum KM_Level kl, const char *fmt, ...);

#endif

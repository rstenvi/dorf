/**
* \file process.h
* Header file for implementing multiprogramming.
*
* Creation of processes follows the UNIX/Linux way of forking and copying on
* write.
*/

#ifndef __PROCESS_H
#define __PROCESS_H

#include "kernel.h"
#include "vmm.h"
#include "fifo.h"
#include "dev.h"
#include "dllist.h"

#include "../hal/isr.h"




/**
Transition between states:
1. PROC_READY -> PROC_RUNNING (choosed for schedule)
2. PROC_RUNNING -> PROC_READY (pre-emptive scheduling or yield())
3. PROC_RUNNING -> PROC_BLOCKED_PROC (waiting for other proc to finish (wait system call))
4. PROC_RUNNING -> PROC_BLOCKED_DEV (device driver is not yet ready to complete call)
5. PROC_BLOCKED_PROC -> PROC_RUNNING (process waited for has finished)
6. PROC_BLOCKED_DEV -> PROC_RUNNING (device has completed operation and can return result)
*/
typedef enum _ProcessState	{
	PROC_RUNNING = 0,
	PROC_READY,
	PROC_BLOCKED_PROC,
	PROC_BLOCKED_DEV,

	// Number of possible states
	PROC_NUM_STATES
} ProcessState;


#define PROC_RUNNING 1
#define PROC_READY   2
#define PROC_BLOCKED 3

#define PROCESS_STATES 10

#define KSTACKSZ KB4

typedef struct _SecurityToken SecurityToken;
typedef struct _pcb pcb;
typedef struct _tcb	tcb;


typedef uint16_t procpri_t;
typedef uint16_t procid_t;



#define PROC_OPEN_MAX 10

typedef struct _DevOpened DevOpened;


#define PROC_READYLIST_MAX 16
#define PROC_BLOCKEDLIST_MAX 16

// Max number of security tokens which can be defined at the same time
#define PROC_MAX_TOKENS 8

#define PROC_PRIORITIES 2
	#define PROC_PRI_NONE 0
	#define PROC_PRI_HIGH 1

typedef enum _Blockedby	{
	BLOCKED_BY_PROC = 1,
	BLOCKED_BY_DEVICE
} Blockedby;


typedef struct _Processes Processes;



/**
* Initialize the first process
*/
Processes* process_init();
tcb* thread_current();
pcb* create_kernel_proc();

// Syscall interfaces

int exec(Registers* regs);
int exit(Registers* reg);
int wait(Registers* reg);
int fork(Registers* regs);
int getpid(Registers* regs);
int yield(Registers* regs);
int nice(Registers* regs);
int open(Registers* regs);
int close(Registers* regs);
int lseek(Registers* regs);
int read(Registers* regs);
int write(Registers* regs);
int getc(Registers* regs);
int putc(Registers* regs);
int fstat(Registers* regs);
int malloc(Registers* regs);
int free(Registers* regs);
int halt_cpu(Registers* regs);


int kill(Registers* regs);
int poweroff(Registers* regs);


void proc_store_registers(Registers*);
int proc_getc_intr(uint32_t devid, uint32_t ch);
int current_pid();

#endif

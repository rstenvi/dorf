#ifndef __ALLOCATE_H
#define __ALLOCATE_H

#include <stddef.h>
#include <stdint.h>

typedef struct _KHeap_block	{
	uint32_t vmmstart,
			blksz;
	struct _KHeap_block* next;
	uint8_t* bitmap;
} __attribute__((packed)) KHeap_block;

typedef struct _KHeap	{
	uint32_t vmmstart,
			blksz,
			maxsz,
			vmflag;
	struct _KHeap_block* next;
	uint8_t* bitmap;	// Bitmap 4KB in size;
} __attribute__((packed)) KHeap;


void heap_free(void* addr);
int heap_free_internal(KHeap* alloc, void* addr);
void* heap_malloc(uint32_t size);
void* heap_malloc_internal(KHeap* alloc, uint32_t size);
void allocate_init(KHeap* alloc, uint32_t start, uint32_t blksz, uint32_t maxsz, uint32_t vmflag);

#endif

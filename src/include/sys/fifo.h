#ifndef __FIFO_H
#define __FIFO_H


typedef int (*fifo_compare)(void*, void*);

typedef struct _Fifo Fifo;


Fifo* fifo_create(uint16_t sz);
int fifo_push(Fifo* f, void* ptr);
void* fifo_pop(Fifo* f);
int fifo_elements(Fifo* f);
void* fifo_find(Fifo* f, fifo_compare cmp, void* s);

#endif

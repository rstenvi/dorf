
#ifndef __EXCEPTION_H
#define __EXCEPTION_H

#include "kernel.h"

typedef struct _Exception	{
	// Expected error code or 0 for any/wildcard
	uint32_t errorcode;
	Registers* regs;

} Exception;


typedef uint32_t kjmpvar;


int ksetjmp(kjmpvar);
void klongjmp(kjmpvar, kerrorcode);






#endif


#ifndef __ELF_H
#define __ELF_H

#include "kernel.h"

int32_t elf_load(uint32_t devid);

#endif

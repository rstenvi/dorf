/**
* \file stdio.h
*/

#ifndef __STDIO_H
#define __STDIO_H

#include <stdarg.h>
#include <stddef.h>

typedef int (*fnptr_t)(unsigned c, void **helper);
int vsprintf(char *buf, const char *fmt, va_list args);
int sprintf(char *buf, const char *fmt, ...);
int snprintf(char *buf, size_t maxlen, const char *fmt, ...);
int vprintf(const char *fmt, va_list args);
int printf(const char *fmt, ...);
int do_printf(const char *fmt, va_list args, fnptr_t fn, void *ptr);

#endif

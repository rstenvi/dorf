/**
* \file dorf.h
*/

#ifndef __DORF_H
#define __DORF_H

#include <stdint.h>

typedef enum _Filetypes	{
	FILE_REGULAR = 0,
	FILE_DIRECTORY,
	FILE_SPECIAL,
	FILE_UNKNOWN
} Filetype;

#define FSTAT_PERM_EXEC  (1 << 0)
#define FSTAT_PERM_WRITE (1 << 1)
#define FSTAT_PERM_READ  (1 << 2)

typedef struct _Filestat	{
	uint32_t size;
	Filetype filetype;
	uint16_t owner, group;
	uint8_t operm, gperm, wperm, sperm;
} __attribute__((packed)) Filestat;

typedef struct _Direntry	{
	uint32_t length;
	char* name;
} __attribute__((packed)) Direntry;


#define SYSCALL_OK                0
#define SYSCALL_WRONG_PARAMETERS -1
#define SYSCALL_NO_SUCH_FILE     -2
#define SYSCALL_ACCESS_DENIED    -3
#define SYSCALL_NO_AVAILABLE_FD  -4
#define SYSCALL_INCOMPAT_PARAMS  -5

#define SYSCALL_UNKNOWN_ERROR    -127

// Flags used in open
#define DORF_OREAD      (1 << 0)
#define DORF_OWRITE     (1 << 1)
#define DORF_OCREATE    (1 << 2)
#define DORF_OEXCLUSIVE (1 << 3)
#define DORF_OINCLUSIVE (1 << 4)
#define DORF_OVIRTUAL   (1 << 5)
#define DORF_EEXEC      (1 << 6)	// Not necessary to specify in user-mode


#endif

/**
* \file vmlayout.h
* Determines the addresses and sizes of the various parts of kernel memory.
* This is all placed in the same place to ensure that they don't overlap.
*/

#ifndef __VMLAYOUT_H
#define __VMLAYOUT_H

#include "sizes.h"
#include "config.h"

#define BIOS_DATA_ADDR  KB1
#define MAIN_BIOS_START (KB1*0x380)
#define MAIN_BIOS_END   (MB1-1)

// Kernel is at 1MB

// The stack, 2 4KB blocks for each CPU core
#define KERNEL_STACK_TOP MB4
#define KERNEL_STACK_SZ  (KB4*2)


#define LAPIC_PHYS_VIRT_ADDR (KERNEL_STACK_TOP - (KERNEL_STACK_SZ*MAX_CPUS) - KB4)
#define KEYBOARD_MAP (LAPIC_PHYS_VIRT_ADDR - KB4)
#define KEYBOARD_MAP_MAX_SZ KB4

#define MAX_KERNEL_MEM (KEYBOARD_MAP-KB4)

#define PROC_VMM_START MB256
#define PROC_VMM_SIZE  (KB4*KB4*8)
#define PROC_VMM_END   (PROC_VMM_START + PROC_VMM_SIZE)

#define PROC_KSTACK_START PROC_VMM_END
#define PROC_KSTACK_SIZE  KB4*KB4*8
#define PROC_KSTACK_END   (PROC_KSTACK_START + PROC_KSTACK_SIZE)

// The kernel heap
#define HEAP_START PROC_KSTACK_END
#define HEAP_SIZE MB256
#define HEAP_END (HEAP_START + HEAP_SIZE)

#define BLKMGR_START HEAP_END
#define BLKMGR_SIZE  KB4*KB4
#define BLKMGR_END   (BLKMGR_START + BLKMGR_SIZE)

#define VMM_CPUS_START BLKMGR_END
#define VMM_CPUS_SIZE (KB4*MAX_CPUS)
#define VMM_CPUS_END (VMM_CPUS_START + VMM_CPUS_SIZE)

#define VMM_TMP_BLOCK_START (VMM_CPUS_END + KB4)
#define VMM_TMP_BLOCK_END   (VMM_TMP_BLOCK_START + KB4)

// Must be changed when adding new sections to always represent end of kernel memory
#define KERNEL_MAX_VM VMM_TMP_BLOCK_END

// First GB is reserved for kernel, then user space
#define USERMODE_START 		GB1
#define USERMODE_HEAP_START GB2
#define USERMODE_HEAP_END   GB3


// Sanity check that we are not using too much VM
#if USERMODE_START < KERNEL_MAX_VM
	#define VM_VALID false
#else
	#define VM_VALID true
#endif


#endif


#include "sys/dev.h"
#include "sys/kernel.h"
#include "sys/allocate.h"
#include "drv/ramdisk.h"


/**
 * Information about a stream device in memory.
 */
typedef struct _mem_drv	{
	addr_t start, end;
} mem_drv;

DeviceOpened* ramdisk_init(addr_t start_addr, addr_t end_addr)	{
	kprintf(K_DEBUG, "RAMDISK: 0x%x -> 0x%x\n", start_addr, end_addr);
	mem_drv* mem = (mem_drv*)heap_malloc(sizeof(mem_drv));
	mem->start = start_addr;
	mem->end = end_addr;

	DeviceOpened* opened = (DeviceOpened*)heap_malloc(sizeof(DeviceOpened));
	dev_fill_device_opened(opened, ramdisk_read, ramdisk_write, dev_error, dev_error, dev_error, mem);
	return opened;
}

DevRet ramdisk_write(void* devcsr, uint8_t* from, uint32_t len, uint32_t offset, uint32_t* wrote)	{
	kprintf(K_DEBUG, "RAMDISK: from: %p | offset: %i\n", from, offset);
	uint32_t i;
	mem_drv* drv = (mem_drv*)devcsr;
	addr_t location = drv->start + offset;
	uint8_t* to = (uint8_t*)location;
	for(i = 0; i < len; i++)	{
		location++;
		if(location >= drv->end)	return DEV_FAILURE_DOWNSTREAM;
		to[i] = from[i];
	}
	*wrote = i;
	return DEV_OK;
}


DevRet ramdisk_read(void* devcsr, uint8_t* to, uint32_t len, uint32_t offset, uint32_t* read)	{
	uint32_t i;
	mem_drv* drv = (mem_drv*)devcsr;
	addr_t location = drv->start + offset;
	uint8_t* from = (uint8_t*)location;
	for(i = 0; i < len; i++)	{
		if(location >= drv->end)	{
			*read = i;
			return DEV_OK;
		}
		to[i] = from[i];
	}
	*read = i;
	return DEV_OK;
}


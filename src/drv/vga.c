/**
* \addtogroup VGA_drv
* @{
* \file vga.c
* Implementaion details for the VGA driver.
*/

#include "sys/kernel.h"
#include "sys/dev.h"
#include "drv/vga.h"
#include "sys/allocate.h"
#include "hal/hal.h"	// outb

/** Max number of characters that can be displaye horizontally. */
#define SCREEN_LENGTH 80

/** Max number of vertical lines. */
#define SCREEN_HEIGHT 25

#define SCREEN_ADDR 0xb8000

#define write_blank(a,b,screen) vga_write_mem(screen,a,b,(uint16_t)' ')

void vga_clear(VgaScreen* screen);

void vga_putc(VgaScreen* screen, char ch);

/**
* Structure defining the entire screen for the VGA driver.
*/
typedef struct _VgaScreen{
	/** Byte for combined foreground and background color. */
	uint8_t color;
	uint8_t x,	/**< Current horizontal index. */
			y;	/**< Current vertical index. */
	uint8_t tab_sz;	/**< Number of spaces in a tab. */
	uint16_t* mem;	/**< Address to the VGA memory. */

	/** Lock to control who has access to the CPU. */
	spinlock lock;
} VgaScreen;



static inline void vga_write_mem(VgaScreen* screen, uint8_t x, uint8_t y, uint16_t val);

static void vga_scroll(VgaScreen* screen);
static void vga_update_cursor(VgaScreen* screen);

Device* vga_create_device(VgaScreen* screen, const char* path);
DevRet vga_dev_write(void* devcsr, uint8_t* buf, uint32_t len, uint32_t offset, uint32_t* wrote);
DevRet vga_dev_putc(void* devcsr, uint8_t ch);
DevRet vga_dev_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* open);
DevRet vga_dev_fstat(void* devcsr, const char* path, Filestat* fstat);


//VgaScreen screen;


Device* vga_init(const char* path, uint32_t startx, uint32_t starty, vga_color fg, vga_color bg)	{
//	init_spinlock(&screen.lock, LOCK_CONSOLE);
//	spinlock_acquire(&screen.lock);
	VgaScreen* screen = (VgaScreen*)heap_malloc(sizeof(VgaScreen));
	screen->x = startx;
	screen->y = starty;
	screen->tab_sz = 4;
	screen->color = (bg << 4) + fg;
	screen->mem = (uint16_t*)SCREEN_ADDR;
	vga_clear(screen);

	Device* dev = dev_alloc_device(path, vga_dev_open, vga_dev_fstat, screen);

//	Device* dev = vga_create_device(&screen, "/dev/stdout");

//	spinlock_release(&screen.lock);

	return dev;
}


/*
Device* vga_create_device(VgaScreen* screen, const char* path)	{
	Device* dev = devmgr_create(DEVTYPE_CONSOLE, path, DEV_STATUS_UNOPENED);

	dev->func->unopened->open = vga_dev_open;
	dev->func->unopened->fstat = vga_dev_fstat;
	dev->info.cat = DEVCAT_STREAM;
	dev->info.blk_sz = 1;

	dev->devcsr = (void*)screen;
	return dev;
}
*/

DevRet vga_dev_fstat(void* devcsr, const char* path, Filestat* fstat)	{
	fstat->filetype = FILE_SPECIAL;
	fstat->owner = 0;
	fstat->group = 0;
	fstat->size = 0;
	fstat->operm = fstat->gperm = fstat->wperm = FSTAT_PERM_WRITE;
	return DEV_OK;
}

DevRet vga_dev_write(void* devcsr, uint8_t* buf, uint32_t len, uint32_t offset, uint32_t* wrote)	{
	uint32_t i;
	for(i = 0; i < len; i++)	{
		vga_putc((VgaScreen*)devcsr, buf[i]);
	}
	*wrote = i;
	return DEV_OK;
}
DevRet vga_dev_putc(void* devcsr, uint8_t ch)	{
	vga_putc((VgaScreen*)devcsr, ch);
	return DEV_OK;
}

DevRet vga_dev_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* open)	{
	dev_fill_device_opened(open, dev_error, vga_dev_write, vga_dev_putc, dev_error, dev_error, devcsr);
	return DEV_OK;
}


void vga_putc(VgaScreen* screen, char ch)	{
	uint16_t write = (uint16_t)ch + ((uint16_t)screen->color << 8);

	// Backspace
	if(ch == 0x08 && screen->x > 0)	{
		screen->x--;
		write_blank(screen->x, screen->y, screen);
	}
	// tab
	else if(ch == 0x09)	{
		screen->x += (screen->tab_sz-((screen->x+screen->tab_sz)%screen->tab_sz));
	}

	else if(ch == '\r')	{
		screen->x = 0;
	}
	else if(ch == '\n')	{
		screen->x = 0;
		screen->y++;
		if(screen->y >= (SCREEN_HEIGHT))	{
			vga_scroll(screen);
		}
	}
	else if(ch >= ' ' && ch <= '~')	{
		vga_write_mem(screen, screen->x, screen->y, write);
		screen->x++;
	}
	else	{
		// Don't know how to handle this yet
		return;
	}

	if(screen->x >= SCREEN_LENGTH)	{
		screen->x = 0;
		screen->y++;
		if(screen->y >= (SCREEN_HEIGHT))	{
			vga_scroll(screen);
		}
	}
	vga_update_cursor(screen);
}

/*
void vga_write(VgaScreen* screen, char* str)	{
	int i = 0;
	while(str[i] != 0x00)	{
		vga_putc(screen, str[i]);
		i++;
	}
}*/

void vga_clear(VgaScreen* screen)	{
	uint16_t fill = ((uint16_t)screen->color << 8) + (uint16_t)' ';
	int i;
	for(i = 0; i < SCREEN_LENGTH * SCREEN_HEIGHT; i++)	{
		screen->mem[i] = fill;
	}
}



static inline void vga_write_mem(VgaScreen* screen, uint8_t x, uint8_t y, uint16_t val)	{
	screen->mem[(y*SCREEN_LENGTH) + x] = val;
}


static void vga_scroll(VgaScreen* screen)	{
	if(screen->y >= SCREEN_HEIGHT)	{
		int i;
		for(i = 0; i < (SCREEN_HEIGHT-1)*SCREEN_LENGTH; i++)	{
			screen->mem[i] = screen->mem[i+SCREEN_LENGTH];
		}
		for(i = (SCREEN_HEIGHT-1)*SCREEN_LENGTH; i <
			SCREEN_HEIGHT*SCREEN_LENGTH; i++)	{
			screen->mem[i] = (uint16_t)' ' + ((uint16_t)screen->color << 8);
		}
		screen->y -= 1;
	}
}

static void vga_update_cursor(VgaScreen* screen)	{
	uint16_t loc = screen->y * SCREEN_LENGTH + screen->x;
	outb(0x3D4, 14);
	outb(0x3D5, loc >> 8);
	outb(0x3D4, 15);
	outb(0x3D5, loc);
}


/** @}*/	// VGA_drv

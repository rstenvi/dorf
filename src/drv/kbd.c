/**
* \file kbd.c
* Generic keyboard driver that handles the logic of the keypresses, but does not
* deal with hardware or the low-level packets.
*
* Design
* - Maintains a type of state machine that changes with the function
* kbd_add_scancode. The state consist of:
*  - Which key-modifiers are pressed, the same key-states that are included in
*  keyboard packet
*  - A sequence of scan codes not forwarded and pending interpretation.
* - Static data
*  - An array of all scancode to key_code mapping, scan code is index. If value
*  is found in this array, value can be forwarded.
*  - If the index is not found in the previous case we must store it.
*  - If we have a pending scan code and get a new one
*
* Key set is loaded as a block of memory with the following format:
* - X 32-bit elements with:
*  - 16-bit key code
*  - 16-bit flags matching some of the flags required in packet
*   - This includes make or break and type of key (numpad, etc)
* - If input is pending a new key, it has the following format
*  - (signed) 16-bit offset to new array
*  - 16-bit flag with only MSB set to 1.
*  - \todo I think this should be feasible
*/

#include "sys/kernel.h"
#include "sys/dev.h"
#include "sys/process.h"
#include "drv/kbd.h"
#include "kbd_constants.h"


typedef struct	{
	uint8_t kc;
	uint8_t flag;
} __attribute__((packed)) sc_kc;

typedef struct	{
	sc_kc map[256];
} __attribute__((packed)) sc_kc_map;


sc_kc_map* orig_map;
sc_kc_map* curr_map;

uint32_t kbd_devid = 0;
uint32_t status = 0;

uint8_t pressed[255];
uint16_t FLAG = 0x0000;	// Global flag for keyboard

#define KBD_REPORT_GETC 1

DevRet kbd_getc(void* devcsr, uint32_t* get);
DevRet kbd_close(void* devcsr);
DevRet kbd_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* open);
DevRet kbd_fstat(void* devcsr, const char* path, Filestat* fstat);


Device* kbd_init(const char* path, uint32_t keymap)	{
	orig_map = (sc_kc_map*)keymap;
	curr_map = (sc_kc_map*)keymap;

	memset(pressed, 0x00, 255);

//	uint32_t status = 0;
	Device* dev = dev_alloc_device(path, kbd_open, kbd_fstat, NULL);

	return dev;

//	Device* dev = devmgr_create(DEVTYPE_KEYBOARD, "/dev/stdin", DEV_STATUS_UNOPENED);
//	if(dev == NULL)	PANIC("Unable to create device\n");

//	dev->func->unopened->open = kbd_open;
//	dev->func->unopened->fstat = kbd_fstat;

//	kbd_dev = dev;

//	dev->devcsr = (void*)status;
}


void kbd_add_scancode(uint8_t sc)	{
	if(curr_map->map[sc].flag & KBD_FLAG_MASK_PEND_MORE)	{
//		uint32_t s = (uint32_t)kbd_dev->devcsr;
		uint32_t a = (uint32_t)orig_map;
		a += (curr_map->map[sc].kc)*256*sizeof(sc_kc);
		curr_map = (sc_kc_map*)a;
	}
	else	{
		uint16_t flag=0;
		curr_map = orig_map;
		uint8_t kc = curr_map->map[sc].kc;
		if(pressed[sc] == 0)	{
			pressed[sc] = 1;
			flag |= KBD_FLAGS_MASK_PRESSED;
			if(kc == KBD_EVENT_LSHIFT || kc == KBD_EVENT_RSHIFT)	{
				FLAG |= KBD_FLAGS_MASK_SHIFT;
			}
			else if(kc == KBD_EVENT_LALT)	{
				FLAG |= KBD_FLAGS_MASK_ALT;
			}
			else if(kc == KBD_EVENT_RALT)	{
				FLAG |= KBD_FLAGS_MASK_ALTGR;
			}
			else if(kc == KBD_EVENT_LCTRL || kc == KBD_EVENT_RCTRL)	{
				FLAG |= KBD_FLAGS_MASK_CTRL;
			}
		}
		else	{
			pressed[sc] = 0;
			if(kc == KBD_EVENT_CAPS)	{
				FLAG |= (~(FLAG & KBD_FLAGS_MASK_CAPSLOCK) & KBD_FLAGS_MASK_CAPSLOCK);
			}

			// TODO: Must handle case where both control key are pressed and one is released
			else if(kc == KBD_EVENT_LSHIFT || kc == KBD_EVENT_RSHIFT)	{
				FLAG &= ~(KBD_FLAGS_MASK_SHIFT);
			}
			else if(kc == KBD_EVENT_LALT)	{
				FLAG &= ~(KBD_FLAGS_MASK_ALT);
			}
			else if(kc == KBD_EVENT_RALT)	{
				FLAG &= ~(KBD_FLAGS_MASK_ALTGR);
			}
			else if(kc == KBD_EVENT_LCTRL || kc == KBD_EVENT_RCTRL)	{
				FLAG &= ~(KBD_FLAGS_MASK_CTRL);
			}
		}
		flag |= FLAG;
		flag <<= 8;
		kprintf(K_DEBUG, "GETC: %x\n", curr_map->map[sc].kc);
		proc_getc_intr( kbd_devid, (flag | curr_map->map[sc].kc));
	}
}

DevRet kbd_fstat(void* devcsr, const char* path, Filestat* fstat)	{
	fstat->filetype = FILE_SPECIAL;
	fstat->owner = fstat->group = 0;
	fstat->size = 0;
	fstat->operm = fstat->gperm = fstat->wperm = FSTAT_PERM_READ;
	return DEV_OK;
}

DevRet kbd_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* open)	{
	dev_fill_device_opened(open, dev_error, dev_error, dev_error, kbd_getc, kbd_close, devcsr);
	kbd_devid = devid;
/*
	*fd = 0;
	Device* dev2 = devmgr_create(DEVTYPE_KEYBOARD, NULL, DEV_STATUS_OPENED_READ);
	ASSERT(dev2 != NULL, "Unable to create device");
	dev2->func->opened_read->getc = kbd_getc;
	kprintf(K_DEBUG, "KBD: devices %x %x %x\n", dev2, dev2->parent->major, dev2->minor);
	*fd = ((dev2->parent->major & 0xffff) << 16) | (dev2->minor);
	kbd_dev = dev2;
	*/
	return DEV_OK;
}

DevRet kbd_close(void* devcsr)	{

	return DEV_FAILURE_UNSUPPORTED;
}



DevRet kbd_getc(void* devcsr, uint32_t* get)	{
	//uint32_t s = (uint32_t)devcsr;
	status |= KBD_REPORT_GETC;

	return DEV_OK_WAITING;
}


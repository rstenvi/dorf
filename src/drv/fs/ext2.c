
#include "dorf.h"
#include "sys/kernel.h"
#include "sys/dev.h"
#include "sys/allocate.h"
#include "drv/fs/ext2.h"


#define EXT2_SB_OFFSET 1024


typedef struct _Ext_sb	{
	uint32_t num_inodes, num_blks, num_res_su, num_unalloc_blks,
			 num_unalloc_inode,
			 sb_blk, blk_sz, frag_sz, blks_in_grp, frags_in_blk_grp,
			 inodes_in_blk_grp, last_mnt, last_write;
	uint16_t times_mounted_check, mounts_bef_chk, sig, states, error,
			 version_minor;
	uint32_t timer, interval_chk, os_id, version_major;
	uint16_t user_id, group_id;
} __attribute__((packed)) Ext_sb;

typedef struct _Ext_extended_sb	{
	uint32_t first_available_inode;
	uint16_t inode_sz, sb_blk_grp;
	uint32_t opt_features, req_features, ro_features;
	uint8_t fsid[16], volname[16], pathname[64];
	uint32_t compression;
	uint8_t prealloc_file, prealloc_dir;
	uint16_t unused;
	uint8_t journal_id[16];
	uint32_t journal_inode, journal_dev, orphan_inode;
} __attribute__((packed)) Ext_extended_sb;

typedef struct _Ext_blk_group	{
	uint32_t blk_addr_blk_bitmap, blk_addr_inode_bitmap, addr_inode_tab;
	uint16_t num_unalloc_blk_grp, num_unalloc_inode_grp, num_dir_grp;
} __attribute__((packed)) Ext_blk_group;

typedef struct _Ext	{
	Ext_sb* sb;
	Ext_extended_sb* sbe;
	Ext_blk_group* first_group;
	uint32_t blksz;
	uint32_t devid;	/**< Dev disk ID for underlying disk. */
} Ext;

typedef struct _Ext_inode	{
	uint16_t type, user_id;
	uint32_t sz_lower, access, creation, modification, deletion;
	uint16_t group_id, count_hard_links;
	uint32_t count_disk_sectors, flags, os_val;
	uint32_t pointers[12], singly_indirect, doubly_indirect, triply_indirect;
	uint32_t gen_num, acl, file_sz_or_acl, blk_addr_fragment;
	uint32_t os_value2[3];
} __attribute__((packed)) Ext_inode;

typedef struct _ExtOpened {
	Ext* ext;
	Ext_inode* inode;
	DeviceOpened* open;
	ExtFiletype ftype;
} ExtOpened;


typedef struct _Ext_file	{
	uint32_t /*inum,*/ position;
	uint8_t* buffer;
	Ext* ext;

	// Should parse all info from inode and not store it
	Ext_inode* inode;
	uint32_t* blocks;
	ExtFiletype ftype;
	uint16_t perm;
} Ext_file;

typedef struct _Ext_dir_entry	{
	uint32_t inode;
	uint16_t sz_entry;
	uint8_t name_length_lsb, type_indicator;
	char* name;
} __attribute__((packed)) Ext_dir_entry;

//Ext_inode* inode2blknum(Ext* ext, uint32_t inode, Ext_inode* in);
//uint32_t* inode2blocks(Ext* ext, Ext_inode* inode, uint32_t* count);
//uint32_t read_file_inode(Ext* ext, Ext_inode* inode, uint8_t* buf, uint32_t maxlen, uint32_t offset);
bool ext2_is_valid(Ext_sb* sb);


// New functions
Ext_inode* inum2inode(Ext* ext, int inum, Ext_inode* in);
int write_inode(Ext* ext, Ext_inode* inode, int inum);
uint32_t ext2_read_internal(
		Ext* ext,
		Ext_inode* inode,
		uint8_t* buf,
		uint32_t len,
		uint32_t offset);
uint32_t inode_blknum2addr(Ext* ext, Ext_inode* inode, uint32_t blknum);


// Remove
//int inode2data(Ext* ext, Ext_inode* inode, uint8_t* data, size_t maxdata, size_t offset);

ExtFiletype ext2_inode2filetype(Ext_inode* inode)	{
	uint16_t ftype = (inode->type & 0xF000);
	switch (ftype)	{
		case 0x1000:	return EXT2_FIFO; 		break;
		case 0x2000:	return EXT2_CHARACTER; 	break;
		case 0x4000:	return EXT2_DIRECTORY; 	break;
		case 0x6000:	return EXT2_BLOCK; 		break;
		case 0x8000:	return EXT2_REGULAR; 	break;
		case 0xA000:	return EXT2_SYMBOLIC; 	break;
		case 0xC000:	return EXT2_SOCKET; 	break;
		default: 		return EXT2_UNKNOWN; 	break;
	}
	return EXT2_UNKNOWN;
}

uint16_t ext2_inode2permissions(Ext_inode* inode)	{
	uint16_t perm = (inode->type & 0x0FFF);

	uint16_t ret = 0;
	ret |= (perm & 0x0007);
	if( (perm & 0x0008) )	ret |= 0x0010;
	if( (perm & 0x0010) )	ret |= 0x0020;
	if( (perm & 0x0020) )	ret |= 0x0040;
	if( (perm & 0x0040) )	ret |= 0x0100;
	if( (perm & 0x0080) )	ret |= 0x0200;
	if( (perm & 0x0100) )	ret |= 0x0400;
	if( (perm & 0x0200) )	ret |= 0x1000;
	if( (perm & 0x0400) )	ret |= 0x2000;
	if( (perm & 0x0800) )	ret |= 0x4000;

	return ret;
}



int path2inum2(Ext* ext, const char* path, uint32_t inum, uint32_t* parent_inum)	{
	Ext_blk_group* blk = ext->first_group;
	*parent_inum = inum;
#define MAX_READ_DIR 2048

	// If no more path, we have reached the end
	if(strlen(path) == 0)	return inum;
	Ext_inode in;
	uint8_t data[MAX_READ_DIR];
	Ext_dir_entry* dentry = NULL;
	while(1)	{
		inum2inode(ext, inum, &in);
		kprintf(K_DEBUG, "EXT: Found inode for inum %i @0x%p, size is %i\n", inum, &in, in.sz_lower);
		if(in.sz_lower >= MAX_READ_DIR)	{
			PANIC("Directory larger than expected");
		}

		uint32_t read = ext2_read_internal(ext, &in, data, in.sz_lower, 0);
		kprintf(K_DEBUG, "EXT: read %i bytes from inode\n", read);

		int len = strlen(path);
		kprintf(K_DEBUG, "EXT: Len of path is %i\n", len);
		char* end = strchr(path, '/');
		if(end != NULL)	len = (end - path);
		kprintf(K_DEBUG, "EXT: End is at %p\n", end);
		kprintf(K_DEBUG, "EXT: Len of path is %i\n", len);

		int i;
		uint32_t add = 0;
		while(add < in.sz_lower)	{
			dentry = (Ext_dir_entry*)(data + add);
			char* dname = (char*)(data + add + 8);
			kprintf(K_DEBUG, "EXT: sz: %i length: %i, name: %s\n", dentry->sz_entry, dentry->name_length_lsb, dname);
			if(dentry->sz_entry == 0)	break;

			if(dentry->name_length_lsb == len && strncmp(path, dname, len) == 0)	{
				*parent_inum = inum;
				inum = dentry->inode;
				if(end == NULL)	{
					return inum;
				}
				break;
			}
			add += dentry->sz_entry;
		}
		if(add >= in.sz_lower || end == NULL)	{
			kprintf(K_ERROR, "Unable to find path: %s\n", path);
			return 0;
		}
		path = end + 1;	// Jump 1 past the slash
		kprintf(K_DEBUG, "Path is now %s\n");
	}
	return 0;
}

int write_data(Ext* ext, uint32_t addr, uint8_t* data, uint32_t bytes)	{
	uint32_t written;
	dev_lseek(ext->devid, addr, DEVMGR_OPEN_SEEK_ABS);
	dev_write(ext->devid, data, bytes, &written);
	return 0;
}

int read_data(Ext* ext, uint32_t addr, uint8_t* data, uint32_t bytes)	{
	uint32_t read;
	dev_lseek(ext->devid, addr, DEVMGR_OPEN_SEEK_ABS);
	dev_read(
		ext->devid,
		data,
		bytes,
		&read
	);
	return read;
}


int read_block_addresses(Ext* ext, uint32_t* blocks, uint8_t* data, size_t num_blocks)	{
	int ret = 0;
	size_t i;
	uint32_t read;
	for(i = 0; i < num_blocks; i++)	{
		uint32_t addr = blocks[i];
		if(addr == 0)	return ret;
		dev_lseek(ext->devid, addr, DEVMGR_OPEN_SEEK_ABS);
		dev_read(
			ext->devid,
			(uint8_t*)(data + (i * ext->blksz)),
			ext->blksz,
			&read
		);
		ret += 1;
		ASSERT(read == ext->blksz, "Unexpected read");
	}
	return ret;
}

uint32_t find_free_inum(Ext* ext)	{
	Ext_blk_group* blk = ext->first_group;
	uint32_t addr = (blk->blk_addr_inode_bitmap * ext->blksz);
	uint32_t ninodes = ext->sb->inodes_in_blk_grp;
	uint32_t bytes = ninodes / 8;

	uint8_t* data = (uint8_t*)heap_malloc(bytes + 1);
	read_data(ext, addr, data, bytes);
	uint32_t i;
	for(i = 0; i < bytes; i++)	{
		if(data[i] != 0xff)	{
			uint32_t j;
			for(j = 0; j < 8; j++)	{
				if((data[i] & (1 << j)) != 0)	{
					heap_free(data);
					return ((i*8) + j);
				}

			}

		}
	}
	// TODO: Can look in next block group
	ASSERT(1, "Unable to find free inode number");
	return 0;
}

Ext_inode* allocate_inode()	{
	Ext_inode* inode = (Ext_inode*)heap_malloc(sizeof(Ext_inode));
	memset(inode, 0x00, sizeof(Ext_inode));
	return inode;
}

Ext_inode* create_generic(ExtFiletype ftype, Ext_inode* inode)	{
	uint32_t val = 0;
	switch(ftype)	{
		case EXT2_FIFO:			val = 0x1000; break;
		case EXT2_CHARACTER:	val = 0x2000; break;
		case EXT2_DIRECTORY:	val = 0x4000; break;
		case EXT2_BLOCK:		val = 0x6000; break;
		case EXT2_REGULAR:		val = 0x8000; break;
		case EXT2_SYMBOLIC:		val = 0xA000; break;
		case EXT2_SOCKET:		val = 0xC000; break;
		default:	kprintf(K_ERROR, "EXT: Invalid value passed to create_generid: %i\n", ftype); break;
	}
	ASSERT(val != 0, "Must select a valid file type");

	// TODO: Permission should be decided elsewhere
	val |= 0x0600;

	inode->type = val;
	inode->user_id = 0;	// All owned by root for now
	inode->sz_lower = 0;

	// TODO: Add a real timestamp when we get a clock
	inode->access = inode->creation = inode->deletion = 0;

	inode->group_id = 0;	// Owned by root-group
	inode->count_hard_links = 1;	// Think this is correct
	inode->count_disk_sectors = 0;	// Disk sectors, NOT ext2 blocks
	inode->flags = 0;

	// The rest are unused or we don't have to set them now

	return inode;
}

int32_t first_free_bitmap(uint8_t* bitmap, uint32_t bytes)	{
	uint32_t i, j;
	for(i = 0; i < bytes; i++)	{
		if(bitmap[i] != 0xff)	{
			for(j = 0; j < 8; j++)	{
				if( (bitmap[i] & (1 << j)) == 0)	{
					return (i*8) + j;
				}
			}
		}
	}
	return -1;
}

uint32_t allocate_block(Ext* ext)	{
	Ext_blk_group* blkgrp = ext->first_group;
	uint8_t* blk_bitmap = (uint8_t*)(blkgrp->blk_addr_blk_bitmap * ext->blksz);
	uint32_t bytes = (ext->sb->blks_in_grp / 8);
	uint8_t* data = (uint8_t*)heap_malloc(bytes);
	read_data(ext, (uint32_t)blk_bitmap, data, bytes);

	int32_t idx = first_free_bitmap(data, bytes);
	if(idx < 0)	{
		// TODO: Look in next block group
		PANIC("Unable to find free block in block group");
	}

	// Mark bit as taken
	data[idx/8] |= (1 << (idx%8));
	write_data(ext, (uint32_t)blk_bitmap, data, bytes);

	// TODO: Should also update block descriptor table with how many blocks have
	// been allocated

	// Return address to the block
	if(ext->blksz < 2048)	idx++;
	heap_free(data);
	return (idx * ext->blksz);
}

/*
int append_inode(Ext* ext, Ext_inode* inode, uint32_t inum, uint8_t* datain, uint32_t dlen)	{
	uint8_t* data = datain;	// We need to change the pointer

	uint32_t offset = inode->sz_lower;
	uint32_t last_blk = inode->sz_lower / ext->blksz;
	if((inode->sz_lower % ext->blksz) == 0)	last_blk--;

	// Remaining space in last, in-use, block
	uint32_t rem_space = (ext->blksz - (inode->sz_lower % ext->blksz));
	if(rem_space == ext->blksz)	rem_space = 0;	// Edge-case where the formula is wrong

	if(rem_space > 0)	{
		uint32_t write = (rem_space < dlen) ? rem_space : dlen;
		if(last_blk < 12)	{
			write_data(ext, inode->pointers[last_blk] + (inode->sz_lower % ext->blksz), data, write);
		}
		else	{
			PANIC("Indirect pointers not supported yet");
		}

		// Update inode with new length
		inode->sz_lower += write;
		if(rem_space < dlen)	{
			// More data to write
			return append_inode(ext, inode, inum, &(data[write]), (dlen - write));
		}
	}
	else if(dlen > ext->blksz)	{
		// Must allocate one or more new block(s)
		// We first write all the full blocks
		uint32_t new_blks = dlen / ext->blksz;
		uint32_t i;
		for(i = 0; i < new_blks; i++)	{
			uint32_t currblk = last_blk + i + 1;
			uint32_t dataoffset = (i * ext->blksz);
			if(currblk < 12)	{
				uint32_t addr = allocate_block(ext);
				inode->pointers[currblk] = addr;
				write_data(ext, inode->pointers[currblk], &(data[dataoffset]), ext->blksz);
			}
			else	{
				PANIC("Indirect blocks are supported yet");
			}
		}
		uint32_t addsz = new_blks * ext->blksz;
		inode->sz_lower += addsz;

		uint32_t rem_data = dlen % ext->blksz;
		if(rem_data > 0)	{
			return append_inode(ext, inode, inum, &(datain[addsz]), dlen - addsz);
		}
	}
	else	{
		// Need to allocate a new block and not write to the end
		if(last_blk < 11)	{
			uint32_t addr = allocate_block(ext);
			inode->pointers[last_blk+1] = addr;
			write_data(ext, inode->pointers[last_blk+1], data, dlen);
		}
		else	{
			PANIC("Indirect blocks are supported yet");
		}
	}

	// Write the inode back
	write_inode(ext, inode, inum);
	return 0;
}
*/

/*
int append_inum(Ext* ext, uint32_t inum, uint8_t* datain, uint32_t dlen)	{
	uint8_t* data = datain;	// We need to change the pointer

	Ext_inode* inode = (Ext_inode*)heap_malloc(sizeof(Ext_inode));
	inode = inum2inode(ext, inum, inode);

	int res = append_inode(ext, inode, inum, data, dlen);

	heap_free(inode);

	return res;
}
*/

int write_inode(Ext* ext, Ext_inode* inode, int inum)	{
	// Find where the inode should be written
	uint32_t blk_group_num = (inum - 1) / ext->sb->inodes_in_blk_grp;
	ASSERT(blk_group_num == 0, "Block group for inode is higher than 0, not implemented yet");
	uint32_t blk_index = (inum -1) % ext->sb->inodes_in_blk_grp;
	Ext_blk_group* grp = ext->first_group;


	uint32_t addr = (grp->addr_inode_tab * ext->blksz) + (sizeof(Ext_inode) * blk_index);
	write_data(ext, addr, (uint8_t*)inode, sizeof(Ext_inode));
	return 0;

}

Ext_dir_entry* create_dir_entry(Ext* ext, uint32_t inum, const char* name)	{
	uint32_t nlen = strlen(name);
	uint32_t rlen = nlen;
	nlen += 4 - (nlen % 4);
	uint32_t dirsz = 8 + nlen;
	Ext_dir_entry* dir = (Ext_dir_entry*)heap_malloc(dirsz);
	dir->inode = inum;
	dir->sz_entry = dirsz;
	ASSERT(nlen < 256, "Length of file name is too long");
	dir->name_length_lsb = (uint8_t)(nlen & 0xff);
	dir->type_indicator = 0;
	char* dname = (char*)dir + 8;
	memset(dname, 0x00, nlen);
	memcpy(dname, name, rlen);
	return dir;
}

int add_dir_entry_buf(Ext_dir_entry* entry, uint8_t* buf, int maxlen)	{
	int curroff = 0;
	while(curroff < maxlen)	{
		Ext_dir_entry* curr = (Ext_dir_entry*)(buf + curroff);
		uint32_t lencheck = curr->name_length_lsb;
		lencheck += 4 - (lencheck % 4);	// Should be divisible by 4
		lencheck += 8;	// Remaining structure
		if(curr->sz_entry <= lencheck)	{
			// This entry is in use
			curroff += curr->sz_entry;
		}
		else if((curr->sz_entry - lencheck) >= entry->sz_entry)	{
			kprintf(K_DEBUG, "Found space for entry");
			kprintf(K_DEBUG, "Space at off %i sz: %i | %i\n", curroff, curr->sz_entry, curr->name_length_lsb);
			uint32_t oldsz = curr->sz_entry;
			uint32_t copylen = entry->sz_entry;
			curr->sz_entry = lencheck;
			entry->sz_entry = oldsz - lencheck;
			curroff += lencheck;
			memcpy((buf + curroff), (uint8_t*)entry, copylen);

			kprintf(K_DEBUG, "old sz: %i\n", curr->sz_entry);
			kprintf(K_DEBUG, "new sz: %i\n", entry->sz_entry);
			return 0;
		}
	}
	return 1;
}

int add_dir_entry(Ext* ext, uint32_t parent_inum, uint32_t inum, const char* fname)	{
	uint8_t buf[1024];
	Ext_dir_entry* dentry = create_dir_entry(ext, inum, fname);
	Ext_inode* dirinode = allocate_inode();
	dirinode = inum2inode(ext, parent_inum, dirinode);

//	uint32_t read = ext2_read_internal(ext, dirinode, buf, 1024, 0);
	uint32_t addr = inode_blknum2addr(ext, dirinode, 0);
	uint32_t read = read_data(ext, addr, buf, 1024);
	kprintf(K_DEBUG, "ReadXXX %i bytes\n", read);
	if(add_dir_entry_buf(dentry, buf, 1024) == 0)	{
//		ASSERT(addr != 0, "inode_blknum2addr returned 0");
		write_data(ext, addr, buf, 1024);
	}
	else	{
		PANIC("Unable to add dir entry");
	}

	kprintf(K_DEBUG, "Inode=%i, size=%i\n", parent_inum, dirinode->sz_lower);
	heap_free(dentry);
	return 0;
}

int create_and_write_inode(Ext* ext, Ext_inode* inode, ExtFiletype ftype, uint32_t parent_inum, const char* fname)	{
	inode = create_generic(ftype, inode);
	int inum = find_free_inum(ext);
	kprintf(K_DEBUG, "Found free inum: %i\n", inum);
	if(inum > 0)	{
		write_inode(ext, inode, inum);
		add_dir_entry(ext, parent_inum, inum, fname);

	}
	else	{
		PANIC("Unable to find free inum");
	}
	return inum;
}


/*
int inode2data(Ext* ext, Ext_inode* inode, uint8_t* data, size_t maxdata, size_t offset)	{
	uint32_t blknum = (offset / ext->blksz);
	uint32_t blk_offset = (offset % ext->blksz);

	uint32_t bytes_read = 0;

	// Can never read more than the size of the file
	if(maxdata > inode->sz_lower)	{
		maxdata = inode->sz_lower;
	}

	// How many blocks we should try to read
	uint32_t blks = maxdata / ext->blksz;
	uint32_t blks_off = maxdata % ext->blksz;	// Offset into block we need to read
	if(blknum < 12)	{
		// Sometimes we may also need to read from indirect block
		uint32_t read = ((blks + blknum) < 12)  ? blks : 12 - blknum;
		uint32_t* blocks = &(inode->pointers[blknum]);
//int read_data(Ext* ext, uint32_t addr, uint8_t* data, uint32_t bytes)	{
//int read_block_addresses(Ext* ext, uint32_t* blocks, uint8_t* data, size_t num_blocks)	{
//		uint32_t res = read_block(ext, blocks, data, read);
		uint32_t res = read_block_addresses(ext, blocks, data, read);

		bytes_read = read * ext->blksz;

		if(read < blks)	{
			// Continue reading on indirect blocks
			blknum = 12;
			maxdata -= (read * ext->blksz);
		}
		else if(blks_off != 0)	{
			// Check if last piece of data is in indirect block
			if( (read + blknum) >= 12)	{
				blknum = 12;
				maxdata = blks_off;
			}
			else	{
				PANIC("ERROR");
//				uint32_t res = read_data(ext, addr, (data + bytes_read), blks_off);
			}

		}
	}
	if(blknum >= 12)	{
		PANIC("Not supported yet");
	}
	return bytes_read;
}
*/

Ext_inode* inum2inode(Ext* ext, int inum, Ext_inode* in)	{
	uint32_t blk_group_num = (inum - 1) / ext->sb->inodes_in_blk_grp;
	ASSERT(blk_group_num == 0, "Block group for inode is higher than 0, not implemented yet");
	uint32_t blk_index = (inum -1) % ext->sb->inodes_in_blk_grp;
	Ext_blk_group* grp = ext->first_group;


	// TODO: Walk block group and find correct group
	// Must first read in all block groups, this is not done in the
	// init-function.
	// TODO: Should store inode table for faster lookups, can do that here by
	// reading in the allocated ones. block group should specify how many are
	// allocated, for simplicity, we can assume that the first are allocated. If
	// we need one outside of that range, we must read in.
	uint32_t addr = (grp->addr_inode_tab * ext->blksz) + (sizeof(Ext_inode) * blk_index);
	kprintf(K_DEBUG, "EXT: Reading inode from addr 0x%x\n", addr);


	uint32_t read;
	dev_lseek(ext->devid, addr, DEVMGR_OPEN_SEEK_ABS);
	dev_read(
		ext->devid,
		(uint8_t*)in,
		sizeof(Ext_inode),
		&read
	);
	uint8_t* t = (uint8_t*)in;
	kprintf(K_DEBUG, "Data is %x%x\n", t[0], t[1]);
	ASSERT(read == sizeof(Ext_inode), "Read unexpected bytes of data");
	kprintf(K_DEBUG, "EXT: block pointer[0] = %i\n", in->pointers[0]);
	return in;
}


/*
Device* ext2_create_open_dev(Ext* ext, const char* path, uint32_t mode, Ext_inode* inode)	{
	kprintf(K_DEBUG, "EXT: create open %s, %i\n", path, mode);

	Ext_file* file = (Ext_file*)heap_malloc(sizeof(Ext_file));
//	file->inum = inum;
	file->position = 0;
	file->buffer = NULL;
	file->ext = ext;
	file->inode = inode;

	file->ftype = ext2_inode2filetype(inode);
	file->perm = ext2_inode2permissions(inode);

//	uint32_t count = 0;
//	inode2blocks(ext, inode, &count);
//	file->blocks = inode2blocks(ext, inode, &count);
//	kprintf(K_DEBUG, "EXT: Found %i blocks\n", count);


	// Create final Device
	Device* dev = (Device*)heap_malloc(sizeof(Device));
	memset(dev, sizeof(Device), 0x00);

	dev->devcsr = (void*)file;
	dev->info.cat = DEVCAT_VIRTUAL;
	dev->info.blk_sz = 1;

	Dev_open* open = (Dev_open*)heap_malloc(sizeof(Dev_open));
	open->offset = 0;
	open->mode = mode;
	open->sz = 0;

	dev->open = open;

	dev->func = devmgr_create_device(DEV_STATUS_OPENED_READ);
	dev->func->opened_read->close = ext2_close;
	dev->func->opened_read->read = ext2_read;
	dev->func->opened_read->getc = (dev_getc)devmgr_error;

	return dev;
}
*/

/*
int32_t path2inum(Ext* ext, const char* path, uint32_t* inum, Ext_inode* inode)	{
	// Mount path is stripped, so if we get nothing, we return root '/'
	*inum = 2;

	char* buf = (char*)heap_malloc(1024);
	if(strlen(path) > 0)	{
		memset(buf, 0x00, 1024);
		*inum = path2inode(ext, path, 2, inode, buf, 1024);
		if(*inum == 0)	{
			heap_free(buf);
			return DEVMGR_NO_SUCH_FILE;
		}
	}
	heap_free(buf);
	return 0;
}
*/

DevRet ext2_fstat(void* devcsr, const char* path, Filestat* fstat)	{
	kprintf(K_DEBUG, "EXT: fstat %s\n", path);
	Ext* ext = (Ext*)devcsr;

	// Get inode number for path
	uint32_t parent = 0;
	uint32_t inum = path2inum2(ext, path, 2, &parent);
	kprintf(K_DEBUG, "Found inum: %i\n", inum);
	if(inum == 0)	return DEV_NO_SUCH_FILE;

	Ext_inode* inode = allocate_inode();
	inode = inum2inode(ext, inum, inode);

	ExtFiletype ftype = ext2_inode2filetype(inode);
	switch(ftype)	{
		case EXT2_FIFO:
		case EXT2_CHARACTER:
		case EXT2_BLOCK:
		case EXT2_SYMBOLIC:
		case EXT2_SOCKET:
		case EXT2_REGULAR:
			fstat->filetype = FILE_REGULAR;
			break;
		case EXT2_DIRECTORY:
			fstat->filetype = FILE_DIRECTORY;
			break;
		default:
			fstat->filetype = FILE_UNKNOWN;
			break;
	}
	fstat->size = inode->sz_lower;
	uint16_t perm = (inode->type & 0x0fff);	// The first 12 bits is the permission
	fstat->wperm = (uint8_t)(perm & 0x0007);
	fstat->gperm = (uint8_t)((perm >> 3) & 0x0007);
	fstat->operm = (uint8_t)((perm >> 6) & 0x0007);
	fstat->sperm = (uint8_t)((perm >> 9) & 0x0007);	// sticky bit, set group id, set user id

	fstat->owner = inode->user_id;
	fstat->group = inode->group_id;
	// TODO: In ext2 1.0, there may be some additional values that we don't parse

	heap_free(inode);
	return DEV_OK;
}

char* path2filename(const char* path)	{
	char* copy = (char*)path, * test = NULL;
	while( (test = strchr(copy, '/')) != NULL)	{ copy = test + 1; }
	return copy;
}

Ext_inode* ext2_open_internal(Ext* ext, const char* path, uint32_t mode)	{
	uint32_t parent_inum;
	Ext_inode* inode = allocate_inode();
	kprintf(K_DEBUG, "EXT: allocated inode\n");
	uint32_t inum = path2inum2(ext, path, 2, &parent_inum);
	kprintf(K_DEBUG, "Found inum at %i\n", inum);

	if(inum == 0)	{
		if((mode & 0xbb) != 0)	{
			// File does not exist, we need to create it
			char* filename = path2filename(path);
			kprintf(K_DEBUG, "EXT: filename is: %s\n", filename);
			int ninum = create_and_write_inode(ext, inode, EXT2_REGULAR, parent_inum, filename);
			kprintf(K_DEBUG, "New inum: %i\n", ninum);
			return inode;
		}
		else	{
			PANIC("Unsupported");
		}
	}
	else	{
		inode = inum2inode(ext, inum, inode);
		ASSERT(inode != NULL, "inode == null");
		kprintf(K_DEBUG, "Found inode @0x%p\n", inode);
		return inode;
	}
	return NULL;
}

DevRet ext2_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* opened)	{
	kprintf(K_DEBUG, "EXT: open %s (%i), %i\n", path, strlen(path), mode);
	Ext* ext = (Ext*)devcsr;


	Ext_inode* inode = ext2_open_internal(ext, path, mode);
	ASSERT(inode != NULL, "inode == NULL");
	kprintf(K_DEBUG, "EXT: Allcoated inode @0x%p\n", inode);

	ExtOpened* eopened = (ExtOpened*)heap_malloc(sizeof(ExtOpened));
	eopened->ext = ext;
	eopened->open = opened;
	eopened->inode = inode;
	eopened->ftype = ext2_inode2filetype(inode);

	dev_fill_device_opened(opened, ext2_read, ext2_write, dev_error, dev_error, ext2_close, (void*)eopened);

//	Device* dev_o = ext2_create_open_dev((Ext*)dev->devcsr, path, mode, inode);
//	int32_t ret = devmgr_add_minor(DEVTYPE_FILE, dev_o, NULL);
	//*fd = (ret | (DEVTYPE_FILE << 16));

	return DEV_OK;
}

int blknum2location(Ext* ext, uint32_t blknum)	{
	uint32_t pblk = ext->blksz / 4;
	if(blknum < 12)	return 1;
	else if(blknum < pblk + 12)	return 2;
	else if(blknum < 12 + pblk + (pblk * pblk))	return 3;
	else if (blknum < 12 + pblk + (pblk * pblk) + (pblk * pblk * pblk))	return 4;
	return 0;
}

uint32_t inode_blknum2addr(Ext* ext, Ext_inode* inode, uint32_t blknum)	{
	int blkcat = blknum2location(ext, blknum);
	ASSERT(blkcat != 0, "Asking for a block which is outside file space");
	if(blkcat == 1)	{
		return inode->pointers[blknum] * ext->blksz;
	}
	else if(blkcat == 2)	{
		uint32_t addr1 = inode->singly_indirect * ext->blksz;
		uint32_t blkoffset = blknum - 12;
		uint32_t input;

		// TODO: Inefficient
		read_data(ext, addr1 + (4 * blkoffset), (uint8_t*)&input, 4);
		return input * ext->blksz;
	}
	else if(blkcat == 3)	{
		uint32_t addr1 = inode->doubly_indirect * ext->blksz;
		uint32_t pblk = ext->blksz / 4;
		uint32_t blkoffset = blknum - 12 - pblk;
		uint32_t ind = blkoffset / pblk;
		uint32_t off = blkoffset % pblk;
		uint32_t inp1, inp2;

		read_data(ext, addr1 + (ind * 4), (uint8_t*)&inp1, 4);

		inp1 = inp1 * ext->blksz;
		read_data(ext, inp1 + (off * 4), (uint8_t*)&inp2, 4);

		return inp2 * ext->blksz;
	}
	else if(blkcat == 4)	{
		uint32_t addr1 = inode->triply_indirect * ext->blksz;
		uint32_t pblk = ext->blksz / 4;
		uint32_t blkoffset = blknum - 12 - pblk - (pblk * pblk);
		uint32_t ind1 = blkoffset / (pblk * pblk);
		uint32_t rem = blkoffset % (pblk * pblk);
		uint32_t ind2 = rem / pblk;
		uint32_t ind3 = rem % pblk;
		uint32_t inp1, inp2, inp3;

		read_data(ext, addr1 + (ind1 * 4), (uint8_t*)&inp1, 4);
		inp1 = inp1 * ext->blksz;

		read_data(ext, inp1 + (ind2 * 4), (uint8_t*)&inp2, 4);
		inp2 = inp2 * ext->blksz;

		read_data(ext, inp2 + (ind3 * 4), (uint8_t*)&inp3, 4);
		inp3 = inp3 * ext->blksz;
		return inp3;
	}
	PANIC("Unsupported blknum");
	return 0;
}

uint32_t ext2_read_internal(
		Ext* ext,
		Ext_inode* inode,
		uint8_t* buf,
		uint32_t len,
		uint32_t offset)	{

	uint32_t start_blk = (offset / ext->blksz);
	uint32_t start_block_offset = (offset % ext->blksz);
	uint32_t rlen = ((len + offset) <= inode->sz_lower) ? len : inode->sz_lower - offset;

	uint32_t end_blk = ((offset + rlen) / ext->blksz);
	if( ((offset + rlen) % ext->blksz) == 0)	end_blk--;
	uint32_t end_block_offset = ext->blksz;

	uint8_t* data = buf;	// We might change the pointer

	uint32_t read = 0;

	kprintf(K_DEBUG, "file size: %i | rlen: %i | startblk: %i | endblk: %i | startoffset: %i\n",
			inode->sz_lower, rlen, start_blk, end_blk, start_block_offset);


	// 4 cases:
	// 1. Read from zero to some part of block
	// 2. Read from zero and whole block
	// 3. Read from offset and some part of block
	// 4. Read from offset and whole block

	uint32_t i;
	for(i = start_blk; i <= end_blk; i++)	{
		kprintf(K_DEBUG, "Reading block: %i\n", i);
		if(i == end_blk)	{
			// If last block, we must calculate end offset
			end_block_offset = (offset + rlen) % ext->blksz;
			if(end_block_offset == 0)	end_block_offset = ext->blksz;
			kprintf(K_DEBUG, "endblkoffset: %i\n", end_block_offset);
		}
		uint32_t addr = inode_blknum2addr(ext, inode, i);
		kprintf(K_DEBUG, "Readin addr 0x%x\n", addr);
		read_data(ext, addr + start_block_offset, data, (end_block_offset - start_block_offset));

		data += (end_block_offset - start_block_offset);	// Change the data pointer
		read += (end_block_offset - start_block_offset);
		start_block_offset = 0;		// Can only be non-zero on first run
	}
	kprintf(K_DEBUG, "EXT: Read %i bytes\n", read);
	return read;
}

uint32_t buffer2dirlist(Ext* ext, uint8_t* buf, uint32_t len)	{
	Ext_dir_entry* curr = (Ext_dir_entry*)buf;
	Direntry* d;
	uint32_t add = 0;		// Number to add to custom dir entry
	uint32_t badd = 0;		// Number to add to ext2 dir entry

	// TODO: The code below will overwrite the data it's copying from. This
	// works fine as long as our custom direntry structure is smaller than the
	// ext2 one and as long we copy current entry data before we overwrite with
	// name
	while(badd < len)	{
		curr = (Ext_dir_entry*)(buf + badd);
		uint32_t currsz = curr->sz_entry;
		if(curr->sz_entry <= 0)	break;

		d = (Direntry*)(buf + add);
		char* dname = (char*)(buf + add + 4);
		char* cname = (char*)(buf + badd + 8);

		d->length = (curr->name_length_lsb & 0xFF);

		memcpy(dname, cname, d->length);

		add += d->length + sizeof(uint32_t);
		badd += currsz;
	}
	return add;
}


DevRet ext2_read(void* devcsr, uint8_t* buf, uint32_t len, uint32_t offset, uint32_t* read_ret)	{
	kprintf(K_DEBUG, "EXT reading %i bytes from offset %i to buffer 0x%x\n", len , offset, buf);
	ExtOpened* file = (ExtOpened*)devcsr;
//	Ext_file* file = (Ext_file*)dev->devcsr;

	*read_ret = ext2_read_internal(file->ext, file->inode, buf, len, offset);

	if(file->ftype == EXT2_DIRECTORY)	{
		kprintf(K_DEBUG, "EXT: Reading directory: %i bytes\n", *read_ret);
		*read_ret = buffer2dirlist(file->ext, buf, *read_ret);

	}

	return DEV_OK;
}


DevRet ext2_write(void* devcsr, uint8_t* buf, uint32_t len, uint32_t offset, uint32_t* wrote)	{

	return DEV_FAILURE_UNSUPPORTED;
}

DevRet ext2_close(void* devcsr)	{
	ExtOpened* open = (ExtOpened*)devcsr;
	// TODO:
	// - Flush any remaining writes
	kprintf(K_DEBUG, "EXT: close\n");
	heap_free(open->inode);
	heap_free(open);
	return DEV_OK;
}

Device* ext2_init(uint32_t devid, const char* rpath)	{
	kprintf(K_DEBUG, "ext2 start\n");
	uint8_t buf[1024];
	uint32_t read;

	// TODO: What if it's a block device
	dev_lseek(devid, EXT2_SB_OFFSET, DEVMGR_OPEN_SEEK_ABS);
	dev_read(devid, buf, 1024, &read);

	Ext_sb* sb = (Ext_sb*)heap_malloc(sizeof(Ext_sb));
	Ext_extended_sb* sbe = (Ext_extended_sb*)heap_malloc(sizeof(Ext_extended_sb));

	memcpy(sb, buf, sizeof(Ext_sb));
	memcpy(sbe, (buf + sizeof(Ext_sb)), sizeof(Ext_extended_sb));

	// Check if valid
	if(ext2_is_valid(sb) == false)	return NULL;

	// Need to set default values if Major version < 1.0
	if(sb->version_minor < 1)	{
		sbe->first_available_inode = 11;
		sbe->inode_sz = 128;
	}

	// First block group is at block 2 if block size is 1024
	// Otherwise it is at block 1
	// NOTE: Starting from 0
	uint32_t blk_group_addr = 2048;
	if (sb->blk_sz != 0)	blk_group_addr = (1024 >> sb->blk_sz) * 2;

	// Read in block group descriptor
	dev_lseek(devid, blk_group_addr, DEVMGR_OPEN_SEEK_ABS);
	dev_read(devid, buf, sizeof(Ext_blk_group), &read);

	Ext_blk_group* blkgroup = (Ext_blk_group*)heap_malloc(sizeof(Ext_blk_group));
	memcpy(blkgroup, buf, sizeof(Ext_blk_group));

	// Fill in ext2 values
	Ext* ext = (Ext*)heap_malloc(sizeof(Ext));
	ext->first_group = blkgroup;
	ext->sb = sb;
	ext->sbe = sbe;
	ext->devid = devid;
	ext->blksz = (1024 >> sb->blk_sz);

	Device* ret = dev_alloc_device(rpath, ext2_open, ext2_fstat, (void*)ext);
	return ret;

/*
	// Create final Device
	dev->devcsr = (void*)ext;
	dev->info.cat = DEVCAT_VIRTUAL;
	dev->info.blk_sz = 1;

	dev->func = devmgr_create_device(DEV_STATUS_UNOPENED);
	dev->func->unopened->open = ext2_open;
	dev->func->unopened->fstat = ext2_fstat;

	// TODO: I think it makes most sense to path mount point as parameter
	return devmgr_add_minor(DEVTYPE_FS, dev, NULL);
	*/
}


/*
Ext_inode* inode2blknum(Ext* ext, uint32_t inode, Ext_inode* in)	{
	uint32_t blk_grp = (inode -1)/ext->sb->inodes_in_blk_grp;
	uint32_t blk_index = (inode -1) % ext->sb->inodes_in_blk_grp;
	uint32_t contain_blk = (blk_index * ext->sbe->inode_sz) / ext->blksz;
	uint32_t read;

	// TODO: Actually find which block group to use
	uint32_t inode_tab_blk = ext->first_group->addr_inode_tab;
	devmgr_lseek(ext->devid, (ext->blksz * inode_tab_blk) + (sizeof(Ext_inode) * blk_index), DEVMGR_OPEN_SEEK_ABS);
	devmgr_read(
		ext->devid,
		(uint8_t*)in,
		sizeof(Ext_inode),
		&read
	);
	return in;
}
*/

/*
uint32_t path2inode(
		Ext* ext,
		const char* path,
		uint32_t inum,
		Ext_inode* inode,
		char* buf,
		uint32_t maxbuf
	)	{

	kprintf(K_DEBUG, "EXT2: looking for %s\n", path);

	inode2blknum(ext, inum, inode);

	uint32_t read = read_file_inode(ext, inode, (uint8_t*)buf, maxbuf, 0);
	if(read == 0)	{
		PANIC("Zero bytes read\n");
	}

	#define EXT2_MAXLEN 64

	char search[EXT2_MAXLEN];
	char* match = strchr(path, '/');
	int len = 0;

	if(match != NULL)	{
		len = ((int)match - (int)path);
		kprintf(K_DEBUG, "EXT2: match: %s (%p - %p) Len is %i\n", match, match, path, len);
		if(len < EXT2_MAXLEN)	{
			memcpy(search, path, len);
			search[len] = 0x00;
		}
		else	{
			PANIC("EXT2: filename is too long");
		}
	}
	else	{
		strncpy(search, path, EXT2_MAXLEN-1);
	}
	kprintf(K_DEBUG, "EXT2: first match %s\n", search);

	uint32_t curr = 0;
	if((inode->type & 0xF000) == 0x4000)	{
		while(curr < 1024)	{
			Ext_dir_entry* e = (Ext_dir_entry*)&buf[curr];

			// TODO: No file should be longer that 255 characters
			uint16_t len = e->name_length_lsb;
			char* name = &buf[curr+8];
			if(e->inode == 0)	break;
			if( (strlen(search) == len) && (memcmp(search, name, len) == 0) )	{
				path += len + 1;
				if(*path == 0x00)	return e->inode;
				else	{
					kprintf(K_DEBUG, "EXT2: New path %s\n", path);
					return path2inode(ext, path, e->inode, inode, buf, maxbuf);
				}
			}
			curr += e->sz_entry;
		}
	}
	return 0;
}
*/

// TODO: Handle linked pointers
// - Should be able to separate this into a separate function that takes an
// array
/*
uint32_t read_file_inode(Ext* ext, Ext_inode* inode, uint8_t* buf, uint32_t maxlen, uint32_t offset)	{
	int i = 0;
	uint32_t read = 0, tmp;
	for(i = 0; i < 12; i++)	{
		if(inode->pointers[i] != 0)	{
			if(read <= maxlen)	{
				devmgr_lseek(ext->devid, ext->blksz*inode->pointers[i], DEVMGR_OPEN_SEEK_ABS);
				devmgr_read(
					ext->devid,
					&buf[read],
					ext->blksz,
					&tmp
				);
				read += ext->blksz;
			}
			else	{
				return read;
			}
		}
		else	{
			return read;
		}
	}
	return read;
}
*/

/*
uint32_t* inode2blocks(Ext* ext, Ext_inode* inode, uint32_t* count)	{
	kprintf(K_DEBUG, "EXT: inode2blocks %i\n", *count);

	uint32_t blocks = 0, i, j, k;
	uint32_t* ret = NULL, tmp;

	if(*count != 0)	{
		uint32_t sz = *(count) * sizeof(uint32_t);
		ret = (uint32_t*)heap_malloc(sz);
	}

	for(i = 0; i < 12; i++)	{
		if(inode->pointers[i] != 0)	{
			if(ret != NULL)	ret[blocks] = inode->pointers[i];
			blocks++;
		}
	}

	if(inode->singly_indirect != 0)	{
		uint32_t* read = (uint32_t*)heap_malloc(ext->blksz+1);
		devmgr_lseek(ext->devid,ext->blksz*inode->singly_indirect, DEVMGR_OPEN_SEEK_ABS);
		devmgr_read(
			ext->devid, (uint8_t*)read, ext->blksz, &tmp
		);
		for(i = 0; i < ext->blksz/4; i++)	{
			if(read[i] != 0)	{
				if(ret != NULL)	ret[blocks] = read[i];
				blocks++;
			}
		}
		heap_free(read);
	}
	if(inode->doubly_indirect != 0)	{
		uint32_t* read = (uint32_t*)heap_malloc(ext->blksz+1);
		devmgr_lseek(ext->devid, ext->blksz*inode->singly_indirect, DEVMGR_OPEN_SEEK_ABS);
		devmgr_read(
			ext->devid, (uint8_t*)read, ext->blksz, &tmp
		);
		uint32_t* read2 = (uint32_t*)heap_malloc(ext->blksz);
		for(i = 0; i < ext->blksz/4; i++)	{
			devmgr_lseek(ext->devid, ext->blksz*read[i], DEVMGR_OPEN_SEEK_ABS);
			devmgr_read(
				ext->devid, (uint8_t*)read2, ext->blksz, &tmp
			);
			for(j = 0; j < ext->blksz/4; j++)	{
				if(read2[j] != 0)	{
					if(ret != NULL)	ret[blocks] = read2[j];
					blocks++;
				}
			}
		}
		heap_free(read);
		heap_free(read2);
	}

	if(inode->triply_indirect != 0)	{
		// TODO: do triply indirect
		PANIC("Triple indirects are not supported");
	}

	if(*count == 0)	{
		*count = blocks;
	}
	return ret;
}
*/


bool ext2_is_valid(Ext_sb* sb)	{
	if(sb->sig != EXT2_SIG)	return false;
	return true;
}


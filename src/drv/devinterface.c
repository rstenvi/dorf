#include "dorf.h"
#include "sys/kernel.h"
#include "sys/dev.h"
#include "sys/allocate.h"


Device* devinterface_create_device();
DevRet devinterface_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* open);
DevRet devinterface_close(void* devcsr);
DevRet devinterface_fstat(void* devcsr, const char* path, Filestat* stat);
DevRet devinterface_read(void* devcsr, uint8_t* buf, uint32_t maxlen, uint32_t offset, uint32_t* read);

#define DEVINTERFACE_SIZE_BLOCK_FILES 16

typedef struct _DevFile {
	char* name;
	Filestat stat;
} DevFile;

typedef struct _Devinterface	{
	char* mypath;
	DevFile** files;

} Devinterface;

Devinterface* devinterface_create(const char* path)	{
	Devinterface* ret = (Devinterface*)heap_malloc(sizeof(Devinterface));
	ret->mypath = (char*)heap_malloc(strlen(path) + 1);
	strcpy(ret->mypath, path);

	ret->files = (DevFile**)heap_malloc(sizeof(DevFile*) * DEVINTERFACE_SIZE_BLOCK_FILES);
	memset(ret->files, 0x00, sizeof(DevFile*) * DEVINTERFACE_SIZE_BLOCK_FILES);

	return ret;
}

Device* devinterface_init(const char* path)	{
	Devinterface* interf = devinterface_create(path);
	Device* ret = dev_alloc_device(path, devinterface_open, devinterface_fstat, interf);
	return ret;
}

DevRet devinterface_open(void* devcsr, const char* path, uint32_t mode, uint32_t devid, DeviceOpened* open)	{
	if(strlen(path) == 0)	{
		dev_fill_device_opened(open, devinterface_read, dev_error, dev_error, dev_error, devinterface_close, NULL);
	}
	else	{
		// We don't support creating new files or looking at files in dir
		return DEV_FAILURE_UNSUPPORTED;
	}

	return DEV_OK;
}

DevRet devinterface_read(void* devcsr, uint8_t* buf, uint32_t maxlen, uint32_t offset, uint32_t* read)	{
	kprintf(K_DEBUG, "DEVINT: reading\n");
	return DEV_FAILURE_UNSUPPORTED;
}

DevRet devinterface_fstat(void* devcsr, const char* path, Filestat* stat)	{
	kprintf(K_DEBUG, "DEVINT: fstat\n");
	stat->size = 0;
	stat->filetype = FILE_DIRECTORY;
	stat->owner = stat->group = 0;
	stat->operm = stat->gperm = stat->wperm = FSTAT_PERM_READ;
	kprintf(K_DEBUG, "ftype: %i\n", stat->filetype);

	return DEV_OK;
}

DevRet devinterface_close(void* devcsr)	{
	return DEV_OK;
}



; usermode.s
; First instructions executed after we enter user mode.
; This code should just load the usermode executable and jump
; to it


start:
	jmp apentry

bootbpstart:
	mov eax, 0x03		; Open
	mov ebx, consolein		; Path
	mov ecx, 0x00
	int 0x80
	; Check if we were successful
	cmp eax, 0x00
	jnz fatalerror


	mov eax, 0x03
	mov ebx, consoleout
	mov ecx, 0x00
	int 0x80
	cmp eax, 0x01
	jnz fatalerror

	mov eax, 0x06	; fork
	int 0x80

	test eax, eax
	jz .endlessloop
.ccode:
	; Load C code
	mov eax, 0x01	; exec
	mov ebx, init	; Program executable

	; argv - Create memory object
	mov ecx, argv1
	mov [argv], ecx

	mov ecx, argv

	; envp - Create memory object
	mov edx, envp1
	mov [envp], edx

	mov edx, envp2
	mov [envp + 4], edx

	mov edx, envp3
	mov [envp + 8], edx

	mov edx, envp
	int 0x80

	; Should never return here

.endlessloop:
	mov eax, 0x21	; nice
	mov ebx, 0x00	; Lowest priority
	int 0x80

	mov eax, 0xfe	; Halt CPU
	int 0x80


apentry:
	jmp $


fatalerror:
	jmp $

init db '/init', 0x00, 0x00
consoleout db '/dev/stdout', 0x00
consolein db '/dev/stdin', 0x00

argv:
    dd 0x00000000
    dd 0x00000000 ; Marks the end of argv

argv1 db '/init', 0x00

envp:
    dd 0x00000000
    dd 0x00000000
    dd 0x00000000
    dd 0x00000000
    dd 0x00000000

envp1 db 'PWD=/', 0x00
envp2 db 'PATH=/bin', 0x00
envp3 db 'HOME=/', 0x00


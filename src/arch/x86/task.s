; task.s
; Some of the code for doing software task switching

[BITS 32]

[GLOBAL task_enter_usermode]


; About iret instruction
; - Expects to find data in the following order:
;  - SS
;  - ESP
;  - EFLAGS
;  - CS
;  - EIP
; - If eflags is XOR-ed with 0x200, interrupts are enabled again

; INPUT:
; - Return EIP
task_enter_usermode:
	cli
	; TODO: Should have this as parameter, not hardcode it
	mov ebx, 0x40000005
	mov eax, (0x20 | 0x03)
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	push (0x20 | 0x03)	; SS
	push 0xFFB00F00		; ESP

	pushf				; EFLAGS
	pop eax
	or eax, 0x200
	push eax

	push (0x18 | 0x03)	; CS
	push ebx			; EIP
	iret


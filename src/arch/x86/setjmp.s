
[BITS 32]


; Based on implementation in newlib
; https://github.com/eblot/newlib/blob/master/newlib/libc/machine/i386/setjmp.S


setjmp:
	push ebp
	mov ebp, esp

	; Store edi and place buf variable in edi
	push edi
	mov edi, [ebp+8]

	mov [edi+0], EDI
	mov [edi+4], esi
	mov [edi+8], ebp

	mov [edi+16], ebx
	mov [edi+20], edx
	mov [edi+24], ecx
	mov [edi+28], eax

	mov eax, [ebp+4]	; eip
	mov [edi+32], eax

	mov eax, [ebp-4]	; edi
	mov [edi+0], eax

	mov eax, esp
	add eax, 12
	mov [edi+12], eax

	leave
	ret


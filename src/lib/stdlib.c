
#include "lib/stdlib.h"


int atoi(const char *str)	{
	int ret = 0;
	char* i;
	for(i = str; *i != 0x00; i++)	{
		ret *= 10;
		ret += (int)(*i) - (int)('0');
	}
	return ret;
}




all: kernel
	cp src/kernel iso/boot
	cp src/arch/x86/bootap.bin iso/
	cp src/arch/x86/usermode.bin iso/
	cp src/tools/sc2.bin iso/
	cp src/tools/ext2.fs iso/
	grub-mkrescue --xorriso=/opt/bin/xorriso -o image.iso iso

kernel:
	make -C ./src

start-compiler:
	sudo docker-compose up --build -d

docker-compile:
	sudo docker exec -u 1000:1000 -it compiler make

run-bochs: all
	bochs -q -f .bochsrc.txt | tee debug.log
	reset

# SMP does not work with more than 1 CPU on Qemu
# Keyboard doesn't work either
run-qemu: all
	qemu-system-i386 -cdrom image.iso -smp 1 -serial stdio -sdl -k no
#	qemu-system-i386 -cdrom image.iso -smp 1 -serial /dev/pts/4 -serial /dev/pts/3 -sdl -k no
#	qemu-system-i386 -cdrom image.iso -smp 1 -serial stdio -sdl -k no
#	qemu-system-x86_64 -cdrom image.iso

doc:
	make -C ./doc

clean:
	make -C ./src clean
	-rm -f bochsout.txt iso/boot/kernel image.iso serial.txt

fclean: clean
	make -C ./doc fclean

.PHONY: all clean fclean kernel doc run-qemu run-bochs
